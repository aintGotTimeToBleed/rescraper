## Rescraper
Webscraping project.

### Technologies
- jdk 11
- kuberentes, docker
- mongo, redis
- kafka (clients (alpakka kafka [akka], spring kafka, reactor))
- jsoup, selenium, vnc
- vavr
- spring boot (webflux, hateoas, actuator)
- prometheus, grafana

### Architecture
Project contains 5 microservices:
- reactive API 
  - each instance of API has its own instance of mongo db and eventbusconsumer
  - API is connected to db but only read data, 
  - each write permission is done via event which is saved in db via eventbusconsumer
- eventbusconsumer
  - used to read events from kafka and save data in mongo db
  - data is replicated via events
- job processor
  - used to prepare process of scraping
  - very small service
- evaluatior processor
  - used to gather all websites which needs to be scraped
  - creates scrape units
- scrape processor
  - scrapes data
  
### Running how-to
#### Local (linux, docker, intellij)
- ```docker-compose -f infrastrucure/docker/docker-compose.yml up -d```
- start locally from intellij reactive-api, job-processor, evaluation-processor, scrape-processor (add configuration for scrape-processor 2 and 3)

#### Kubernetes
##### Using minikube
- create infrastructure
```
# install minikube
# start minikube
minikube start --memory=6144
minikube start --memory=8192 --driver=virtualbox
---
# add kafka
kubectl create namespace re
kubectl -n re create -f 'https://strimzi.io/install/latest?namespace=re'
kubectl -n re apply -f kafka_strimzi.yaml
kubectl -n re wait kafka/rescraper-cluster --for=condition=Ready --timeout=300
# to test whether kafka is working [optional]
kubectl -n re run kafka-producer -ti --image=quay.io/strimzi/kafka:0.24.0-kafka-2.8.0 --rm=true --restart=Never -- bin/kafka-console-producer.sh --broker-list rescraper-cluster-kafka-bootstrap:9092 --topic my-topic
kubectl -n re run kafka-consumer -ti --image=quay.io/strimzi/kafka:0.24.0-kafka-2.8.0 --rm=true --restart=Never -- bin/kafka-console-consumer.sh --bootstrap-server rescraper-cluster-kafka-bootstrap:9092 --topic my-topic --from-beginning 
# create topics
kubectl -n re run kafka-producer -ti --image=quay.io/strimzi/kafka:0.24.0-kafka-2.8.0 --rm=true --restart=Never -- bin/kafka-topics.sh --create --topic rescraper-jobs --partitions 12 --bootstrap-server rescraper-cluster-kafka-bootstrap:9092
kubectl -n re run kafka-producer -ti --image=quay.io/strimzi/kafka:0.24.0-kafka-2.8.0 --rm=true --restart=Never -- bin/kafka-topics.sh --create --topic rescraper-evaluation-units --partitions 12 --bootstrap-server rescraper-cluster-kafka-bootstrap:9092
kubectl -n re run kafka-producer -ti --image=quay.io/strimzi/kafka:0.24.0-kafka-2.8.0 --rm=true --restart=Never -- bin/kafka-topics.sh --create --topic rescraper-scrape-units --partitions 24 --bootstrap-server rescraper-cluster-kafka-bootstrap:9092
# verify topic existence [optional]
kubectl -n re run kafka-producer -ti --image=quay.io/strimzi/kafka:0.24.0-kafka-2.8.0 --rm=true --restart=Never -- bin/kafka-topics.sh --list --bootstrap-server rescraper-cluster-kafka-bootstrap:9092
kubectl -n re run kafka-producer -ti --image=quay.io/strimzi/kafka:0.24.0-kafka-2.8.0 --rm=true --restart=Never -- bin/kafka-topics.sh --describe --topic rescraper-scrape-units --bootstrap-server rescraper-cluster-kafka-bootstrap:9092
# describe consumer groups [optional]
kubectl -n re run kafka-producer -ti --image=quay.io/strimzi/kafka:0.24.0-kafka-2.8.0 --rm=true --restart=Never -- bin/kafka-consumer-groups.sh --bootstrap-server rescraper-cluster-kafka-bootstrap:9092 --describe --all-groups
---
# mongo
kubectl -n re create -f mongo_pvc.yaml 
## expose mongo [optional]
kubectl -n re port-forward rescraper-mongo-c98b747cc-xrkx5 27019:27017
---
# redis
kubectl -n re create -f redis_pvc.yaml
kubectl -n re apply -f redis_deployment.yaml
---
# chrome
kubectl -n re apply -f chrome_deployment.yaml
---
# install helm (https://helm.sh/docs/intro/install/)
## ubuntu
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm
---
# install prometheus (https://engineering.linecorp.com/en/blog/monitoring-a-spring-boot-app-in-kubernetes-what-i-learned-from-devoxx-belgium-2019/)
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm install re-prometheus stable/prometheus
## expose prometheus
export POD_NAME=$(kubectl get pods -n re -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")  
kubectl -n re port-forward $POD_NAME 9090
---
# install grafana
helm install re-grafana stable/grafana
## get admin password
kubectl get secret --namespace re re-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
## expose grafana
export POD_NAME=$(kubectl get pods --namespace re -l "app.kubernetes.io/name=grafana,app.kubernetes.io/instance=re-grafana" -o jsonpath="{.items[0].metadata.name}")
kubectl --namespace re port-forward $POD_NAME 3000
## add datasource in grafana
datasoure -> prometheus
url: http://localhost:9090
access: browser
---
# add kafka metrics [todo]
- https://dzone.com/articles/grafana-and-prometheus-setup-with-strimzi-aka-kafk
- https://strimzi.io/docs/operators/latest/deploying.html#assembly-metrics-setup-str

```
- deploy services/pods
```
cd rescraper
eval $(minikube docker-env)
mvn clean install
  
# EVENT BUS CONSUMER
docker build -t rescraper/eventbusconsumer .  
  
# API multicontainer with EVENTBUSCONSUMER and MONGO
docker build -t rescraper/reactive-api .
kubectl apply -f k8s/api_stateful_set.yaml -n re
[optional]
kubectl -n re port-forward rescraper-reactiveapi-54499f598c-q57wf  9999:9999
kubectl -n re port-forward rescraper-chrome-api-7677d97d79-zd6gp 5900:5900

# JOB PROCESSOR
mvn clean install
docker build -t rescraper/job-processor .
kubectl -n re apply -f k8s/job_deployment.yaml 

# EVALUATION PROCESSOR
mvn clean install
docker build -t rescraper/evaluation-processor .
kubectl -n re apply -f k8s/evaluation_deployment.yaml 

# SCRAPE PROCESSOR
mvn clean install
docker build -t rescraper/scrape-processor .
kubectl -n re apply -f k8s/scrape_deployment.yaml 
```
#### See API readme for example requests




===
### Below sections need to be updated, some information in below section can be wrong
#### Docker (windows) [UPDATE]
1. Following must be installed: GIT, ConEMU, Mongo Compass, docker, docker-compose
2. Build apps with ``` mvn clean install -DskipTests ```. Then built docker images, before starting docker-compose 
    - API ``` docker build -t rescraper/reactive-api . ```
    - Job Processor ``` docker build -t rescraper/job-processor . ```
    - Evaluation Processor ``` docker build -t rescraper/evaluation-processor . ```
    - Scrape Processor ``` docker build -t rescraper/scrape-processor . ```
3. Start
``` docker-compose -f up -d ``` wait at least 1 minute, containers need to start properly
4. Stop in following order
```
    docker-compose -f down
```
5. Useful commands
```
    docker logs --follow 90da4231caf4
```
6. Url
- Grafana -> localhost:3000 [admin : admin]
- Prometheus -> localhost:9090
#### Troubleshooting [VERIFY]
```
    docker volume prune
    docker container prune
```



#### Start prometheus [TO UPDATE] [UPDATE]
docker run -d --name=rescraper-prometheus -p 9090:9090 -v /home/zapisek/projects/rescraper-common/src/main/resources/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus --config.file=/etc/prometheus/prometheus.yml

Windows:
- zmienic ustawienia w dockerze, dodac w file sharing lokalizacje
- odpal command prompt
- docker run -d --name=rescraper-prometheus -p 9090:9090 -v c:\projects\hackaton\rescraper\rescrapercommon\src\main\resources\prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus --config.file=/etc/prometheus/prometheus.yml
    
#### todo
* [done] send to specific partitions based on max parallelism
* [done] rerun for evaluation
* [done] rerun for messages
* [done] add redis (can be used to skip evaluationUnits, scrapeUnits for cancelled jobs)
* [done] status check
* [done] testrun // job, eval, scrape 
* [done] divide consumers configuration
* unit tests
* integration tests, use wiremock to stub a website
* [done] result generation (api endpoint), result controller
* validators
* add annotations
* add headers to kafka message (jobId, cancelling messages will run faster)
* proxy rotator
* tracing
* graceful shutdown for kafka when scraping is active -> https://github.com/reactor/reactor-kafka/issues/51
* [done] graceful shutdown for api
* docker infrastructure (multiple kafka brokers)
* [done] kubernetes infrastructure
* terraform deploying to AWS
* elk stack for logs
* possibility to choose between spring kafka / reactor kafka / alpakka kafka to consume kafka messages
* [done] add protobuf
* web driver wrapper with logging and metrics (see WebDriverService)
* [!!] add Resillience4j
* [done] add liveness probes (kubernetes)
* add UI 
* add scheduler
* add shedlock to scheduler
* add reactive redis driver
* [done] use block hound
* akka events
* kafka should be available from outside kubernetes network
* [done] processor pods with sidecar db and akka event consumer, consumer should insert data into db
* create junit extension instead of abstract class [low]
* prometheus/grafana on Kubernetes -> https://devopscube.com/setup-prometheus-monitoring-on-kubernetes/
                                    -> https://engineering.linecorp.com/en/blog/monitoring-a-spring-boot-app-in-kubernetes-what-i-learned-from-devoxx-belgium-2019/
* kafka metrics on prometheus
* [done] processors shouldnt call db directly but through API, inserts into db via events

#### add random proxies to redis [TO UPDATE] [UPDATE]
- https://www.proxy-list.download/HTTP

LPUSH proxies 167.71.5.83:3128 198.199.86.11:3128 134.122.119.118:80 102.129.249.120:3128 138.68.60.8:3128
LPUSH proxies 20.186.110.157:3128 69.252.50.230:3128 191.96.42.80:3128 165.225.32.114:10417 165.225.32.113:10223

#### services [UPDATE]
- http://localhost:9090/graph
- http://localhost:3000
    - add datasource
        * example 172.17.0.1:9090 (docker gateway)

#### How to - Initialize ReScraper project WINDOWS [UPDATE]
- intellij - add modules
- ```docker-compose up -d```
- start prometheus - windows: ```docker run -d --name=rescraper-prometheus -p 9090:9090 -v c:\projects\hackaton\rescraper\rescrapercommon\src\main\resources\prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus --config.file=/etc/prometheus/prometheus.yml```
- start api 
- start job processor 
- start evaluation processor
- start scraper (1)
- start additional scraper (2), set "Environment variables:" in Intellij -> server.port=8002;selenium.hub.host=http://localhost:4448/wd/hub
- start additional scraper (3), set "Environment variables:" in Intellij -> server.port=8003;selenium.hub.host=http://localhost:4449/wd/hub
- download VNC viewer
- password for vnc images: ```secret```
- vnc ports:
 * job processor - 5903
 * evaluation processor - 5902
 * scrape processor (1) - 5901
 * scrape processor (2) - 5904
 * scrape processor (3) - 5905
- grafana - http://localhost:3000 , login and password: admin/admin ; trzeba dodac datasoruce: url 172.17.0.1:9090

#### notes
- there will be one instance of redis for now
  - caching
  - distributed locks (shedlock, scheduler)
- should scheduler be in a separate project or in API? 
