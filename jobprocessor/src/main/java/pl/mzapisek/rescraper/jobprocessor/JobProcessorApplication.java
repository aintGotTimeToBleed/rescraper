package pl.mzapisek.rescraper.jobprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.mzapisek.rescraper.common.protobuf.ScrapeResultProtos;
import pl.mzapisek.rescraper.reactor_kafka.jobprocessor.EnableReactorKafkaJobProcessor;
import reactor.blockhound.BlockHound;

@SpringBootApplication
//@EnableSpringKafkaJobProcessor // spring kafka
@EnableReactorKafkaJobProcessor // reactor kafka
public class JobProcessorApplication {

    public static void main(String[] args) {
        BlockHound.install();
        SpringApplication.run(JobProcessorApplication.class, args);
    }

}
