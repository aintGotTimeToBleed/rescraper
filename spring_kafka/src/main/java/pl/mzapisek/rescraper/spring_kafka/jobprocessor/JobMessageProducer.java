package pl.mzapisek.rescraper.spring_kafka.jobprocessor;

import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.services.JsonUtils;

@RequiredArgsConstructor
public class JobMessageProducer {

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final KafkaBaseProperties kafkaBaseProperties;
    private final JsonUtils jsonUtils;

    public ListenableFuture<SendResult<String, String>> send(Job message) {
        return jsonUtils.createJsonTry(message)
                .map(jobAsString -> kafkaTemplate.send(kafkaBaseProperties.getJobTopic(), jobAsString))
                .get();
    }

}
