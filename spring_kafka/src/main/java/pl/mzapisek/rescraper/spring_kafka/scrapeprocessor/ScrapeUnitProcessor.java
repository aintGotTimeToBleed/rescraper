package pl.mzapisek.rescraper.spring_kafka.scrapeprocessor;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;
import pl.mzapisek.rescraper.common.shared.repository.ScrapeResultMongoRepository;
import pl.mzapisek.rescraper.common.api.CompleteProcessService;
import pl.mzapisek.rescraper.common.shared.services.MetricsService;
import pl.mzapisek.rescraper.common.shared.services.RedisService;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import static java.time.LocalDateTime.now;

@Log4j2
@RequiredArgsConstructor
public class ScrapeUnitProcessor {

    private final ScrapeScriptFactory scrapeScriptFactory;
    private final ScrapeUnitMongoRepository scrapeUnitMongoRepository;
    private final ScrapeResultMongoRepository scrapeResultMongoRepository;
    private final MetricsService metricsService;
    private final RedisService redisService;
    private final CompleteProcessService completeProcessService;

    ScrapeUnit process(ScrapeUnit scrapeUnit) {
        Objects.requireNonNull(scrapeUnit);

        log.info("scrape unit processing started for job: {}, evaluation: {}", scrapeUnit.getJobId(), scrapeUnit.getEvaluationId());
        log.info("scrape unit: {}", scrapeUnit);

        return Try.of(() -> scrapeScriptFactory.create(scrapeUnit.getScriptName(), scrapeUnit.getScriptVersion()))
                .map(scrapeScript -> processScrapeUnit(scrapeUnit, scrapeScript))
                .map(this::saveScrapeResult)
                .map(this::completeProcess)
                .getOrElseGet(throwable -> {
                    log.error(throwable.getMessage());
                    throwable.printStackTrace();

                    return saveScrapeUnit(scrapeUnit.toBuilder()
                            .scrapeUnitStatus(ScrapeUnitStatus.FAILED)
                            .errorMessage(throwable.getMessage())
                            .finishedAt(now())
                            .build());
                });
    }

    private Tuple2<ScrapeUnit, List<ScrapeResult>> processScrapeUnit(ScrapeUnit scrapeUnit, ScrapeScript scrapeScript) {
        ScrapeUnit.ScrapeUnitBuilder scrapeUnitBuilder = scrapeUnit.toBuilder();
        if (redisService.isJobCancelled(scrapeUnit.getJobId())) {
            return Tuple.of(scrapeUnitBuilder.scrapeUnitStatus(ScrapeUnitStatus.CANCELLED).build(), List.of());
        } else {
            return Tuple.of(scrapeUnitBuilder.scrapeUnitStatus(ScrapeUnitStatus.PROCESSING).build(), scrapeScript.process(scrapeUnit));
        }
    }

    private ScrapeUnit saveScrapeResult(Tuple2<ScrapeUnit, List<ScrapeResult>> tuple) {
        ScrapeUnit scrapeUnit = tuple._1;
        List<ScrapeResult> scrapeResults = tuple._2;

        if (scrapeUnit.getScrapeUnitStatus().equals(ScrapeUnitStatus.CANCELLED)) {
            saveScrapeUnit(scrapeUnit);
        } else {
            scrapeResultMongoRepository
                    .saveAll(scrapeResults)
                    .doOnComplete(() ->
                            saveScrapeUnit(scrapeUnit.toBuilder()
                                    .finishedAt(LocalDateTime.now())
                                    .scrapeUnitStatus(ScrapeUnitStatus.SUCCESS)
                                    .build()))
                    .subscribe();
        }

        return tuple._1;
    }

    private ScrapeUnit saveScrapeUnit(ScrapeUnit scrapeUnit) {
        scrapeUnitMongoRepository.save(scrapeUnit).subscribe();
        metricsService.scrapeUnitTimer(scrapeUnit);
        log.info("processed scrape unit: {}", scrapeUnit);

        return scrapeUnit;
    }

    private ScrapeUnit completeProcess(ScrapeUnit scrapeUnit) {
        completeProcessService
                .completeProcess(scrapeUnit.getEvaluationId(), scrapeUnit.getJobId())
                .subscribe();

        return scrapeUnit;
    }

}