package pl.mzapisek.rescraper.spring_kafka.scrapeprocessor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import pl.mzapisek.rescraper.common.configuration.RescraperConfiguration;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.shared.repository.ScrapeResultMongoRepository;
import pl.mzapisek.rescraper.common.api.CompleteProcessService;
import pl.mzapisek.rescraper.common.shared.services.MetricsService;
import pl.mzapisek.rescraper.common.shared.services.RedisService;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.spring_kafka.configs.SpringKafkaConsumerConfiguration;

@Import({SpringKafkaConsumerConfiguration.class, RescraperConfiguration.class})
@ComponentScan(basePackages = {"pl.mzapisek.rescraper.common.scrapeprocessor"})
public class ScrapeProcessorConfiguration {

    @Bean
    public ScrapeUnitConsumer scrapeUnitConsumer(ScrapeUnitProcessor scrapeUnitProcessor) {
        return new ScrapeUnitConsumer(scrapeUnitProcessor);
    }

    @Bean
    public ScrapeUnitProcessor scrapeUnitProcessor(
            ScrapeScriptFactory scrapeScriptFactory,
            ScrapeUnitMongoRepository scrapeUnitMongoRepository,
            ScrapeResultMongoRepository scrapeResultMongoRepository,
            MetricsService metricsService,
            RedisService redisService,
            CompleteProcessService completeProcessService) {
        return new ScrapeUnitProcessor(
                scrapeScriptFactory,
                scrapeUnitMongoRepository,
                scrapeResultMongoRepository,
                metricsService,
                redisService,
                completeProcessService);
    }

}
