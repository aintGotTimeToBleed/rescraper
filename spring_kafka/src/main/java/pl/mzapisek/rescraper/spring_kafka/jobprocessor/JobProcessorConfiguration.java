package pl.mzapisek.rescraper.spring_kafka.jobprocessor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.KafkaTemplate;
import pl.mzapisek.rescraper.common.configuration.RescraperConfiguration;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.repository.EvaluationUnitMongoRepository;
import pl.mzapisek.rescraper.common.jobprocessor.repository.JobMongoRepository;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.services.BatchCreator;
import pl.mzapisek.rescraper.common.shared.services.JsonUtils;
import pl.mzapisek.rescraper.common.shared.services.MetricsService;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.spring_kafka.configs.SpringKafkaConsumerConfiguration;
import pl.mzapisek.rescraper.spring_kafka.evaluationprocessor.EvaluationUnitMessageProducer;
import pl.mzapisek.rescraper.spring_kafka.scrapeprocessor.ScrapeUnitMessageProducer;

@Import({SpringKafkaConsumerConfiguration.class, RescraperConfiguration.class})
@ComponentScan(
        basePackageClasses = {BatchCreator.class},
        basePackages = {"pl.mzapisek.rescraper.common.jobprocessor"})
public class JobProcessorConfiguration {

    @Bean
    public JobConsumer jobConsumer(JobProcessor jobProcessor) {
        return new JobConsumer(jobProcessor);
    }

    @Bean
    public EvaluationUnitMessageProducer evaluationMessageProducer(KafkaTemplate<String, String> kafkaTemplate,
                                                                   KafkaBaseProperties kafkaBaseProperties,
                                                                   BatchCreator<EvaluationUnit> batchCreator,
                                                                   JsonUtils jsonUtils) {
        return new EvaluationUnitMessageProducer(kafkaTemplate, kafkaBaseProperties, jsonUtils, batchCreator);
    }

    @Bean
    public ScrapeUnitMessageProducer scrapeUnitMessageProducer(KafkaTemplate<String, String> kafkaTemplate,
                                                               KafkaBaseProperties kafkaBaseProperties,
                                                               BatchCreator<ScrapeUnit> batchCreator,
                                                               JsonUtils jsonUtils) {
        return new ScrapeUnitMessageProducer(kafkaTemplate, kafkaBaseProperties, batchCreator, jsonUtils);
    }

    @Bean
    public JobProcessor jobProcessor(EvaluationUnitMessageProducer evaluationUnitMessageProducer,
                                     ScrapeUnitMessageProducer scrapeUnitMessageProducer,
                                     ScrapeScriptFactory scrapeScriptFactory,
                                     EvaluationUnitMongoRepository evaluationUnitMongoRepository,
                                     ScrapeUnitMongoRepository scrapeUnitMongoRepository,
                                     JobMongoRepository jobMongoRepository,
                                     MetricsService metricsService) {
        return new JobProcessor(
                evaluationUnitMessageProducer,
                scrapeUnitMessageProducer,
                scrapeScriptFactory,
                evaluationUnitMongoRepository,
                scrapeUnitMongoRepository,
                jobMongoRepository,
                metricsService);
    }

}
