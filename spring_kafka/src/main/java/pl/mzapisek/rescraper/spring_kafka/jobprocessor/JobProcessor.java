package pl.mzapisek.rescraper.spring_kafka.jobprocessor;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnitStatus;
import pl.mzapisek.rescraper.common.evaluationprocessor.repository.EvaluationUnitMongoRepository;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.jobprocessor.domain.JobStatus;
import pl.mzapisek.rescraper.common.jobprocessor.repository.JobMongoRepository;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;
import pl.mzapisek.rescraper.common.shared.services.MetricsService;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.spring_kafka.evaluationprocessor.EvaluationUnitMessageProducer;
import pl.mzapisek.rescraper.spring_kafka.scrapeprocessor.ScrapeUnitMessageProducer;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
@Log4j2
public class JobProcessor {

    private final EvaluationUnitMessageProducer evaluationUnitMessageProducer;
    private final ScrapeUnitMessageProducer scrapeUnitMessageProducer;
    private final ScrapeScriptFactory scrapeScriptFactory;
    private final EvaluationUnitMongoRepository evaluationUnitMongoRepository;
    private final ScrapeUnitMongoRepository scrapeUnitMongoRepository;
    private final JobMongoRepository jobMongoRepository;
    private final MetricsService metricsService;

    Job process(Job job) {
        Objects.requireNonNull(job);

        log.info("job processing started for job: {}", job.getId());
        log.info("job: {}", job);

        return Try.of(() -> scrapeScriptFactory.create(job.getScriptName(), job.getScriptVersion()))
                .map(script -> sendUnits(job.toBuilder().status(JobStatus.PROCESSING).build(), script))
                .map(this::saveJob)
                .getOrElseGet(throwable -> {
                    log.error(throwable.getMessage());
                    throwable.printStackTrace();

                    return saveJob(job.toBuilder()
                            .status(JobStatus.FAILED)
                            .errorMessage(throwable.getMessage())
                            .build());
                });
    }

    private Job sendUnits(Job job, ScrapeScript scrapeScript) {
        if (job.isRerun()) {
            getFailedEvaluationUnits(job).subscribe(evaluationUnitMessageProducer::send);
            getFailedScrapeUnits(job).subscribe(scrapeUnitMessageProducer::send);
        } else {
            List<EvaluationUnit> evaluationUnits = scrapeScript.process(job);
            evaluationUnitMongoRepository
                    .saveAll(evaluationUnits)
                    .doOnComplete(() -> evaluationUnitMessageProducer.send(evaluationUnits, job.getAssignedPartitions()))
                    .subscribe();
        }

        return job;
    }

    private Job saveJob(Job job) {
        metricsService.jobTimer(job);
        jobMongoRepository.save(job).subscribe();
        log.info("processed job: {}", job);

        return job;
    }

    private Flux<EvaluationUnit> getFailedEvaluationUnits(Job job) {
        return evaluationUnitMongoRepository.findByStatusAndJobId(EvaluationUnitStatus.FAILED, job.getId());
    }

    private Flux<ScrapeUnit> getFailedScrapeUnits(Job job) {
        return scrapeUnitMongoRepository.findByScrapeUnitStatusAndJobId(ScrapeUnitStatus.FAILED, job.getId());
    }

}
