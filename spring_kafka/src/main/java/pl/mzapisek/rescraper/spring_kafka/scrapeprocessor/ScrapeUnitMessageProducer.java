package pl.mzapisek.rescraper.spring_kafka.scrapeprocessor;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.services.BatchCreator;
import pl.mzapisek.rescraper.common.shared.services.JsonUtils;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ScrapeUnitMessageProducer {

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final KafkaBaseProperties kafkaBaseProperties;
    private final BatchCreator<ScrapeUnit> batchCreator;
    private final JsonUtils jsonUtils;

    // todo send to specified partition from pool
    public ListenableFuture<SendResult<String, String>> send(ScrapeUnit message) {
        return jsonUtils.createJsonTry(message)
                .map(evaluationUnit -> kafkaTemplate.send(kafkaBaseProperties.getScrapeUnitTopic(), evaluationUnit))
                .get();
    }

    public List<ListenableFuture<SendResult<String, String>>> send(List<ScrapeUnit> scrapeUnits, List<Integer> partitionNumbers) {
        Objects.requireNonNull(scrapeUnits);
        Objects.requireNonNull(partitionNumbers);

        var atomicInteger = new AtomicInteger();

        return batchCreator.createBatch(scrapeUnits, partitionNumbers.size())
                .stream()
                .flatMap(aScrapeUnits -> sendBatch(aScrapeUnits, partitionNumbers.get(atomicInteger.getAndIncrement())))
                .collect(Collectors.toList());
    }

    private Stream<ListenableFuture<SendResult<String, String>>> sendBatch(List<ScrapeUnit> scrapeUnits, Integer partition) {
        return scrapeUnits.stream()
                .map(jsonUtils::createJsonTry)
                .map(tryScrapeUnitAsString ->
                        tryScrapeUnitAsString.map(su -> kafkaTemplate.send(createProducerRecord(partition, su))))
                .filter(Try::isSuccess)
                .map(Try::get);
    }

    private ProducerRecord<String, String> createProducerRecord(Integer partition, String scrapeUnitAsString) {
        return new ProducerRecord<>(kafkaBaseProperties.getScrapeUnitTopic(), partition, null, scrapeUnitAsString);
    }

}
