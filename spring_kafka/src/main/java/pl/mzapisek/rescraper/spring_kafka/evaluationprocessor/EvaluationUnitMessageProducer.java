package pl.mzapisek.rescraper.spring_kafka.evaluationprocessor;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.services.BatchCreator;
import pl.mzapisek.rescraper.common.shared.services.JsonUtils;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class EvaluationUnitMessageProducer {

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final KafkaBaseProperties kafkaBaseProperties;
    private final JsonUtils jsonUtils;
    private final BatchCreator<EvaluationUnit> batchCreator;

    public ListenableFuture<SendResult<String, String>> send(EvaluationUnit message) {
        return jsonUtils.createJsonTry(message)
                .map(evaluationUnit -> kafkaTemplate.send(kafkaBaseProperties.getEvaluationUnitTopic(), evaluationUnit))
                .get();
    }

    public List<ListenableFuture<SendResult<String, String>>> send(List<EvaluationUnit> evaluationUnits, List<Integer> partitionNumbers) {
        Objects.requireNonNull(evaluationUnits);
        Objects.requireNonNull(partitionNumbers);

        var atomicInteger = new AtomicInteger();

        return batchCreator.createBatch(evaluationUnits, partitionNumbers.size())
                .stream()
                .flatMap(aEvaluationUnits -> sendBatch(aEvaluationUnits, partitionNumbers.get(atomicInteger.getAndIncrement())))
                .collect(Collectors.toList());
    }

    private Stream<ListenableFuture<SendResult<String, String>>> sendBatch(List<EvaluationUnit> evaluationUnits, Integer partition) {
        return evaluationUnits.stream()
                .map(jsonUtils::createJsonTry)
                .map(msgAsString -> msgAsString.map(su -> kafkaTemplate.send(createProducerRecord(partition, su))))
                .filter(Try::isSuccess)
                .map(Try::get);
    }

    private ProducerRecord<String, String> createProducerRecord(Integer partition, String evaluationUnitAsString) {
        return new ProducerRecord<>(kafkaBaseProperties.getEvaluationUnitTopic(), partition, null, evaluationUnitAsString);
    }

}
