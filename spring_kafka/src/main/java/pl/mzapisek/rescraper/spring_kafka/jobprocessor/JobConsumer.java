package pl.mzapisek.rescraper.spring_kafka.jobprocessor;

import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;

@RequiredArgsConstructor
public class JobConsumer {

    private final JobProcessor jobProcessor;

    @KafkaListener(groupId = "${kafka.consumer.consumer-group}", topics = "${kafka.base.job-topic}")
    public void jobConsumer(Job job) {
        jobProcessor.process(job);
    }

}
