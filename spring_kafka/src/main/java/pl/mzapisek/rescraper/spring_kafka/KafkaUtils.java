package pl.mzapisek.rescraper.spring_kafka;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.PartitionInfo;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class KafkaUtils {

    private final KafkaTemplate<String, String> kafkaTemplate;

    public List<Integer> getRandomPartitions(String topic, Integer numberOfPartitions) {
        var partitions = getPartitionInfos(topic).stream()
                .map(PartitionInfo::partition)
                .collect(Collectors.toList());

        Collections.shuffle(partitions);

        return partitions.stream()
                .limit(numberOfPartitions)
                .collect(Collectors.toList());
    }

    private List<PartitionInfo> getPartitionInfos(String topic) {
        return kafkaTemplate.partitionsFor(topic);
    }

}