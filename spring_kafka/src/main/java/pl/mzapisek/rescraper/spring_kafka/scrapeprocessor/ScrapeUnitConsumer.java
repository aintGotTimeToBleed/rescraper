package pl.mzapisek.rescraper.spring_kafka.scrapeprocessor;

import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;

@RequiredArgsConstructor
public class ScrapeUnitConsumer {

    private final ScrapeUnitProcessor scrapeUnitProcessor;

    @KafkaListener(groupId = "${kafka.consumer.consumer-group}", topics = "${kafka.base.scrape-unit-topic}")
    public void scrapeUnitConsumer(ScrapeUnit scrapeUnit) {
        scrapeUnitProcessor.process(scrapeUnit);
    }

}
