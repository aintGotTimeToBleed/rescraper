package pl.mzapisek.rescraper.spring_kafka.evaluationprocessor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.KafkaTemplate;
import pl.mzapisek.rescraper.common.configuration.RescraperConfiguration;
import pl.mzapisek.rescraper.common.evaluationprocessor.repository.EvaluationUnitMongoRepository;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.services.*;
import pl.mzapisek.rescraper.spring_kafka.configs.SpringKafkaConsumerConfiguration;
import pl.mzapisek.rescraper.spring_kafka.scrapeprocessor.ScrapeUnitMessageProducer;

@Import({SpringKafkaConsumerConfiguration.class, RescraperConfiguration.class})
@ComponentScan(
        basePackageClasses = {BatchCreator.class},
        basePackages = {"pl.mzapisek.rescraper.common.evaluationprocessor"})
public class EvaluationProcessorConfiguration {

    @Bean
    public EvaluationUnitConsumer evaluationUnitConsumer(EvaluationUnitProcessor evaluationUnitProcessor) {
        return new EvaluationUnitConsumer(evaluationUnitProcessor);
    }

    @Bean
    public ScrapeUnitMessageProducer scrapeUnitMessageProducer(KafkaTemplate<String, String> kafkaTemplate,
                                                               KafkaBaseProperties kafkaBaseProperties,
                                                               BatchCreator<ScrapeUnit> batchCreator,
                                                               JsonUtils jsonUtils) {
        return new ScrapeUnitMessageProducer(kafkaTemplate, kafkaBaseProperties, batchCreator, jsonUtils);
    }

    @Bean
    public EvaluationUnitProcessor evaluationUnitProcessor(ScrapeUnitMessageProducer scrapeUnitMessageProducer,
                                                           ScrapeScriptFactory scrapeScriptFactory,
                                                           EvaluationUnitMongoRepository evaluationUnitMongoRepository,
                                                           ScrapeUnitMongoRepository scrapeUnitMongoRepository,
                                                           MetricsService metricsService,
                                                           RedisService redisService) {
        return new EvaluationUnitProcessor(
                scrapeUnitMessageProducer,
                scrapeScriptFactory,
                evaluationUnitMongoRepository,
                scrapeUnitMongoRepository,
                metricsService,
                redisService);
    }

}
