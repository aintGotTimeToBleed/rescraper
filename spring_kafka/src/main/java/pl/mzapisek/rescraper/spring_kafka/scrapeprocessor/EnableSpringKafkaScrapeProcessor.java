package pl.mzapisek.rescraper.spring_kafka.scrapeprocessor;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({ScrapeProcessorConfiguration.class})
public @interface EnableSpringKafkaScrapeProcessor {
}
