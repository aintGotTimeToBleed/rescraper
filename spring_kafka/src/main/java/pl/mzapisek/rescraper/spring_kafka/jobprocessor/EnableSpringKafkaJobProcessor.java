package pl.mzapisek.rescraper.spring_kafka.jobprocessor;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({JobProcessorConfiguration.class})
public @interface EnableSpringKafkaJobProcessor {
}
