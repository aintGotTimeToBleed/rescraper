package pl.mzapisek.rescraper.spring_kafka.evaluationprocessor;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnitStatus;
import pl.mzapisek.rescraper.common.evaluationprocessor.repository.EvaluationUnitMongoRepository;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.shared.services.MetricsService;
import pl.mzapisek.rescraper.common.shared.services.RedisService;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.spring_kafka.scrapeprocessor.ScrapeUnitMessageProducer;

import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
@Log4j2
public class EvaluationUnitProcessor {

    private final ScrapeUnitMessageProducer scrapeUnitMessageProducer;
    private final ScrapeScriptFactory scrapeScriptFactory;
    private final EvaluationUnitMongoRepository evaluationUnitMongoRepository;
    private final ScrapeUnitMongoRepository scrapeUnitMongoRepository;
    private final MetricsService metricsService;
    private final RedisService redisService;

    EvaluationUnit process(EvaluationUnit evaluationUnit) {
        Objects.requireNonNull(evaluationUnit);

        // todo add evaluation unit validator

        log.info("evaluation unit processing started for job: {}", evaluationUnit.getJobId());
        log.info("evaluation unit: {}", evaluationUnit);

        return Try.of(() -> scrapeScriptFactory.create(evaluationUnit.getScriptName(), evaluationUnit.getScriptVersion()))
                .map(scrapeScript -> processEvaluationUnit(scrapeScript, evaluationUnit))
                .map(this::sendScrapeUnits)
                .map(evaluationUnitScrapeUnitsTuple -> saveEvaluationUnit(evaluationUnitScrapeUnitsTuple._1))
                .getOrElseGet(throwable -> {
                    log.error(throwable.getMessage());
                    throwable.printStackTrace();

                    return saveEvaluationUnit(evaluationUnit.toBuilder()
                            .status(EvaluationUnitStatus.FAILED)
                            .errorMessage(throwable.getMessage())
                            .build());
                });
    }

    private Tuple2<EvaluationUnit, List<ScrapeUnit>> processEvaluationUnit(ScrapeScript scrapeScript, EvaluationUnit evaluationUnit) {
        EvaluationUnit.EvaluationUnitBuilder evaluationUnitBuilder = evaluationUnit.toBuilder();
        if (redisService.isJobCancelled(evaluationUnit.getJobId())) {
            return Tuple.of(evaluationUnitBuilder.status(EvaluationUnitStatus.CANCELLED).build(), List.of());
        } else {
            List<ScrapeUnit> scrapeUnits = scrapeScript.process(evaluationUnit);
            if (scrapeUnits.isEmpty()) {
                return Tuple.of(evaluationUnitBuilder.status(EvaluationUnitStatus.SUCCESS).build(), scrapeUnits);
            } else {
                return Tuple.of(evaluationUnitBuilder.status(EvaluationUnitStatus.PROCESSING).build(), scrapeUnits);
            }
        }
    }

    private Tuple2<EvaluationUnit, List<ScrapeUnit>> sendScrapeUnits(Tuple2<EvaluationUnit, List<ScrapeUnit>> tuple) {
        EvaluationUnit evaluationUnit = tuple._1;
        List<ScrapeUnit> scrapeUnits = tuple._2;

        if (evaluationUnit.getStatus().equals(EvaluationUnitStatus.PROCESSING)) {
            scrapeUnitMongoRepository
                    .saveAll(scrapeUnits)
                    .doOnComplete(() -> scrapeUnitMessageProducer.send(scrapeUnits, evaluationUnit.getAssignedPartitions()))
                    .subscribe();
        }

        return tuple;
    }

    private EvaluationUnit saveEvaluationUnit(EvaluationUnit evaluationUnit) {
        metricsService.evaluationUnitTimer(evaluationUnit);
        evaluationUnitMongoRepository.save(evaluationUnit).subscribe();
        log.info("evaluation unit processed: {}", evaluationUnit);

        return evaluationUnit;
    }

}
