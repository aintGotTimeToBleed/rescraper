package pl.mzapisek.rescraper.spring_kafka.evaluationprocessor;

import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;

@RequiredArgsConstructor
public class EvaluationUnitConsumer {

    private final EvaluationUnitProcessor evaluationUnitProcessor;

    @KafkaListener(groupId = "${kafka.consumer.consumer-group}", topics = "${kafka.base.evaluation-unit-topic}")
    public void jobConsumer(EvaluationUnit evaluationUnit) {
        evaluationUnitProcessor.process(evaluationUnit);
    }

}
