package pl.mzapisek.rescraper.spring_kafka.jobprocessor;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.repository.EvaluationUnitMongoRepository;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.jobprocessor.domain.JobStatus;
import pl.mzapisek.rescraper.common.jobprocessor.repository.JobMongoRepository;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.shared.exceptions.ScrapeScriptNotFound;
import pl.mzapisek.rescraper.common.shared.services.MetricsService;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.spring_kafka.evaluationprocessor.EvaluationUnitMessageProducer;
import pl.mzapisek.rescraper.spring_kafka.scrapeprocessor.ScrapeUnitMessageProducer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JobProcessorTest {

    @Mock
    private EvaluationUnitMessageProducer evaluationMessageProducer;
    @Mock
    private ScrapeUnitMessageProducer scrapeUnitMessageProducer;
    @Mock
    private ScrapeScriptFactory scrapeScriptFactory;
    @Mock
    private EvaluationUnitMongoRepository evaluationUnitMongoRepository;
    @Mock
    private ScrapeUnitMongoRepository scrapeUnitMongoRepository;
    @Mock
    private JobMongoRepository jobMongoRepository;
    @Mock
    private MetricsService metricsService;
    @InjectMocks
    JobProcessor jobProcessor;

    @Mock
    ScrapeScript scrapeScript;
    @Mock
    Mono<Job> jobMono;
    @Mock
    Flux<EvaluationUnit> evaluationUnitFlux;
    @Mock
    Flux<ScrapeUnit> scrapeUnitFlux;

    @Test
    void shouldReturnFailedJob_whenScrapeScriptNotFound() {
        Job job = createJob();
        String errorMessage = "Scrape script not found";

        when(scrapeScriptFactory.create(any(), any()))
                .thenThrow(new ScrapeScriptNotFound(errorMessage));
        when(jobMongoRepository.save(any()))
                .thenReturn(jobMono);

        Job processedJob = jobProcessor.process(job);

        assertThat(processedJob.getStatus()).isEqualTo(JobStatus.FAILED);
        assertThat(processedJob.getErrorMessage()).contains(errorMessage);
    }

    @Test
    void shouldReturnJobInProcessingState_whenEverythingGoesWell() {
        Job job = createJob();

        when(scrapeScriptFactory.create(any(), any()))
                .thenReturn(scrapeScript);
        when(jobMongoRepository.save(any()))
                .thenReturn(jobMono);
        when(evaluationUnitMongoRepository.saveAll(ArgumentMatchers.anyCollection()))
                .thenReturn(evaluationUnitFlux);

        Job processedJob = jobProcessor.process(job);

        assertThat(processedJob.getStatus()).isEqualTo(JobStatus.PROCESSING);
        assertThat(processedJob.getErrorMessage()).isNull();
    }

    @Test
    void shouldRerunUnits_whenRerunWasSet() {
        Job job = createJob().toBuilder().rerun(true).build();

        when(scrapeScriptFactory.create(any(), any()))
                .thenReturn(scrapeScript);
        when(jobMongoRepository.save(any()))
                .thenReturn(jobMono);
        when(evaluationUnitMongoRepository.findByStatusAndJobId(any(), any()))
                .thenReturn(evaluationUnitFlux);
        when(scrapeUnitMongoRepository.findByScrapeUnitStatusAndJobId(any(), any()))
                .thenReturn(scrapeUnitFlux);

        Job processedJob = jobProcessor.process(job);

        assertThat(processedJob.getStatus()).isEqualTo(JobStatus.PROCESSING);
    }

    @Test
    void shouldReturnFailedJob_whenUnexpectedErrorOccurs() {
        Job job = createJob();
        String errorMessage = "unexpected error";

        when(scrapeScriptFactory.create(any(), any()))
                .thenReturn(scrapeScript);
        when(jobMongoRepository.save(any()))
                .thenReturn(jobMono);
        when(evaluationUnitMongoRepository.saveAll(ArgumentMatchers.anyCollection()))
                .thenThrow(new RuntimeException(errorMessage));

        Job processedJob = jobProcessor.process(job);

        assertThat(processedJob.getStatus()).isEqualTo(JobStatus.FAILED);
        assertThat(processedJob.getErrorMessage()).contains(errorMessage);
    }

    private Job createJob() {
        return Job.builder()
                .id("test")
                .status(JobStatus.READY)
                .scriptName("invalid_script_name")
                .scriptVersion(1)
                .build();
    }

}