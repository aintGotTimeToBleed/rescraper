package pl.mzapisek.rescraper.eventbusconsumer;

import akka.Done;
import akka.actor.ActorSystem;
import akka.kafka.CommitterSettings;
import akka.kafka.ConsumerSettings;
import akka.kafka.Subscriptions;
import akka.kafka.javadsl.Committer;
import akka.kafka.javadsl.Consumer;
import akka.stream.ActorAttributes;
import akka.stream.Attributes;
import akka.stream.Supervision;
import com.typesafe.config.Config;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.reactive.function.client.WebClient;
import pl.mzapisek.rescraper.common.api.Task;
import pl.mzapisek.rescraper.common.api.TaskMongoRepository;
import pl.mzapisek.rescraper.common.configuration.MongoConfiguration;
import pl.mzapisek.rescraper.common.evaluationprocessor.repository.EvaluationUnitMongoRepository;
import pl.mzapisek.rescraper.common.jobprocessor.repository.JobMongoRepository;
import pl.mzapisek.rescraper.common.protobuf.*;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.shared.mappers.*;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.properties.KafkaConsumerProperties;
import pl.mzapisek.rescraper.common.shared.repository.ScrapeResultMongoRepository;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Log4j2
@Configuration
@Import({MongoConfiguration.class})
@ComponentScan(basePackageClasses = {
        ScrapeResultMongoRepository.class,
        ScrapeUnitMongoRepository.class,
        JobMongoRepository.class,
        EvaluationUnitMongoRepository.class,
        TaskMongoRepository.class
})
@EnableConfigurationProperties({KafkaConsumerProperties.class, KafkaBaseProperties.class})
public class AkkaConfiguration {

    private static final String AKKA_KAFKA_COMMITER_CONFIG = "akka.kafka.committer";
    private static final String AKKA_KAFKA_CONSUMER_CONFIG = "akka.kafka.consumer";

    @Value("${instance.number}")
    private int instanceNumber;

    @Bean
    public ActorSystem actorSystem() {
        return ActorSystem.create();
    }

    @Bean
    public ConsumerSettings<String, byte[]> consumerSettings(KafkaBaseProperties kafkaBaseProperties,
                                                             ActorSystem actorSystem) {

        System.out.println("INSTANCE NUMBER: " + instanceNumber);

        final Config config = actorSystem.settings().config().getConfig(AKKA_KAFKA_CONSUMER_CONFIG);
        return ConsumerSettings.create(config, new StringDeserializer(), new ByteArrayDeserializer())
                .withBootstrapServers(kafkaBaseProperties.getBootstrapServers())
                .withGroupId("eventbus-consumer-" + instanceNumber)
                .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
                .withStopTimeout(Duration.ofSeconds(5));
    }

    @Bean
    public WebClient webClient() {
      return WebClient.create("http://localhost:9999");
    }

    @Bean
    public Attributes attributes() {
        return ActorAttributes.withSupervisionStrategy(
                exception -> {
                    if (exception instanceof com.google.protobuf.InvalidProtocolBufferException) {
                        return (Supervision.Directive) Supervision.resume();
                    } else {
                        return (Supervision.Directive) Supervision.stop();
                    }
                });
    }

    // todo refactor
    @Bean(name = "akkaConsumer")
    public Consumer.DrainingControl<Done> consumer(ConsumerSettings<String, byte[]> consumerSettings,
                                                   ActorSystem actorSystem,
                                                   ScrapeResultMongoRepository scrapeResultMongoRepository,
                                                   KafkaBaseProperties kafkaBaseProperties,
                                                   //Attributes attributes,
                                                   ScrapeUnitMongoRepository scrapeUnitMongoRepository,
                                                   JobMongoRepository jobMongoRepository,
                                                   EvaluationUnitMongoRepository evaluationUnitMongoRepository,
                                                   TaskMongoRepository taskMongoRepository,
                                                   WebClient webClient) {
        CommitterSettings committerSettings = CommitterSettings.create(actorSystem.settings().config().getConfig(AKKA_KAFKA_COMMITER_CONFIG));
        return Consumer.committableSource(consumerSettings, Subscriptions.topics(kafkaBaseProperties.getEventBusTopic()))
                .map(param -> {
                    var record = param.record().topic() + "-" + param.record().partition() + "-" + param.record().offset();
                    var recordValue = param.record().value();
                    switch (new String(param.record().headers().lastHeader("event_type").value())) {
                        case "ScrapeResult":
                            var scrapeResultProto = ScrapeResultProtos.ScrapeResult.parseFrom(recordValue);
                            log.info("scrape result: " + scrapeResultProto.getJobId() + ", " + record);
                            scrapeResultMongoRepository
                                    .save(ScrapeResultMapper.mapProtoToScrapeResult(scrapeResultProto))
                                    .subscribe();
                            break;
                        case "ScrapeUnit":
                            var scrapeUnitProto = ScrapeUnitProtos.ScrapeUnit.parseFrom(recordValue);
                            log.info("scrape unit: " + scrapeUnitProto.getJobId() + ", " + record);
                            scrapeUnitMongoRepository
                                    .save(ScrapeUnitMapper.mapProtoToScrapeUnit(scrapeUnitProto))
                                    .subscribe();
                            break;
                        case "Job":
                            var jobProto = JobProtos.Job.parseFrom(recordValue);
                            log.info("job: " + jobProto.getId() + ", " + record);
                            jobMongoRepository
                                    .save(JobMapper.mapProtoToJob(jobProto))
                                    .subscribe();
                            break;
                        case "EvaluationUnit":
                            var evaluationUnitProto = EvaluationUnitProtos.EvaluationUnit.parseFrom(recordValue);
                            log.info("evaluationUnit: " + evaluationUnitProto.getJobId() + ", " + record);
                            evaluationUnitMongoRepository
                                    .save(EvaluationUnitMapper.mapProtoToEvaluationUnit(evaluationUnitProto))
                                    .subscribe();
                            break;
                        case "Task": // task created event
                            var taskProto = TaskProtos.Task.parseFrom(recordValue);
                            log.info("task: " + record);
                          Task task = TaskMapper.mapTaskProtoToTask(taskProto);
                          taskMongoRepository.save(task).subscribe();

                            // call api to create
                            // WebClient

                          webClient.post()
                            .uri("/tasks/schedule")
                            .body(Mono.just(task), Task.class)
                            .retrieve()
                            .bodyToMono(String.class)
                            .subscribe();

                          break;

                      case "TaskRemoved":
                        var taskRemovedProto = TaskRemovedProtos.TaskRemoved.parseFrom(recordValue);
                        log.info("task removed: " + record);
                        taskMongoRepository.deleteById(taskRemovedProto.getId())
                          .subscribe(unused -> log.info(unused), throwable -> log.error(throwable.getMessage()));

                        webClient.delete()
                          .uri("/tasks/schedule/" + taskRemovedProto.getId())
                          .retrieve()
                          .bodyToMono(String.class)
                          .subscribe();
                    }

                    return param.committableOffset();
                })
                // .withAttributes(attributes)
                // todo increase batch
                .toMat(Committer.sink(committerSettings.withMaxBatch(1)), Consumer::createDrainingControl)
                .run(actorSystem);
    }

//    @DependsOn("akkaConsumer")
//    @Bean
//    public String metrics(@Qualifier("akkaConsumer") Consumer.DrainingControl<Done> consumer) {
//        consumer.getMetrics().thenAccept(System.out::println);
//        return "oko";
//    }

}
