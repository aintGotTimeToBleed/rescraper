package pl.mzapisek.rescraper.eventbusconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventbusConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventbusConsumerApplication.class, args);
    }

}
