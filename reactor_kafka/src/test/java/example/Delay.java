package example;

import lombok.extern.log4j.Log4j2;
import reactor.blockhound.BlockHound;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@Log4j2
public class Delay {

    public static void main(String args[]) {

       // BlockHound.install();

         //ReactorDebugAgent.init();

//        Mono.delay(Duration.ofSeconds(1))
//                .doOnNext(it -> {
//                    try {
//                        Thread.sleep(10);
//                    }
//                    catch (InterruptedException e) {
//                        throw new RuntimeException(e);
//                    }
//                })
//                .block();

        BlockHound.install();
        Flux.range(1, 10)
                //.subscribeOn(Schedulers.boundedElastic())
                .flatMap(integer -> {
                    return Mono.fromCallable(() -> {
                        System.out.println(integer);
                        log.debug("oko");
                        Thread.sleep(1000);
                        return "d";
                    });
                })
                .blockLast();


//        Flux.range(0, Runtime.getRuntime().availableProcessors() * 2)
//                .subscribeOn(Schedulers.parallel())
//                .map(i -> {
//                    CountDownLatch latch = new CountDownLatch(1);
//
//                    Mono.delay(Duration.ofMillis(i * 100))
//                            .subscribe(it -> latch.countDown());
//
//                    try {
//                        latch.await();
//                    } catch (InterruptedException e) {
//                        throw new RuntimeException(e);
//                    }
//
//                    return i;
//                })
//                .blockLast();
//
//        Thread.sleep(5000);

//        //threadSleep(4, 1);
//
//        //timeunitSleep(4, 1);
//
//        delayedServiceTask(5);
//
//        //fixedRateServiceTask(5);
//
//        System.out.println("Done.");
//
//        return;

    }

    public static Mono<String> abc() {
        return Mono.fromCallable(() -> {
            Thread.sleep(1000);
            return "d";
        });
    }

    private static void threadSleep(Integer iterations, Integer secondsToSleep) {

        for (Integer i = 0; i < iterations; i++) {

            System.out.println("This is loop iteration number " + i.toString());

            try {
                Thread.sleep(secondsToSleep * 1000);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }

        }

    }

    private static void timeunitSleep(Integer iterations, Integer secondsToSleep) {

        for (Integer i = 0; i < iterations; i++) {

            System.out.println("This is loop iteration number " + i.toString());

            try {
                TimeUnit.SECONDS.sleep(secondsToSleep);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }

        }

    }

    private static void delayedServiceTask(Integer delayInSeconds) {

        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

        executorService.schedule(Delay::someTask1, delayInSeconds, TimeUnit.SECONDS);

        System.out.println("here");

        executorService.shutdown();
    }

    private static void fixedRateServiceTask(Integer delayInSeconds) {

        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

        ScheduledFuture<?> sf = executorService.scheduleAtFixedRate(Delay::someTask2, 0, delayInSeconds,
                TimeUnit.SECONDS);

        try {
            TimeUnit.SECONDS.sleep(20);
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }

        sf.cancel(true);

        executorService.shutdown();
    }

    private static void someTask1() {
        System.out.println("Task 1 completed.");
    }

    private static void someTask2() {
        System.out.println("Task 2 completed.");
    }

}
