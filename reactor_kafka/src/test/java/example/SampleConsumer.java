/*
 * Copyright (c) 2016-2018 Pivotal Software Inc, All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package example;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.blockhound.BlockHound;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.receiver.ReceiverRecord;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Sample consumer application using Reactive API for Kafka.
 * To run sample consumer
 * <ol>
 * <li> Start Zookeeper and Kafka server
 * <li> Update {@link #BOOTSTRAP_SERVERS} and {@link #TOPIC} if required
 * <li> Create Kafka topic {@link #TOPIC}
 * <li> Send some messages to the topic, e.g. by running {@link SampleProducer}
 * <li> Run {@link SampleConsumer} as Java application with all dependent jars in the CLASSPATH (eg. from IDE).
 * <li> Shutdown Kafka server and Zookeeper when no longer required
 * </ol>
 */
public class SampleConsumer {

    private static final Logger log = LoggerFactory.getLogger(SampleConsumer.class.getName());

    private static final String BOOTSTRAP_SERVERS = "localhost:29092";
    private static final String TOPIC = "demo-topic";

    private final ReceiverOptions<Integer, String> receiverOptions;
    private final SimpleDateFormat dateFormat;

    public SampleConsumer(String bootstrapServers) {

        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
//         props.put(ConsumerConfig.CLIENT_ID_CONFIG, "sample-consumer");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "sample-group-2");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, "5000");
        receiverOptions = ReceiverOptions.create(props);
        dateFormat = new SimpleDateFormat("HH:mm:ss:SSS z dd MMM yyyy");
    }

    public Disposable consumeMessages(String topic, CountDownLatch latch) {

        ReceiverOptions<Integer, String> options = receiverOptions.subscription(Collections.singleton(topic))
                .addAssignListener(partitions -> log.debug("onPartitionsAssigned {}", partitions))
                .addRevokeListener(partitions -> log.debug("onPartitionsRevoked {}", partitions));
        KafkaReceiver<Integer, String> receiver = KafkaReceiver.create(options);
//        Flux<ReceiverRecord<Integer, String>> kafkaFlux = receiver.receive();
//        return kafkaFlux
//                .subscribe(record -> {
////                    ReceiverOffset offset = record.receiverOffset();
////                    System.out.printf("Received message: topic-partition=%s offset=%d timestamp=%s key=%d value=%s\n",
////                            offset.topicPartition(),
////                            offset.offset(),
////                            dateFormat.format(new Date(record.timestamp())),
////                            record.key(),
////                            record.value());
////                    offset.acknowledge();
////                    latch.countDown();
//                });

        return receiver
                .receive()
//                .log()
//                .delayElements(Duration.ofSeconds(1))
//                .flatMap(record -> {
//                    System.out.println(record.value());
//                    log.info("oko");
//                    try {
//                        Thread.sleep(5000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    record.receiverOffset().acknowledge();
//                    latch.countDown();
//                    return Mono.just(record);
//                })
//                .flatMap(record -> {
//                    record.receiverOffset().acknowledge();
//                    latch.countDown();
//                    return receiver.doOnConsumer(consumer -> {
//                        log.info("pause: " + TOPIC + record.partition());
//                        TopicPartition topicPartition = new TopicPartition(TOPIC, record.partition());
//                        consumer.pause(Collections.singleton(topicPartition));
//                        return record;
//                    });
//                })
                .retry()
                .limitRate(1)


                // try out 1
//                //.publishOn(Schedulers.single(), 16)
//                .flatMap(record -> {
//                    return receiver.doOnConsumer(consumer -> {
////                        log.info("pause: " + TOPIC + record.partition());
//                        TopicPartition topicPartition = new TopicPartition(TOPIC, record.partition());
//                        consumer.pause(Collections.singleton(topicPartition));
//                        return record;
//                    })
////                            .subscribeOn(Schedulers.boundedElastic())
//                            .flatMap(record1 -> {
//                        return create(record)
//                                .subscribeOn(Schedulers.boundedElastic())
//                                ;
////                        return Mono.just(record)
////                                .subscribeOn(Schedulers.newBoundedElastic(4,4,"klop"))
////                                .flatMap(this::create);
//                    }).doOnSuccess(record1 -> {
//                        record.receiverOffset().acknowledge();
//                        receiver.doOnConsumer(consumer -> {
////                            log.info("resume: " + TOPIC + record.partition());
//                            TopicPartition topicPartition = new TopicPartition(TOPIC, record.partition());
//                            consumer.resume(Collections.singleton(topicPartition));
//                            return consumer.paused();
//                        }).flatMap(topicPartitions -> {
//                            topicPartitions.forEach(topicPartition -> {
////                                System.out.println(topicPartition.partition());
//                            });
//                            return Mono.just(topicPartitions.size());
//                        }).subscribe();
//                    });
//                })


//                // try out 2
                .flatMap(record -> {
                    return receiver.doOnConsumer(consumer -> {
//                        log.info("pause: " + TOPIC + record.partition());
                        TopicPartition topicPartition = new TopicPartition(TOPIC, record.partition());
                        consumer.pause(Collections.singleton(topicPartition));
                        return record;
                    });
                })
                .publishOn(Schedulers.boundedElastic())
                .flatMap(record -> {
//                    return Mono.just(record)
//                            .delayUntil(record1 -> create(record1))
//                            .subscribeOn(Schedulers.boundedElastic());
                    return create(record);
                            // .subscribeOn(Schedulers.boundedElastic());
//                     return Mono.just(record).subscribeOn(Schedulers.single()).flatMap(record1 -> create(record1));
//                    return Mono.just(record).subscribeOn(Schedulers.single()).flatMap(record1 -> create(record1));
                })
                .flatMap(record -> {
//                    log.info("message: " + record.value());
                    record.receiverOffset().acknowledge();
                    return Mono.just(record);
                })
                .flatMap(record -> {
                    return receiver.doOnConsumer(consumer -> {
//                        log.info("resume: " + TOPIC + record.partition());
                        TopicPartition topicPartition = new TopicPartition(TOPIC, record.partition());
                        consumer.resume(Collections.singleton(topicPartition));
                        return consumer.paused();
                    });
                })
                .map(topicPartitions -> {
                    topicPartitions.forEach(topicPartition -> {
                        // System.out.println(topicPartition.partition());
                    });
                    return topicPartitions.size();
                })
                .onErrorContinue((throwable, o) -> throwable.printStackTrace())
                .subscribe();
//                        .subscribe(record -> {
//                    ReceiverOffset offset = record.receiverOffset();
//                    System.out.printf("Received message: topic-partition=%s offset=%d timestamp=%s key=%d value=%s\n",
//                            offset.topicPartition(),
//                            offset.offset(),
//                            dateFormat.format(new Date(record.timestamp())),
//                            record.key(),
//                            record.value());
//                    offset.acknowledge();
//                    latch.countDown();
//                });
    }

    public Mono<ReceiverRecord<Integer, String>> create(ReceiverRecord<Integer, String> record) {
//        log.info("process");
        return Mono.fromCallable(() -> {
//            Awaitility
//                    .await()
//                    .atMost(Duration.FIVE_SECONDS)
//                    .until(() -> {
//                        while (true) {
//                            // noop
//                        }
//                    });
//            try {
                log.info("start");
                 Thread.sleep(10_000);
                 log.info("end");
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            log.info(record.value());
            return record;
        });
    }


    public static void main(String[] args) throws Exception {
//        BlockHound.install();
        int count = 20;
        CountDownLatch latch = new CountDownLatch(count);
        SampleConsumer consumer = new SampleConsumer(BOOTSTRAP_SERVERS);
        Disposable disposable = consumer.consumeMessages(TOPIC, latch);
        latch.await(120, TimeUnit.SECONDS);
        disposable.dispose();
    }
}
