package pl.mzapisek.rescraper.reactor_kafka;

import java.util.HashMap;
import java.util.Map;

import io.micrometer.core.instrument.MockClock;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.springframework.boot.actuate.autoconfigure.metrics.CompositeMeterRegistryAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.fasterxml.jackson.core.JsonProcessingException;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import org.testcontainers.utility.DockerImageName;
import pl.mzapisek.rescraper.common.configuration.RescraperConfiguration;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.shared.repository.ScrapeResultMongoRepository;
import pl.mzapisek.rescraper.common.api.CompleteProcessService;
import pl.mzapisek.rescraper.common.shared.services.JsonUtils;
import pl.mzapisek.rescraper.common.shared.services.MetricsService;
import pl.mzapisek.rescraper.common.shared.services.RedisService;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.reactor_kafka.configs.ReactorKafkaConsumerBaseConfiguration;
import pl.mzapisek.rescraper.reactor_kafka.scrapeprocessor.ScrapeProcessor;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;
import reactor.kafka.sender.SenderRecord;

@Disabled
@Import({RescraperConfiguration.class,
                ReactorKafkaConsumerBaseConfiguration.class})
@ContextConfiguration(classes = {
        MockClock.class,
        CompositeMeterRegistryAutoConfiguration.class,
        MetricsAutoConfiguration.class,
        JacksonAutoConfiguration.class,
        ReactiveKafkaConsumer.class,
        JsonUtils.class,
        ScrapeProcessor.class,
        ScrapeScriptFactory.class,
        ScrapeUnitMongoRepository.class,
        MetricsService.class,
        RedisService.class,
        ScrapeResultMongoRepository.class,
        CompleteProcessService.class
})
@SpringBootTest
@Testcontainers
public abstract class AbstractConsumerIntegrationTest {
// todo create extenstion instead of abstract class

    @Container
    protected static final KafkaContainer KAFKA = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka"));
    @Container
    protected static final MongoDBContainer MONGO = new MongoDBContainer(DockerImageName.parse("mongo:4.0.10"));
    @Container
    protected static final GenericContainer<?> REDIS =
            new GenericContainer<>(DockerImageName.parse("redis:5.0.3-alpine")).withExposedPorts(6379);
    protected static KafkaSender<String, String> kafkaSender;
    private ObjectMapper objectMapper = new ObjectMapper();

    @BeforeAll
    static void init() {
        kafkaSender = KafkaSender.create(SenderOptions.create(producerProps()));
    }

    protected void sendScrapeUnit(ScrapeUnit scrapeUnit, String topicName) throws JsonProcessingException {
        kafkaSender
                .send(Mono.just(SenderRecord.create(new ProducerRecord<>(topicName,
                        objectMapper.writeValueAsString(scrapeUnit)), null)))
                .subscribe();
    }

    protected static Map<String, Object> producerProps() {
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA.getBootstrapServers());
        props.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, String.valueOf(100));
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        return props;
    }

}
