package pl.mzapisek.rescraper.reactor_kafka.dataprovider;

import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;

import java.time.LocalDateTime;
import java.util.UUID;

public class ScrapeUnitDataProvider {

    public static final String TEST_SCRIPT_NAME = "TestScript";

    public static ScrapeUnit createScrapeUnit(String id, String testScriptName, ScrapeUnitStatus scrapeUnitStatus) {
        return ScrapeUnit.builder()
                .id(id != null ? id : "scrape_id")
                .jobId("job_id")
                .evaluationId("evaluation_id")
                .scriptName(testScriptName != null ? testScriptName : TEST_SCRIPT_NAME)
                .scrapeUnitStatus(scrapeUnitStatus != null ? scrapeUnitStatus : ScrapeUnitStatus.READY)
                .scriptVersion(1)
                .build();
    }

    public static ScrapeResult createsScrapeResult(String scrapeId) {
        return ScrapeResult.builder()
                .id(UUID.randomUUID().toString())
                .jobId("jobId")
                .evaluationId("evaluationId")
                .scrapeUnitId(scrapeId != null ? scrapeId : "scrape_id")
                .urlParam("param")
                .jsonValue("{\"license\": \"test\"}")
                .createdAt(LocalDateTime.now())
                .build();
    }

}
