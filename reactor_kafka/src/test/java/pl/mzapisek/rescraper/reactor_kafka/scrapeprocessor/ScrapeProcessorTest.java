package pl.mzapisek.rescraper.reactor_kafka.scrapeprocessor;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;
import pl.mzapisek.rescraper.common.shared.exceptions.ScrapeScriptNotFound;
import pl.mzapisek.rescraper.common.shared.repository.ScrapeResultMongoRepository;
import pl.mzapisek.rescraper.common.shared.services.MetricsService;
import pl.mzapisek.rescraper.common.shared.services.RedisService;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.reactor_kafka.eventbus.EventBusProducer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@Disabled
@ExtendWith(MockitoExtension.class)
class ScrapeProcessorTest {

    @Mock
    private ScrapeScriptFactory scrapeScriptFactory;
    @Mock
    private ScrapeUnitMongoRepository scrapeUnitMongoRepository;
    @Mock
    private MetricsService metricsService;
    @Mock
    private RedisService redisService;
    @Mock
    private ScrapeResultMongoRepository scrapeResultMongoRepository;
    @Mock
    private EventBusProducer eventBusProducer;
    @InjectMocks
    private ScrapeProcessor scrapeProcessor;

    @Mock
    ScrapeScript scrapeScript;

    @Test
    void shouldReturnCancelledScrapeUnit_whenJobWasCancelled() {
        var scrapeUnit = createScrapeUnit();
        when(redisService.isJobCancelled(scrapeUnit.getJobId()))
                .thenReturn(true);
        when(scrapeUnitMongoRepository.save(any(ScrapeUnit.class)))
                .thenReturn(Mono.just(createScrapeUnit()));

        StepVerifier.create(scrapeProcessor.process(scrapeUnit))
                .expectNext(scrapeUnit)
                .verifyComplete();

        ArgumentCaptor<ScrapeUnit> argumentCaptor = ArgumentCaptor.forClass(ScrapeUnit.class);
        verify(scrapeUnitMongoRepository).save(argumentCaptor.capture());
        ScrapeUnit captured = argumentCaptor.getValue();
        assertThat(captured.getScrapeUnitStatus()).isEqualTo(ScrapeUnitStatus.CANCELLED);
        assertThat(captured.getFinishedAt()).isNotNull();
    }

    @SuppressWarnings("unchecked")
    @Test
    void shouldReturnScrapeUnitWithSuccessStatus_whenEverythingGoesFine() {
        var scrapeUnit = createScrapeUnit();
        when(redisService.isJobCancelled(scrapeUnit.getJobId()))
                .thenReturn(false);
        when(scrapeScriptFactory.create(any(), any()))
                .thenReturn(scrapeScript);
        when(scrapeResultMongoRepository.saveAll(any(List.class)))
                .thenReturn(Flux.just(ScrapeResult.builder().build()));
        when(scrapeUnitMongoRepository.save(any(ScrapeUnit.class)))
                .thenReturn(Mono.just(createScrapeUnit()));

        StepVerifier.create(scrapeProcessor.process(scrapeUnit))
                .expectNext(scrapeUnit)
                .verifyComplete();

        ArgumentCaptor<ScrapeUnit> argumentCaptor = ArgumentCaptor.forClass(ScrapeUnit.class);
        verify(scrapeUnitMongoRepository).save(argumentCaptor.capture());
        ScrapeUnit captured = argumentCaptor.getValue();
        assertThat(captured.getScrapeUnitStatus()).isEqualTo(ScrapeUnitStatus.SUCCESS);
        assertThat(captured.getFinishedAt()).isNotNull();
    }

    @Test
    void shouldReturnScrapeUnitWithFailedStatus_whenScrapeScriptNotFound() {
        var scrapeUnit = createScrapeUnit();
        when(redisService.isJobCancelled(scrapeUnit.getJobId()))
                .thenReturn(false);
        when(scrapeScriptFactory.create(any(), any()))
                .thenThrow(new ScrapeScriptNotFound("not found"));
        when(scrapeUnitMongoRepository.save(any(ScrapeUnit.class)))
                .thenReturn(Mono.just(createScrapeUnit()));

        StepVerifier.create(scrapeProcessor.process(scrapeUnit))
                .expectNext(scrapeUnit)
                .verifyComplete();

        ArgumentCaptor<ScrapeUnit> argumentCaptor = ArgumentCaptor.forClass(ScrapeUnit.class);
        verify(scrapeUnitMongoRepository).save(argumentCaptor.capture());
        ScrapeUnit captured = argumentCaptor.getValue();
        assertThat(captured.getScrapeUnitStatus()).isEqualTo(ScrapeUnitStatus.FAILED);
        assertThat(captured.getErrorMessage()).isEqualTo("not found");
        assertThat(captured.getFinishedAt()).isNotNull();
    }

    @SuppressWarnings("unchecked")
    @Test
    void shouldReturnScrapeUnitWithFailedStatus_whenRandomExceptionIsThrownBeforeThen() {
        var scrapeUnit = createScrapeUnit();
        when(redisService.isJobCancelled(scrapeUnit.getJobId()))
                .thenReturn(false);
        when(scrapeScriptFactory.create(any(), any()))
                .thenReturn(scrapeScript);
        when(scrapeResultMongoRepository.saveAll(any(List.class)))
                .thenReturn(Flux.just(ScrapeResult.builder().build()));
        when(scrapeUnitMongoRepository.save(any(ScrapeUnit.class)))
                .thenReturn(Mono.error(new RuntimeException("random exception")), Mono.just(createScrapeUnit()));

        StepVerifier.create(scrapeProcessor.process(scrapeUnit))
                .expectNext(scrapeUnit)
                .verifyComplete();

        ArgumentCaptor<ScrapeUnit> argumentCaptor = ArgumentCaptor.forClass(ScrapeUnit.class);
        verify(scrapeUnitMongoRepository, times(2)).save(argumentCaptor.capture());
        List<ScrapeUnit> captured = argumentCaptor.getAllValues();

        assertThat(captured.get(0).getScrapeUnitStatus()).isEqualTo(ScrapeUnitStatus.SUCCESS);
        assertThat(captured.get(1).getFinishedAt()).isNotNull();

        assertThat(captured.get(1).getScrapeUnitStatus()).isEqualTo(ScrapeUnitStatus.FAILED);
        assertThat(captured.get(1).getErrorMessage()).isEqualTo("random exception");
        assertThat(captured.get(1).getFinishedAt()).isNotNull();
        assertThat(captured.get(0).getFinishedAt()).isNotEqualTo(captured.get(1).getFinishedAt());
    }

    @SuppressWarnings("unchecked")
    @Test
    void shouldThrowAnError_whenExceptionOccursInOnErrorResume() {
        var scrapeUnit = createScrapeUnit();
        when(scrapeScriptFactory.create(any(), any()))
                .thenReturn(scrapeScript);
        when(scrapeResultMongoRepository.saveAll(any(List.class)))
                .thenReturn(Flux.just(ScrapeResult.builder().build()));
        when(scrapeUnitMongoRepository.save(any(ScrapeUnit.class)))
                .thenReturn(
                        Mono.error(new RuntimeException("random exception")),
                        Mono.error(new RuntimeException("on error random exception")));

        StepVerifier.create(scrapeProcessor.process(scrapeUnit))
                .expectErrorMessage("on error random exception")
                .verify();
    }

    // should throw ScrapeUnitAlreadyProcessed

    private ScrapeUnit createScrapeUnit() {
        return ScrapeUnit.builder()
                .jobId("jobId")
                .evaluationId("evaluationId")
                .id("scrapeId")
                .scrapeUnitStatus(ScrapeUnitStatus.READY)
                .scriptName("test")
                .scriptVersion(1)
                .urlParams(Collections.emptyList())
                .build();
    }

}