package pl.mzapisek.rescraper.reactor_kafka.helpers;

import com.mongodb.MongoTimeoutException;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoCollection;
import com.mongodb.reactivestreams.client.MongoDatabase;
import org.bson.Document;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class MongoIntegrationHelper {

	public static ObservableSubscriber<Long> countDocuments(String host, int port, String dbName, String collectionName) {
		MongoClient mongoClient = MongoClients.create("mongodb://" + host + ":" + port);
		MongoDatabase scraper = mongoClient.getDatabase(dbName);
		MongoCollection<Document> scrapeUnit = scraper.getCollection(collectionName);
		ObservableSubscriber<Long> subscriber = new ObservableSubscriber<>();
		scrapeUnit.countDocuments().subscribe(subscriber);
		subscriber.await();
		return subscriber;
	}


	public static class ObservableSubscriber<T> implements Subscriber<T> {
		private final List<T> received;
		private final List<Throwable> errors;
		private final CountDownLatch latch;
		private volatile Subscription subscription;
		private volatile boolean completed;

		ObservableSubscriber() {
			this.received = new ArrayList<T>();
			this.errors = new ArrayList<Throwable>();
			this.latch = new CountDownLatch(1);
		}

		@Override
		public void onSubscribe(final Subscription s) {
			subscription = s;
		}

		@Override
		public void onNext(final T t) {
			received.add(t);
		}

		@Override
		public void onError(final Throwable t) {
			errors.add(t);
			onComplete();
		}

		@Override
		public void onComplete() {
			completed = true;
			latch.countDown();
		}

		public Subscription getSubscription() {
			return subscription;
		}

		public List<T> getReceived() {
			return received;
		}

		public Throwable getError() {
			if (errors.size() > 0) {
				return errors.get(0);
			}
			return null;
		}

		public boolean isCompleted() {
			return completed;
		}

		public List<T> get(final long timeout, final TimeUnit unit) throws Throwable {
			return await(timeout, unit).getReceived();
		}

		public ObservableSubscriber<T> await() {
			try {
				return await(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
			} catch (Throwable throwable) {
				throwable.printStackTrace();
				return null;
			}
		}

		public ObservableSubscriber<T> await(final long timeout, final TimeUnit unit) throws Throwable {
			subscription.request(Integer.MAX_VALUE);
			if (!latch.await(timeout, unit)) {
				throw new MongoTimeoutException("Publisher onComplete timed out");
			}
			if (!errors.isEmpty()) {
				throw errors.get(0);
			}
			return this;
		}
	}

}
