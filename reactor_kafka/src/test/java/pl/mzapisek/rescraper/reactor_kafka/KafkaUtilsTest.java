package pl.mzapisek.rescraper.reactor_kafka;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import pl.mzapisek.rescraper.reactor_kafka.configs.ReactorKafkaProducerBaseConfiguration;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

@Disabled
@SpringBootTest(
        classes = {
                ReactorKafkaProducerBaseConfiguration.class,
                KafkaUtils.class},
        properties = {
                "kafka.base.job-topic=rescraper-jobs",
                "kafka.base.evaluation-unit-topic=rescraper-evaluation-units",
                "kafka.base.scrape-unit-topic=rescraper-scrape-units",
                "kafka.base.client-id=rescraper-scrape-processor",
                "kafka.producer.acks=all",
                "kafka.producer.batch-size=100"})
@Testcontainers
@DirtiesContext
class KafkaUtilsTest {

    private static final String TEST_TOPIC_NAME = "test";

    @Autowired
    KafkaUtils kafkaUtils;

    // kafka, should be unified with other integration tests, now we have to use different names for static kafka env
    @Container
    private static final KafkaContainer KAFKA2 = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka"));

    @DynamicPropertySource
    static void dynamicProperties(DynamicPropertyRegistry registry) {
        registry.add("kafka.base.bootstrap-servers", KAFKA2::getBootstrapServers);
    }

    @BeforeAll
    static void setUp() {
        AdminClient.create(Map.of(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA2.getBootstrapServers()))
                .createTopics(List.of(new NewTopic(TEST_TOPIC_NAME, 5, (short) 1)));
    }

    @Test
    void shouldGetCorrectAmountOfRandomPartitions() {
        List<Integer> partitions = new ArrayList<>();

        kafkaUtils.getRandomPartitions(TEST_TOPIC_NAME, 10).subscribe(partitions::addAll);

        await().pollDelay(Duration.ofSeconds(2))
                .timeout(Duration.ofSeconds(3))
                .until(() -> partitions.size() == 5);

        assertThat(partitions).hasSize(5);
    }

}