package pl.mzapisek.rescraper.reactor_kafka;

import io.micrometer.core.instrument.MockClock;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.metrics.CompositeMeterRegistryAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.fasterxml.jackson.core.JsonProcessingException;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import org.testcontainers.utility.DockerImageName;
import pl.mzapisek.rescraper.common.api.CompleteProcessService;
import pl.mzapisek.rescraper.common.configuration.RescraperConfiguration;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.shared.Processor;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.repository.ScrapeResultMongoRepository;
import pl.mzapisek.rescraper.common.shared.services.*;
import pl.mzapisek.rescraper.reactor_kafka.configs.ReactorKafkaConsumerBaseConfiguration;
import pl.mzapisek.rescraper.reactor_kafka.configs.ReactorKafkaProducerBaseConfiguration;
import pl.mzapisek.rescraper.reactor_kafka.dataprovider.ScrapeUnitDataProvider;
import pl.mzapisek.rescraper.reactor_kafka.eventbus.EventBusProducer;
import pl.mzapisek.rescraper.reactor_kafka.helpers.MongoIntegrationHelper;
import pl.mzapisek.rescraper.reactor_kafka.scrapeprocessor.ScrapeProcessor;
import reactor.core.Disposable;
import reactor.core.publisher.Mono;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;
import reactor.kafka.sender.SenderRecord;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled
@Import({ReactiveKafkaConsumerIntegrationTest.TestScriptConfiguration.class,
        RescraperConfiguration.class,
        ReactorKafkaConsumerBaseConfiguration.class,
        ReactorKafkaProducerBaseConfiguration.class
})
@ContextConfiguration(classes = {
        MockClock.class,
        CompositeMeterRegistryAutoConfiguration.class,
        MetricsAutoConfiguration.class,
        JacksonAutoConfiguration.class,
        JsonUtils.class,
        ScrapeProcessor.class,
        ScrapeScriptFactory.class,
        ScrapeUnitMongoRepository.class,
        MetricsService.class,
        RedisService.class,
        ScrapeResultMongoRepository.class,
        CompleteProcessService.class,
        ReactorKafkaConsumerBaseConfiguration.class,
        EventBusProducer.class
})
@SpringBootTest(properties = {
        "mongo.database-name=scraper",
        "kafka.base.scrape-unit-topic=rescraper-scrape-units",
        "kafka.base.client-id=rescraper-scrape-processor",
        "kafka.consumer.consumer-group=scrape-processor",
        "kafka.consumer.auto-offset-reset=latest",
        "kafka.consumer.auto-commit-interval=100",
        "kafka.consumer.max-poll-records=100",
        "kafka.consumer.max-poll-interval-ms=15000",
        "redis.timeout.ms=100"
})
@Testcontainers
@DirtiesContext
class ReactiveKafkaConsumerIntegrationTest {

    private static final int SLEEP_SEC = 10;
    private static final String TEST_SCRIPT_NAME = "TestScript";
    private static final String SCRAPE_UNIT_TOPIC = "rescraper-scrape-units";
    @Container
    private static final KafkaContainer KAFKA = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka"));
    @Container
    private static final MongoDBContainer MONGO = new MongoDBContainer(DockerImageName.parse("mongo:4.0.10"));
    @Container
    private static final GenericContainer<?> REDIS =
            new GenericContainer<>(DockerImageName.parse("redis:5.0.3-alpine")).withExposedPorts(6379);
    private static KafkaSender<String, String> kafkaSender;

    @Autowired
    KafkaBaseProperties kafkaBaseProperties;
    @Autowired
    JsonUtils jsonUtils;
    @Autowired
    Processor<ScrapeUnit, Mono<ScrapeUnit>> scrapeProcessor;
    @Autowired
    ReceiverOptions<String, String> receiverOptions;

    private ObjectMapper objectMapper = new ObjectMapper();

    @DynamicPropertySource
    static void dynamicProperties(DynamicPropertyRegistry registry) {
        registry.add("mongo.port", MONGO::getFirstMappedPort);
        registry.add("mongo.host", MONGO::getHost);
        registry.add("kafka.base.bootstrap-servers", KAFKA::getBootstrapServers);
        registry.add("redis.host", REDIS::getHost);
        registry.add("redis.port", () -> REDIS.getMappedPort(6379));
        registry.add("kafka.consumer.max.poll.interval.ms", () -> "10000");
        registry.add("logging.level.root", () -> "INFO");
    }

    @BeforeAll
    static void init() {
        kafkaSender = KafkaSender.create(SenderOptions.create(producerProps()));
        // todo create topics
    }

    public static Map<String, Object> producerProps() {
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA.getBootstrapServers());
        props.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, String.valueOf(100));
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        return props;
    }

    @Test
    void shouldProcessOnlyOneMessageAtATime() throws InterruptedException, JsonProcessingException {
        ReactiveKafkaConsumer<ScrapeUnit> reactiveKafkaConsumer =
                new ReactiveKafkaConsumer<>(kafkaBaseProperties.getScrapeUnitTopic(), scrapeProcessor, jsonUtils.createScrapeUnitFunction(), receiverOptions);

        AtomicInteger atomicInteger = new AtomicInteger();

        Disposable subscriber = reactiveKafkaConsumer
                .createFlux()
                .subscribe(stringStringReceiverRecord -> atomicInteger.incrementAndGet());

        // wait for reactiveKafkaConsumer to join consumer group
        Thread.sleep(5000);

        sendScrapeUnit(ScrapeUnitDataProvider.createScrapeUnit("1", TEST_SCRIPT_NAME, ScrapeUnitStatus.READY));
        sendScrapeUnit(ScrapeUnitDataProvider.createScrapeUnit("2", TEST_SCRIPT_NAME, ScrapeUnitStatus.READY));
        sendScrapeUnit(ScrapeUnitDataProvider.createScrapeUnit("3", TEST_SCRIPT_NAME, ScrapeUnitStatus.READY));

        Awaitility.await()
                .pollDelay(Duration.ofSeconds(SLEEP_SEC + 1))
                .atMost(Duration.ofSeconds(SLEEP_SEC + SLEEP_SEC / 2))
                .until(() -> atomicInteger.get() == 1);

        var scrapeUnitCollectionCountSubscriber = MongoIntegrationHelper.countDocuments(MONGO.getHost(),
                MONGO.getFirstMappedPort(), "scraper", "scrapeUnit");
        assertThat(scrapeUnitCollectionCountSubscriber.getReceived()).isNotEmpty();
        assertThat(scrapeUnitCollectionCountSubscriber.getReceived().get(0)).isEqualTo(1);
        assertThat(atomicInteger.get()).isEqualTo(1);

        // clean up
        subscriber.dispose();
        Thread.sleep(5000);
    }

    void sendScrapeUnit(ScrapeUnit scrapeUnit) throws JsonProcessingException {
        kafkaSender
                .send(Mono.just(SenderRecord.create(new ProducerRecord<>(SCRAPE_UNIT_TOPIC, objectMapper.writeValueAsString(scrapeUnit)), null)))
                .subscribe();
    }

    @TestConfiguration
    static class TestScriptConfiguration {

        @Bean("TestScript")
        public ScrapeScript testScript() {
            return new ScrapeScript() {
                @Override
                public String getScriptName() {
                    return TEST_SCRIPT_NAME;
                }

                @Override
                public Integer getVersion() {
                    return 1;
                }

                @Override
                public String getHeader() {
                    return null;
                }

                @Override
                public String getRow(String json) {
                    return null;
                }

                @Override
                public List<EvaluationUnit> process(Job job) {
                    return Collections.emptyList();
                }

                @Override
                public List<ScrapeUnit> process(EvaluationUnit evaluationUnit) {
                    return Collections.emptyList();
                }

                @Override
                public List<ScrapeResult> process(ScrapeUnit scrapeUnit) {
                    if (scrapeUnit.getId().equals("5")) {
                        throw new RuntimeException("5");
                    }
                    try {
                        Thread.sleep(SLEEP_SEC * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return List.of(ScrapeUnitDataProvider.createsScrapeResult(scrapeUnit.getId()));
                }

                @Override
                public Map<String, String> getDefaultParams() {
                    return null;
                }

                @Override
                public List<String> getDefaultUrlParams() {
                    return null;
                }
            };
        }
    }

}
