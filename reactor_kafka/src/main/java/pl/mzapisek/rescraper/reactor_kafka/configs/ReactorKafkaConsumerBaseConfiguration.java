package pl.mzapisek.rescraper.reactor_kafka.configs;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.properties.KafkaConsumerProperties;
import reactor.kafka.receiver.ReceiverOptions;

import java.util.HashMap;

@EnableConfigurationProperties({KafkaConsumerProperties.class, KafkaBaseProperties.class})
@RequiredArgsConstructor
public class ReactorKafkaConsumerBaseConfiguration {

    private final KafkaBaseProperties kafkaBaseProperties;
    private final KafkaConsumerProperties kafkaConsumerProperties;

    @Bean
    public ReceiverOptions<String, String> kafkaReceiverOptions() {
        HashMap<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, kafkaConsumerProperties.getMaxPollIntervalMs());
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBaseProperties.getBootstrapServers());
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, kafkaBaseProperties.getClientId());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaConsumerProperties.getConsumerGroup());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, kafkaConsumerProperties.getAutoOffsetReset());

        return ReceiverOptions.create(props);
    }

}
