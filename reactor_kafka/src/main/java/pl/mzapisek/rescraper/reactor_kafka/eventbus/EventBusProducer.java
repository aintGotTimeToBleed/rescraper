package pl.mzapisek.rescraper.reactor_kafka.eventbus;

import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.protobuf.*;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderRecord;
import reactor.kafka.sender.SenderResult;

import java.util.List;

// refactor
@Service
@Log4j2
public class EventBusProducer {

	private final KafkaSender<String, byte[]> kafkaSender;
	private final KafkaBaseProperties kafkaBaseProperties;

	public EventBusProducer(@Qualifier("eventBusKafkaSender") KafkaSender<String, byte[]> kafkaSender,
							KafkaBaseProperties kafkaBaseProperties) {
		this.kafkaSender = kafkaSender;
		this.kafkaBaseProperties = kafkaBaseProperties;
	}

	public Flux<SenderResult<byte[]>> sendJob(JobProtos.Job job) {
		return kafkaSender.send(Mono.just(createSenderRecord(job.getId(), job.toByteArray(), "Job")));
	}

	public Flux<SenderResult<byte[]>> sendEvaluationUnits(List<EvaluationUnitProtos.EvaluationUnit> evaluationUnits) {
		return kafkaSender.send(Flux.fromIterable(evaluationUnits)
				.map(evaluationUnit -> createSenderRecord(evaluationUnit.getJobId(), evaluationUnit.toByteArray(), "EvaluationUnit")));
	}

	public Flux<SenderResult<byte[]>> sendScrapeUnit(ScrapeUnitProtos.ScrapeUnit scrapeUnit) {
		return kafkaSender.send(Mono.just(createSenderRecord(scrapeUnit.getJobId(), scrapeUnit.toByteArray(), "ScrapeUnit")));
	}

	public Flux<SenderResult<byte[]>> sendScrapeUnits(List<ScrapeUnitProtos.ScrapeUnit> scrapeUnits) {
		return kafkaSender.send(Flux.fromIterable(scrapeUnits)
				.map(scrapeUnit -> createSenderRecord(scrapeUnit.getJobId(), scrapeUnit.toByteArray(), "ScrapeUnit")));
	}

	public Flux<SenderResult<byte[]>> sendScrapeResults(List<ScrapeResultProtos.ScrapeResult> scrapeResults) {
		return kafkaSender.send(Flux.fromIterable(scrapeResults)
				.map(scrapeResult -> createSenderRecord(scrapeResult.getJobId(), scrapeResult.toByteArray(), "ScrapeResult")));
	}

	public Flux<SenderResult<byte[]>> sendTask(TaskProtos.Task task) {
		return kafkaSender.send(Mono.just(createTaskRecord(task.toByteArray(), task.getTaskName(), "Task")));
	}

	public Flux<SenderResult<byte[]>> sendTaskRemovedEvent(TaskRemovedProtos.TaskRemoved taskRemoved, String taskName) {
		return kafkaSender.send(Mono.just(createTaskRecord(taskRemoved.toByteArray(), taskName, "TaskRemoved")));
	}

	// sendTaskDeletedEvent
	// sendTaskCreatedEvent

	private SenderRecord<String, byte[], byte[]> createSenderRecord(String jobId, byte[] msg, String eventName) {
		return SenderRecord.create(new ProducerRecord<>(kafkaBaseProperties.getEventBusTopic(), null,
				jobId, msg, List.of(new RecordHeader("event_type", eventName.getBytes()))), msg);
	}

	private SenderRecord<String, byte[], byte[]> createTaskRecord(byte[] msg, String taskName, String eventName) {
		return SenderRecord.create(new ProducerRecord<>(kafkaBaseProperties.getEventBusTopic(),
				null, taskName, msg, List.of(new RecordHeader("event_type", eventName.getBytes()))), msg);
	}


}
