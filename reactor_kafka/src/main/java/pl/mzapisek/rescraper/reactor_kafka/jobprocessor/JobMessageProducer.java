package pl.mzapisek.rescraper.reactor_kafka.jobprocessor;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.producer.ProducerRecord;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.services.JsonUtils;
import pl.mzapisek.rescraper.reactor_kafka.KafkaUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderRecord;
import reactor.kafka.sender.SenderResult;

import java.util.Objects;

@Log4j2
@RequiredArgsConstructor
public class JobMessageProducer {

    private final KafkaSender<String, String> kafkaSender;
    private final KafkaBaseProperties kafkaBaseProperties;
    private final JsonUtils jsonUtils;

    public Flux<SenderResult<String>> send(Job job) {
        Objects.requireNonNull(job);

        return kafkaSender
                .send(Mono.just(createSenderRecord(jsonUtils.createJsonTry(job).get(),
                        KafkaUtils.getRandomPartition(job.getAssignedPartitions()))))
                .doOnComplete(() -> log.info("job sent: {}", job));
    }

    private SenderRecord<String, String, String> createSenderRecord(String msg, Integer partition) {
        return SenderRecord.create(new ProducerRecord<>(kafkaBaseProperties.getJobTopic(), partition, null, msg), msg);
    }

}
