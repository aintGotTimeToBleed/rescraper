package pl.mzapisek.rescraper.reactor_kafka.evaluationprocessor;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({EvaluationProcessorConfiguration.class})
public @interface EnableReactorKafkaEvaluationProcessor {
}
