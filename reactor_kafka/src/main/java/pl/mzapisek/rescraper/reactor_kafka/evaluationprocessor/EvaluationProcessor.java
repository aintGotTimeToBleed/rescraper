package pl.mzapisek.rescraper.reactor_kafka.evaluationprocessor;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnitStatus;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.shared.Processor;
import pl.mzapisek.rescraper.common.shared.exceptions.JobCancelled;
import pl.mzapisek.rescraper.common.shared.mappers.EvaluationUnitMapper;
import pl.mzapisek.rescraper.common.shared.mappers.ScrapeUnitMapper;
import pl.mzapisek.rescraper.common.shared.services.MetricsService;
import pl.mzapisek.rescraper.common.shared.services.RedisService;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.reactor_kafka.eventbus.EventBusProducer;
import pl.mzapisek.rescraper.reactor_kafka.scrapeprocessor.ScrapeUnitMessageProducer;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.SenderResult;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Log4j2
public class EvaluationProcessor implements Processor<EvaluationUnit, Mono<EvaluationUnit>> {

    private final ScrapeUnitMessageProducer scrapeUnitMessageProducer;
    private final ScrapeScriptFactory scrapeScriptFactory;
    private final MetricsService metricsService;
    private final RedisService redisService;
    private final EventBusProducer eventBusProducer;

    public Mono<EvaluationUnit> process(EvaluationUnit evaluationUnit) {
        Objects.requireNonNull(evaluationUnit);

        log.info("evaluation unit processing started: {}", evaluationUnit);

        return Mono
                .fromSupplier(() -> processEvaluationUnitBlocking(evaluationUnit))
                .flatMap(this::saveScrapeUnits)
                .flatMap(scrapeUnits -> sendScrapeUnits(scrapeUnits, evaluationUnit.getAssignedPartitions()))
                .map(senderResults -> createProcessingEvaluationUnit(evaluationUnit))
                .switchIfEmpty(createSuccessWithoutDataEvaluationUnit(evaluationUnit))
                .flatMap(this::saveEvaluationUnit)
                .onErrorResume(throwable -> {
                    if (throwable instanceof JobCancelled) {
                        return saveEvaluationUnit(createCancelledEvaluationUnit(evaluationUnit));
                    }

                    log.error(throwable.getMessage());
                    throwable.printStackTrace();

                    return saveEvaluationUnit(createFailedEvaluationUnit(evaluationUnit, throwable));
                });
    }

    private EvaluationUnit createFailedEvaluationUnit(EvaluationUnit evaluationUnit, Throwable throwable) {
        return evaluationUnit.toBuilder()
                .status(EvaluationUnitStatus.FAILED)
                .finishedAt(LocalDateTime.now())
                .errorMessage(throwable.getMessage())
                .build();
    }

    private EvaluationUnit createCancelledEvaluationUnit(EvaluationUnit evaluationUnit) {
        return evaluationUnit.toBuilder()
                .status(EvaluationUnitStatus.CANCELLED)
                .finishedAt(LocalDateTime.now())
                .build();
    }

    private EvaluationUnit createProcessingEvaluationUnit(EvaluationUnit evaluationUnit) {
        return evaluationUnit.toBuilder()
                .status(EvaluationUnitStatus.PROCESSING)
                .build();
    }

    private Mono<EvaluationUnit> createSuccessWithoutDataEvaluationUnit(EvaluationUnit evaluationUnit) {
        return Mono.just(evaluationUnit.toBuilder()
                .status(EvaluationUnitStatus.SUCCESS_WITHOUT_DATA)
                .finishedAt(LocalDateTime.now())
                .build());
    }

    private List<ScrapeUnit> processEvaluationUnitBlocking(EvaluationUnit evaluationUnit) {
        if (redisService.isJobCancelled(evaluationUnit.getJobId())) {
            throw new JobCancelled(evaluationUnit.getJobId());
        }
        var scrapeScript = scrapeScriptFactory.create(evaluationUnit.getScriptName(), evaluationUnit.getScriptVersion());
        return scrapeScript.process(evaluationUnit);
    }

    private Mono<List<ScrapeUnit>> saveScrapeUnits(List<ScrapeUnit> scrapeUnits) {
        return eventBusProducer
                .sendScrapeUnits(scrapeUnits.stream()
                        .map(ScrapeUnitMapper::mapScrapeUnitToProto)
                        .collect(Collectors.toList()))
                .collectList()
                .map(senderResults -> scrapeUnits);
    }

    private Mono<List<SenderResult<String>>> sendScrapeUnits(List<ScrapeUnit> scrapeUnits, List<Integer> partitions) {
        return scrapeUnitMessageProducer.send(scrapeUnits, partitions).collectList();
    }

    private Mono<EvaluationUnit> saveEvaluationUnit(EvaluationUnit evaluationUnit) {
        return eventBusProducer
                .sendEvaluationUnits(List.of(EvaluationUnitMapper.mapEvaluationUnitToProto(evaluationUnit)))
                .collectList()
                .map(senderResults -> evaluationUnit);
    }

}
