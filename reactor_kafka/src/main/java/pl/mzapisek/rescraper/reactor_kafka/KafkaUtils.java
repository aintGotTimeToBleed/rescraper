package pl.mzapisek.rescraper.reactor_kafka;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.PartitionInfo;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.KafkaSender;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class KafkaUtils {

	private final KafkaSender<String, String> kafkaSender;

	public Mono<List<Integer>> getRandomPartitions(String topic, Integer numberOfPartitions) {
		return getPartitionInfos(topic)
				.map(this::getPartitionNumbersFromPartitionInfos)
				.map(integers -> takeRandomPartitions(integers, numberOfPartitions));
	}

	public static Integer getRandomPartition(List<Integer> partitionNumbers) {
		return partitionNumbers.get(new Random().nextInt(partitionNumbers.size()));
	}

	private Mono<List<PartitionInfo>> getPartitionInfos(String topic) {
		return kafkaSender.doOnProducer(producer -> producer.partitionsFor(topic));
	}

	private List<Integer> getPartitionNumbersFromPartitionInfos(List<PartitionInfo> partitionInfos) {
		return partitionInfos.stream()
				.map(PartitionInfo::partition)
				.collect(Collectors.toList());
	}

	private List<Integer> takeRandomPartitions(List<Integer> partitions, Integer numberOfPartitions) {
		Collections.shuffle(partitions);
		return partitions.stream()
				.limit(numberOfPartitions)
				.collect(Collectors.toList());
	}

}
