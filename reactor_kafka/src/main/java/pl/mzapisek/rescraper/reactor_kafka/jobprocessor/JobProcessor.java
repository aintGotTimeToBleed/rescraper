package pl.mzapisek.rescraper.reactor_kafka.jobprocessor;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.reactive.function.client.WebClient;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnitStatus;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.jobprocessor.domain.JobStatus;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.shared.Processor;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;
import pl.mzapisek.rescraper.common.shared.mappers.EvaluationUnitMapper;
import pl.mzapisek.rescraper.common.shared.mappers.JobMapper;
import pl.mzapisek.rescraper.common.shared.services.MetricsService;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.reactor_kafka.KafkaUtils;
import pl.mzapisek.rescraper.reactor_kafka.evaluationprocessor.EvaluationUnitMessageProducer;
import pl.mzapisek.rescraper.reactor_kafka.eventbus.EventBusProducer;
import pl.mzapisek.rescraper.reactor_kafka.scrapeprocessor.ScrapeUnitMessageProducer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.kafka.sender.SenderResult;

import java.util.Objects;
import java.util.stream.Collectors;

@Log4j2
@RequiredArgsConstructor
class JobProcessor implements Processor<Job, Mono<Job>> {

    private final EvaluationUnitMessageProducer evaluationUnitMessageProducer;
    private final ScrapeUnitMessageProducer scrapeUnitMessageProducer;
    private final ScrapeScriptFactory scrapeScriptFactory;
    private final MetricsService metricsService;
    private final WebClient webClient;
    private final EventBusProducer eventBusProducer;

    public Mono<Job> process(Job job) {
        Objects.requireNonNull(job);

        log.info("job processing started for job: {}", job.getId());
        log.info("job: {}", job);

        return Flux.just(scrapeScriptFactory.create(job.getScriptName(), job.getScriptVersion()))
                .flatMap(scrapeScript -> sendUnits(job, scrapeScript))
                .then(saveJob(job.toBuilder()
                        .status(JobStatus.PROCESSING)
                        .build()))
                .onErrorResume(throwable -> {
                    throwable.printStackTrace();

                    return saveJob(job.toBuilder()
                            .status(JobStatus.FAILED)
                            .errorMessage(throwable.getMessage())
                            .build());
                });
    }

    private Flux<SenderResult<String>> sendUnits(Job job, ScrapeScript scrapeScript) {
        if (job.isRerun()) {
            return Flux.merge(sendFailedEvaluationUnits(job), sendFailedScrapeUnits(job));
        } else {
            return Mono.fromSupplier(() -> scrapeScript.process(job))
                    .subscribeOn(Schedulers.boundedElastic())
                    .flux()
                    .flatMap(evaluationUnits ->
                            eventBusProducer
                                    .sendEvaluationUnits(evaluationUnits.stream()
                                            .map(EvaluationUnitMapper::mapEvaluationUnitToProto)
                                            .collect(Collectors.toList()))
                                    .map(stringSenderResult -> evaluationUnits))
                    .flatMap(evaluationUnits ->
                            evaluationUnitMessageProducer
                                    .send(evaluationUnits, job.getAssignedPartitions()));
        }
    }

    private Mono<Job> saveJob(Job job) {
        metricsService.jobTimer(job);
        log.info("processed job: {}", job);
        return eventBusProducer.sendJob(JobMapper.mapJobToProto(job))
                .collectList()
                .map(senderResults -> job);
    }

    private Flux<SenderResult<String>> sendFailedEvaluationUnits(Job job) {
        return webClient
                .get()
                .uri(uriBuilder ->
                        uriBuilder.path("/evaluationunits")
                                .queryParam("job_id", job.getId())
                                .queryParam("evaluation_status", EvaluationUnitStatus.FAILED.toString())
                                .build())
                .retrieve()
                .bodyToFlux(EvaluationUnit.class)
                .flatMap(evaluationUnit -> evaluationUnitMessageProducer.send(evaluationUnit, job.getAssignedPartitions()));
    }

    private Flux<SenderResult<String>> sendFailedScrapeUnits(Job job) {
        return webClient
                .get()
                .uri(uriBuilder ->
                        uriBuilder.path("/scrapeunits")
                                .queryParam("job_id", job.getId())
                                .queryParam("scrape_status", ScrapeUnitStatus.FAILED.toString())
                                .build())
                .retrieve()
                .bodyToFlux(ScrapeUnit.class)
                .flatMap(scrapeUnit -> scrapeUnitMessageProducer.send(scrapeUnit, KafkaUtils.getRandomPartition(job.getAssignedPartitions())));
    }

}
