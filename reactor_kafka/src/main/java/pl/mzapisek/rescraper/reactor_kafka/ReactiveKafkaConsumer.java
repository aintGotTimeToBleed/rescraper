package pl.mzapisek.rescraper.reactor_kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.common.TopicPartition;
import pl.mzapisek.rescraper.common.shared.Processor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.receiver.ReceiverRecord;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

@Log4j2
@RequiredArgsConstructor
public class ReactiveKafkaConsumer<T> {

    private static final int NUMBER_OF_RETRIES_FOR_DO_ON_CONSUMER_OPERATIONS = 3;

    private final String topicName;
    private final Processor<T, Mono<T>> processor;
    private final Function<String, T> deserializer;
    private final ReceiverOptions<String, String> receiverOptions;
    private KafkaReceiver<String, String> receiver;

    public Flux<ReceiverRecord<String, String>> createFlux() {
        receiver = KafkaReceiver.create(receiverOptions.subscription(List.of(topicName)));

        return receiver
                .receive()
                .limitRate(1)
                .flatMap(this::assigned)
                .flatMap(this::resumeAll)
                .flatMap(this::pausePartition)
                .publishOn(Schedulers.boundedElastic())
                .flatMap(this::processMessage)
                .flatMap(this::ackMessage)
                .flatMap(this::resumePartition)
                .doOnError(throwable -> {
                    log.error("on error: " + throwable.getMessage());
                    throwable.printStackTrace();
                })
                .retry(64);
    }

    private Mono<ReceiverRecord<String, String>> processMessage(ReceiverRecord<String, String> record) {
        return processor
                .process(deserializer.apply(record.value()))
                .flatMap(scrapeUnit -> Mono.just(record));
    }

    private Mono<ReceiverRecord<String, String>> resumeAll(ReceiverRecord<String, String> record) {
        return receiver.doOnConsumer(org.apache.kafka.clients.consumer.Consumer::paused)
                .flatMap(topicPartitions -> receiver.doOnConsumer(consumer -> {
                    log.debug("resuming all paused partitions: " + topicPartitions);
                    consumer.resume(topicPartitions);
                    return record;
                }));
    }

    private Mono<ReceiverRecord<String, String>> assigned(ReceiverRecord<String, String> record) {
        return receiver.doOnConsumer(consumer -> {
            boolean stringStringReceiverRecord = consumer.assignment()
                    .stream()
                    .anyMatch(topicPartition -> topicPartition.partition() == record.receiverOffset().topicPartition().partition());
            if (stringStringReceiverRecord) {
                log.debug("assigned to partition: " + record.receiverOffset().topicPartition().topic() + "-" + record.receiverOffset().topicPartition().partition());
                return record;
            }

            throw new DetachedConsumerException("consumer detached: " + topicName + "-" + record.partition());
        }).onErrorContinue((throwable, o) -> log.error(throwable.getMessage()));
    }

    private Mono<ReceiverRecord<String, String>> pausePartition(ReceiverRecord<String, String> record) {
        return receiver.doOnConsumer(consumer -> {
            log.debug("pause: " + topicName + "-" + record.partition());
            TopicPartition topicPartition = new TopicPartition(topicName, record.partition());
            consumer.pause(Collections.singleton(topicPartition));
            return record;
        }).retry(NUMBER_OF_RETRIES_FOR_DO_ON_CONSUMER_OPERATIONS);
    }

    private Mono<ReceiverRecord<String, String>> resumePartition(ReceiverRecord<String, String> record) {
        return receiver.doOnConsumer(consumer -> {
            log.debug("resuming: " + topicName + "-" + record.partition());
            TopicPartition topicPartition = new TopicPartition(topicName, record.partition());
            consumer.resume(Collections.singleton(topicPartition));
            return consumer.paused();
        }).flatMap(topicPartitions -> {
            log.debug("paused: " + topicPartitions);
            return Mono.just(record);
        }).retry(NUMBER_OF_RETRIES_FOR_DO_ON_CONSUMER_OPERATIONS);
    }

    private Mono<ReceiverRecord<String, String>> ackMessage(ReceiverRecord<String, String> record) {
        log.debug("acknowledging offset: " + record.receiverOffset().topicPartition().topic() + "-"
                + record.receiverOffset().topicPartition().partition() + "-" + record.receiverOffset().offset());
        record.receiverOffset().acknowledge();
        return Mono.just(record);
    }

    private Mono<ReceiverRecord<String, String>> commitMessage(ReceiverRecord<String, String> record) {
        log.debug("committing offset: " + record.receiverOffset().topicPartition().topic() + "-"
                + record.receiverOffset().topicPartition().partition() + "-" + record.receiverOffset().offset());
        return record
                .receiverOffset()
                .commit()
                .retry(3)
                .flatMap(aVoid -> Mono.just(record))
                .onErrorResume(throwable -> {
                    log.error(throwable.getMessage());
                    return Mono.empty();
                });
    }

}
