package pl.mzapisek.rescraper.reactor_kafka.scrapeprocessor;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.shared.Processor;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;
import pl.mzapisek.rescraper.common.shared.exceptions.JobCancelled;
import pl.mzapisek.rescraper.common.shared.exceptions.ScrapeUnitAlreadyProcessed;
import pl.mzapisek.rescraper.common.shared.mappers.ScrapeResultMapper;
import pl.mzapisek.rescraper.common.shared.mappers.ScrapeUnitMapper;
import pl.mzapisek.rescraper.common.shared.services.MetricsService;
import pl.mzapisek.rescraper.common.shared.services.RedisService;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.reactor_kafka.eventbus.EventBusProducer;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j2
@RequiredArgsConstructor
public class ScrapeProcessor implements Processor<ScrapeUnit, Mono<ScrapeUnit>> {

    private final ScrapeScriptFactory scrapeScriptFactory;
    private final MetricsService metricsService; // todo add metrics
    private final RedisService redisService;
    private final EventBusProducer eventBusProducer;

    public Mono<ScrapeUnit> process(ScrapeUnit scrapeUnit) {
        Objects.requireNonNull(scrapeUnit);

        log.info("scrape unit processing started: {}", scrapeUnit);

        return Mono
                .fromSupplier(() -> processScrapeUnitBlocking(scrapeUnit))
                .flatMap(scrapeResults -> {
                    log.info("results: " + scrapeResults);
                    return eventBusProducer
                            .sendScrapeResults(scrapeResults.stream()
                                    .map(ScrapeResultMapper::mapScrapeResultToProto)
                                    .collect(Collectors.toList()))
                            .collectList();
                })
                .flatMap(scrapeResults -> updateScrapeUnit(scrapeUnit, ScrapeUnitStatus.SUCCESS, null))
                .flatMap(aBoolean -> Mono.just(scrapeUnit))
                .onErrorResume(throwable -> {
                    if (throwable instanceof JobCancelled) {
                        log.info(throwable.getMessage());
                        return updateScrapeUnit(scrapeUnit, ScrapeUnitStatus.CANCELLED, null);
                    }
                    if (throwable instanceof ScrapeUnitAlreadyProcessed) {
                        log.error(throwable.getMessage());
                        return Mono.just(scrapeUnit);
                    }

                    log.error("error processing scrape unite: {}, with error: {}", scrapeUnit, throwable.getMessage());
                    throwable.printStackTrace();

                    return updateScrapeUnit(scrapeUnit, ScrapeUnitStatus.FAILED, throwable.getMessage());
                });
    }

    private List<ScrapeResult> processScrapeUnitBlocking(ScrapeUnit scrapeUnit) {
        if (redisService.isJobCancelled(scrapeUnit.getJobId())) {
            throw new JobCancelled(scrapeUnit.getJobId(), scrapeUnit.getEvaluationId(), scrapeUnit.getId());
        }
        if (redisService.isScrapeUnitCompleted(scrapeUnit.getId())) {
            throw new ScrapeUnitAlreadyProcessed(scrapeUnit.getId());
        }
        var scrapeResults = scrapeScriptFactory.create(scrapeUnit.getScriptName(), scrapeUnit.getScriptVersion()).process(scrapeUnit);
        redisService.scrapeUnitCompleted(scrapeUnit.getId());
        return scrapeResults;
    }

    private Mono<ScrapeUnit> updateScrapeUnit(ScrapeUnit scrapeUnit, ScrapeUnitStatus scrapeUnitStatus, String errorMsg) {
        var updatedScrapeUnit = scrapeUnit.toBuilder()
                .finishedAt(LocalDateTime.now())
                .scrapeUnitStatus(scrapeUnitStatus)
                .errorMessage(errorMsg)
                .build();
        return eventBusProducer
                .sendScrapeUnit(ScrapeUnitMapper.mapScrapeUnitToProto(scrapeUnit))
                .collectList()
                .map(senderResults -> updatedScrapeUnit);
    }

}
