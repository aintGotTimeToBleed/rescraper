package pl.mzapisek.rescraper.reactor_kafka.configs;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.properties.KafkaProducerProperties;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;

import java.util.HashMap;

@EnableConfigurationProperties({KafkaProducerProperties.class, KafkaBaseProperties.class})
@RequiredArgsConstructor
public class ReactorKafkaProducerBaseConfiguration {

    private final KafkaBaseProperties kafkaBaseProperties;
    private final KafkaProducerProperties kafkaProducerProperties;

    private HashMap<String, Object> senderProperties() {
        HashMap<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBaseProperties.getBootstrapServers());
        props.put(ProducerConfig.CLIENT_ID_CONFIG, kafkaBaseProperties.getClientId());
        props.put(ProducerConfig.ACKS_CONFIG, kafkaProducerProperties.getAcks());
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, kafkaProducerProperties.getBatchSize());

        return props;
    }

    @Bean
    public SenderOptions<String, String> kafkaSenderOptions() {
        return SenderOptions.create(senderProperties());
    }

    @Bean(name = "kafkaSender", destroyMethod = "close")
    public KafkaSender<String, String> kafkaSender(SenderOptions<String, String> senderOptions) {
        return KafkaSender.create(senderOptions);
    }

    @Bean(name = "eventBusKafkaSender", destroyMethod = "close")
    public KafkaSender<String, byte[]> kafkaSender() {
        HashMap<String, Object> props = senderProperties();
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);

        return KafkaSender.create(SenderOptions.create(props));
    }

}
