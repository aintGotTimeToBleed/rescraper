package pl.mzapisek.rescraper.reactor_kafka.evaluationprocessor;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.producer.ProducerRecord;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.services.BatchCreator;
import pl.mzapisek.rescraper.common.shared.services.JsonUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderRecord;
import reactor.kafka.sender.SenderResult;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

@Log4j2
@RequiredArgsConstructor
public class EvaluationUnitMessageProducer {

    private final KafkaSender<String, String> kafkaSender;
    private final KafkaBaseProperties kafkaBaseProperties;
    private final JsonUtils jsonUtils;
    private final BatchCreator<EvaluationUnit> batchCreator;

    public Flux<SenderResult<String>> send(EvaluationUnit evaluationUnit, List<Integer> partitionNumbers) {
        Objects.requireNonNull(evaluationUnit);
        Objects.requireNonNull(partitionNumbers);

        return kafkaSender
                .send(Mono.just(createSenderRecord(jsonUtils.createJsonTry(evaluationUnit).get(), getRandomPartition(partitionNumbers))))
                .doOnComplete(() -> log.info("evaluation units sent: " + LocalDateTime.now()));
    }

    public Flux<SenderResult<String>> send(List<EvaluationUnit> evaluationUnits, List<Integer> partitionNumbers) {
        Objects.requireNonNull(evaluationUnits);
        Objects.requireNonNull(partitionNumbers);

        var atomicInteger = new AtomicInteger();
        return Flux
                .fromIterable(batchCreator.createBatch(evaluationUnits, partitionNumbers.size()))
                .flatMap(units -> sendEvaluationUnits(units, partitionNumbers.get(atomicInteger.getAndIncrement())));
    }

    private Flux<SenderResult<String>> sendEvaluationUnits(List<EvaluationUnit> evaluationUnits, Integer partition) {
        return kafkaSender
                .send(Flux.fromIterable(evaluationUnits)
                        .map(evaluationUnit -> createSenderRecord(jsonUtils.createJsonTry(evaluationUnit).get(), partition)));
    }

    private SenderRecord<String, String, String> createSenderRecord(String msg, Integer partition) {
        return SenderRecord.create(new ProducerRecord<>(kafkaBaseProperties.getEvaluationUnitTopic(), partition, null, msg), msg);
    }

    private Integer getRandomPartition(List<Integer> partitionNumbers) {
        return partitionNumbers.get(new Random().nextInt(partitionNumbers.size()));
    }

}
