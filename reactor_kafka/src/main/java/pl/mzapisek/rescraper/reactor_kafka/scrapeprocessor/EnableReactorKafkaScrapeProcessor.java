package pl.mzapisek.rescraper.reactor_kafka.scrapeprocessor;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({ScrapeUnitProcessorConfiguration.class})
public @interface EnableReactorKafkaScrapeProcessor {
}
