package pl.mzapisek.rescraper.reactor_kafka.scrapeprocessor;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import pl.mzapisek.rescraper.common.configuration.RescraperConfiguration;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.shared.Processor;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.repository.ScrapeResultMongoRepository;
import pl.mzapisek.rescraper.common.shared.services.JsonUtils;
import pl.mzapisek.rescraper.common.shared.services.MetricsService;
import pl.mzapisek.rescraper.common.shared.services.RedisService;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.reactor_kafka.ReactiveKafkaConsumer;
import pl.mzapisek.rescraper.reactor_kafka.configs.ReactorKafkaConsumerBaseConfiguration;
import pl.mzapisek.rescraper.reactor_kafka.configs.ReactorKafkaProducerBaseConfiguration;
import pl.mzapisek.rescraper.reactor_kafka.eventbus.EventBusProducer;
import reactor.core.Disposable;
import reactor.core.publisher.Mono;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.sender.KafkaSender;

@Log4j2
@Import({ReactorKafkaConsumerBaseConfiguration.class, RescraperConfiguration.class, ReactorKafkaProducerBaseConfiguration.class})
@ComponentScan(basePackageClasses = {ScrapeProcessor.class, EventBusProducer.class})
public class ScrapeUnitProcessorConfiguration {

    @Bean
    public ScrapeProcessor scrapeProcessor(ScrapeScriptFactory scrapeScriptFactory,
                                           MetricsService metricsService,
                                           RedisService redisService,
                                           EventBusProducer eventBusProducer) {
        return new ScrapeProcessor(scrapeScriptFactory, metricsService, redisService, eventBusProducer);
    }

    @Bean(destroyMethod = "dispose", name = "ScrapeUnitConsumer")
    public Disposable scrapeConsumer(KafkaBaseProperties kafkaBaseProperties,
                                     JsonUtils jsonUtils,
                                     Processor<ScrapeUnit, Mono<ScrapeUnit>> scrapeProcessor,
                                     ReceiverOptions<String, String> receiverOptions) {
        return new ReactiveKafkaConsumer<>(kafkaBaseProperties.getScrapeUnitTopic(), scrapeProcessor, jsonUtils.createScrapeUnitFunction(), receiverOptions)
                .createFlux()
                .subscribe();
    }

//    @Bean
//    public EventBusProducer eventBusProducer(@Qualifier("eventBusKafkaSender") KafkaSender<String, byte[]> kafkaSender, KafkaBaseProperties kafkaBaseProperties) {
//        return new EventBusProducer(kafkaSender, kafkaBaseProperties);
//    }

}
