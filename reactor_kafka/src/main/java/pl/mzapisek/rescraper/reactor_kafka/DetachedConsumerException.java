package pl.mzapisek.rescraper.reactor_kafka;

public class DetachedConsumerException extends RuntimeException {

    public DetachedConsumerException() {
        super();
    }

    public DetachedConsumerException(String message) {
        super(message);
    }

}
