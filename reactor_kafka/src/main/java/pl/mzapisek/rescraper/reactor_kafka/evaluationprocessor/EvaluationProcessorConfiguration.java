package pl.mzapisek.rescraper.reactor_kafka.evaluationprocessor;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import pl.mzapisek.rescraper.common.configuration.RescraperConfiguration;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.shared.Processor;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.services.*;
import pl.mzapisek.rescraper.reactor_kafka.ReactiveKafkaConsumer;
import pl.mzapisek.rescraper.reactor_kafka.configs.ReactorKafkaConsumerBaseConfiguration;
import pl.mzapisek.rescraper.reactor_kafka.configs.ReactorKafkaProducerBaseConfiguration;
import pl.mzapisek.rescraper.reactor_kafka.eventbus.EventBusProducer;
import pl.mzapisek.rescraper.reactor_kafka.scrapeprocessor.ScrapeUnitMessageProducer;
import reactor.core.Disposable;
import reactor.core.publisher.Mono;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.sender.KafkaSender;

@Log4j2
@Import({ReactorKafkaConsumerBaseConfiguration.class,
        ReactorKafkaProducerBaseConfiguration.class,
        RescraperConfiguration.class})
@ComponentScan(basePackageClasses = {EvaluationProcessor.class, EventBusProducer.class})
@RequiredArgsConstructor
public class EvaluationProcessorConfiguration {

    @Bean
    public EvaluationUnitMessageProducer evaluationMessageProducer(KafkaSender<String, String> kafkaSender,
                                                                   KafkaBaseProperties kafkaBaseProperties,
                                                                   BatchCreator<EvaluationUnit> batchCreator,
                                                                   JsonUtils jsonUtils) {
        return new EvaluationUnitMessageProducer(kafkaSender, kafkaBaseProperties, jsonUtils, batchCreator);
    }

    @Bean
    public ScrapeUnitMessageProducer scrapeUnitMessageProducer(KafkaSender<String, String> kafkaSender,
                                                               KafkaBaseProperties kafkaBaseProperties,
                                                               JsonUtils jsonUtils,
                                                               BatchCreator<ScrapeUnit> batchCreator) {
        return new ScrapeUnitMessageProducer(kafkaSender, kafkaBaseProperties, jsonUtils, batchCreator);
    }

    @Bean
    public EvaluationProcessor evaluationProcessor(ScrapeUnitMessageProducer scrapeUnitMessageProducer,
                                                   ScrapeScriptFactory scrapeScriptFactory,
                                                   MetricsService metricsService,
                                                   RedisService redisService,
                                                   EventBusProducer eventBusProducer) {
        return new EvaluationProcessor(scrapeUnitMessageProducer, scrapeScriptFactory, metricsService, redisService, eventBusProducer);
    }

    @Bean(destroyMethod = "dispose")
    public Disposable evaluationUnitConsumer(KafkaBaseProperties kafkaBaseProperties,
                                             JsonUtils jsonUtils,
                                             Processor<EvaluationUnit, Mono<EvaluationUnit>> evaluationProcessor,
                                             ReceiverOptions<String, String> receiverOptions) {
        return new ReactiveKafkaConsumer<>(kafkaBaseProperties.getEvaluationUnitTopic(), evaluationProcessor, jsonUtils.createEvaluationUnitFunction(), receiverOptions)
                .createFlux()
                .subscribe();
    }

}
