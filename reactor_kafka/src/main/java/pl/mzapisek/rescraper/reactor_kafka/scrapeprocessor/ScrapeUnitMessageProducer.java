package pl.mzapisek.rescraper.reactor_kafka.scrapeprocessor;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.producer.ProducerRecord;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.services.BatchCreator;
import pl.mzapisek.rescraper.common.shared.services.JsonUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderRecord;
import reactor.kafka.sender.SenderResult;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Log4j2
@RequiredArgsConstructor
public class ScrapeUnitMessageProducer {

    private final KafkaSender<String, String> kafkaSender;
    private final KafkaBaseProperties kafkaBaseProperties;
    private final JsonUtils jsonUtils;
    private final BatchCreator<ScrapeUnit> batchCreator;

    public Flux<SenderResult<String>> send(ScrapeUnit scrapeUnit, Integer partition) {
        return kafkaSender
                .send(Mono.just(createSenderRecord(jsonUtils.createJsonTry(scrapeUnit).get(), partition)));
    }

    public Flux<SenderResult<String>> send(List<ScrapeUnit> scrapeUnits, List<Integer> partitions) {
        var atomicInteger = new AtomicInteger();
        return Flux.fromIterable(batchCreator.createBatch(scrapeUnits, partitions.size()))
                .flatMap(batch -> sendBatch(batch, partitions.get(atomicInteger.getAndIncrement())));
    }

    private SenderRecord<String, String, String> createSenderRecord(String msg, Integer partition) {
        return SenderRecord.create(new ProducerRecord<>(kafkaBaseProperties.getScrapeUnitTopic(), partition, null, msg), msg);
    }

    private Flux<SenderResult<String>> sendBatch(List<ScrapeUnit> scrapeUnits, Integer partition) {
        return kafkaSender.send(Flux.fromIterable(scrapeUnits).map(scrapeUnit ->
                createSenderRecord(jsonUtils.createJsonTry(scrapeUnit).get(), partition)));
    }

}
