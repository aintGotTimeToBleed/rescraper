package pl.mzapisek.rescraper.reactor_kafka.jobprocessor;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.web.reactive.function.client.WebClient;
import pl.mzapisek.rescraper.common.configuration.RescraperConfiguration;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.repository.EvaluationUnitMongoRepository;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.jobprocessor.repository.JobMongoRepository;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.shared.Processor;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.services.BatchCreator;
import pl.mzapisek.rescraper.common.shared.services.JsonUtils;
import pl.mzapisek.rescraper.common.shared.services.MetricsService;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.reactor_kafka.ReactiveKafkaConsumer;
import pl.mzapisek.rescraper.reactor_kafka.configs.ReactorKafkaConsumerBaseConfiguration;
import pl.mzapisek.rescraper.reactor_kafka.configs.ReactorKafkaProducerBaseConfiguration;
import pl.mzapisek.rescraper.reactor_kafka.evaluationprocessor.EvaluationUnitMessageProducer;
import pl.mzapisek.rescraper.reactor_kafka.eventbus.EventBusProducer;
import pl.mzapisek.rescraper.reactor_kafka.scrapeprocessor.ScrapeUnitMessageProducer;
import reactor.core.Disposable;
import reactor.core.publisher.Mono;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.sender.KafkaSender;

@Log4j2
@Import({ReactorKafkaConsumerBaseConfiguration.class, ReactorKafkaProducerBaseConfiguration.class, RescraperConfiguration.class})
@ComponentScan(basePackageClasses = {JobProcessor.class, EventBusProducer.class})
public class JobProcessorConfiguration {

    private final String rescraperApiUrl;

    public JobProcessorConfiguration(@Value("${rescraper.api}") String rescraperApiUrl) {
        this.rescraperApiUrl = rescraperApiUrl;
    }

    @Bean
    public WebClient webClient() {
        return WebClient.create(rescraperApiUrl);
    }

    @Bean
    public EvaluationUnitMessageProducer evaluationMessageProducer(@Qualifier("kafkaSender") KafkaSender<String, String> kafkaSender,
                                                                   KafkaBaseProperties kafkaBaseProperties,
                                                                   BatchCreator<EvaluationUnit> batchCreator,
                                                                   JsonUtils jsonUtils) {
        return new EvaluationUnitMessageProducer(kafkaSender, kafkaBaseProperties, jsonUtils, batchCreator);
    }

    @Bean
    public ScrapeUnitMessageProducer scrapeUnitMessageProducer(KafkaSender<String, String> kafkaSender,
                                                               KafkaBaseProperties kafkaBaseProperties,
                                                               JsonUtils jsonUtils,
                                                               BatchCreator<ScrapeUnit> batchCreator) {
        return new ScrapeUnitMessageProducer(kafkaSender, kafkaBaseProperties, jsonUtils, batchCreator);
    }

    @Bean
    public JobProcessor jobProcessor(EvaluationUnitMessageProducer evaluationUnitMessageProducer,
                                     ScrapeUnitMessageProducer scrapeUnitMessageProducer,
                                     ScrapeScriptFactory scrapeScriptFactory,
                                     MetricsService metricsService,
                                     WebClient webClient,
                                     EventBusProducer eventBusProducer) {
        return new JobProcessor(
                evaluationUnitMessageProducer,
                scrapeUnitMessageProducer,
                scrapeScriptFactory,
                metricsService,
                webClient,
                eventBusProducer
        );
    }

    @Bean(destroyMethod = "dispose")
    public Disposable jobConsumer(KafkaBaseProperties kafkaBaseProperties,
                                  JsonUtils jsonUtils,
                                  Processor<Job, Mono<Job>> jobProcessor,
                                  ReceiverOptions<String, String> receiverOptions) {
        return new ReactiveKafkaConsumer<>(kafkaBaseProperties.getJobTopic(), jobProcessor, jsonUtils.createJobFunction(), receiverOptions)
                .createFlux()
                .subscribe();
    }

}
