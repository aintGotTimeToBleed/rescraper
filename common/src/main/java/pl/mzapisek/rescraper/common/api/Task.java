package pl.mzapisek.rescraper.common.api;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

@Builder(toBuilder = true)
@Document
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@ToString
public class Task {

    private String id;
    private String taskName;
    private String cronExpression;
    private String scriptName;
    private int scriptVersion;
    private String description;

}
