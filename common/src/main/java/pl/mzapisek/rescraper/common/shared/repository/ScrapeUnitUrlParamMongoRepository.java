package pl.mzapisek.rescraper.common.shared.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitUrlParam;
import reactor.core.publisher.Mono;

// remove?
public interface ScrapeUnitUrlParamMongoRepository extends ReactiveMongoRepository<ScrapeUnitUrlParam, String> {

    Mono<ScrapeUnitUrlParam> findByParamAndJobId(String param, String jobId);

}
