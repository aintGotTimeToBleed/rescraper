package pl.mzapisek.rescraper.common.scripts.architects;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

@Service
public class NMArchitectsScrapeService {

    public NMArchitectsLicense getLicense(Element element) {
        return NMArchitectsLicense.builder()
                .licenseNo(getLicenseNo(element))
                .name(getFirstName(element))
                .company(getCompany(element))
                .city(getCity(element))
                .zip(getZip(element))
                .status(getStatus(element))
                .expirationDate(getExpirationDate(element))
                .build();
    }

    private String getLicenseNo(Element element) {
        final Elements elementsByClass = element.getElementsByClass("card-body-text");
        for (Element el : elementsByClass) {
            final Elements span = el.getElementsByTag("span");
            if (span.size() == 2) {
                final String text = span.first().text();
                if (text.contains("License Information")) {
                    final Elements p = el.getElementsByTag("p");
                    if (p.size() > 0) {
                        final Element first = p.first();
                        final Elements strong = first.getElementsByTag("strong");
                        if (strong.size() > 0) {
                            return strong.first().text();
                        }
                    }
                }
            }
        }

        return "";
    }

    private String getExpirationDate(Element element) {
        final Elements elementsByClass = element.getElementsByClass("card-body-text");
        for (Element el : elementsByClass) {
            final Elements span = el.getElementsByTag("span");
            if (span.size() == 2) {
                if (span.first().text().contains("License Information")) {
                    final Elements p = el.getElementsByTag("p");
                    if (p.size() > 0) {
                        final Element first = p.first();
                        final Elements strong = first.getElementsByTag("strong");
                        if (strong.size() == 2) {
                            final Element element1 = strong.get(1);
                            return element1.text();
                        }
                    }
                }
            }
        }

        return "";
    }

    private String getFirstName(Element element) {
        final Elements h4 = element.select("h4");
        if (h4.isEmpty()) {
            return "";
        }

        return h4.first().text().trim();

    }

    private String getStatus(Element element) {
        final Elements sta = element.getElementsByClass("slds-badge");

        if (sta.isEmpty()) {
            return "";
        }
        return sta.first().text().trim();
    }

    private String getCompany(Element element) {
        final Elements elementsByClass = element.getElementsByClass("card-body-text");
        for (Element el : elementsByClass) {
            final Elements span = el.getElementsByTag("span");
            if (span.size() > 0) {
                if (span.first().text().contains("Company Information")) {
                    final Elements p = el.getElementsByTag("p");
                    if (p.size() > 0) {
                        final Element first = p.first();
                        return first.text();
                    }
                }
            }
        }

        return "";
    }

    private String getCity(Element element) {
        final Elements elementsByClass = element.getElementsByClass("card-body-text");
        for (Element el : elementsByClass) {
            final Elements span = el.getElementsByTag("span");
            if (span.size() > 0) {
                if (span.first().text().contains("Address")) {
                    final Elements p = el.getElementsByClass("slds-truncate");
                    if (p.size() > 0) {
                        final Element first = p.first();
                        final String text = first.text();
                        final String[] split = text.split(",");
                        if (split.length == 2) {
                            return split[0].trim();
                        }
                    }
                }
            }
        }

        return "";
    }

    private String getZip(Element element) {
        final Elements elementsByClass = element.getElementsByClass("card-body-text");
        for (Element el : elementsByClass) {
            final Elements span = el.getElementsByTag("span");
            if (span.size() > 0) {
                if (span.first().text().contains("Address")) {
                    final Elements p = el.getElementsByClass("slds-truncate");
                    if (p.size() > 0) {
                        final Element first = p.first();
                        final String text = first.text();
                        final String[] split = text.split(",");
                        if (split.length == 2) {
                            return split[1].trim();
                        }
                    }
                }
            }
        }
        return "";
    }

}
