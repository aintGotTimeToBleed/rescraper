package pl.mzapisek.rescraper.common.shared.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;

import java.util.function.Function;

@Log4j2
@Service
@RequiredArgsConstructor
public class JsonUtils {

    private final ObjectMapper objectMapper;

    // todo rename

    public Try<String> createJsonTry(Object object) {
        return Try.of(() -> objectMapper.writeValueAsString(object));
    }

    public Try<Job> createJobTry(String msg) {
        return Try.of(() -> objectMapper.readValue(msg, Job.class));
    }

    public Try<EvaluationUnit> createEvaluationUnitTry(String msg) {
        return Try.of(() -> objectMapper.readValue(msg, EvaluationUnit.class));
    }

    public Try<ScrapeUnit> createScrapeUnitTry(String msg) {
        return Try.of(() -> objectMapper.readValue(msg, ScrapeUnit.class));
    }

    public Function<String, ScrapeUnit> createScrapeUnitFunction() {
        return msg -> Try.of(() -> objectMapper.readValue(msg, ScrapeUnit.class)).get();
    }

    public Function<String, Job> createJobFunction() {
        return msg -> Try.of(() -> objectMapper.readValue(msg, Job.class)).get();
    }

    public Function<String, EvaluationUnit> createEvaluationUnitFunction() {
        return msg -> Try.of(() -> objectMapper.readValue(msg, EvaluationUnit.class)).get();
    }

}
