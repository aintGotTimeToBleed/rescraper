package pl.mzapisek.rescraper.common.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = {
        "pl.mzapisek.rescraper.common.shared",
        "pl.mzapisek.rescraper.common.scripts"})
@Import({RedisConfiguration.class})
public interface RescraperConfiguration {
}
