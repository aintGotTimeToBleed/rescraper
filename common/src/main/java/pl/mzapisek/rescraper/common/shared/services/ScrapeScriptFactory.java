package pl.mzapisek.rescraper.common.shared.services;

import org.springframework.stereotype.Component;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.shared.exceptions.ScrapeScriptNotFound;

import java.util.List;

@Component
public class ScrapeScriptFactory {

    private static final int DEFAULT_SCRIPT_VERSION = 1;

    private final List<ScrapeScript> scrapeScripts;

    public ScrapeScriptFactory(List<ScrapeScript> scrapeScripts) {
        this.scrapeScripts = scrapeScripts;
    }

    public ScrapeScript create(String scriptName, Integer version) {
        return scrapeScripts.stream()
                .filter(scrapeScript -> scrapeScript.getScriptName().equals(scriptName) && scrapeScript.getVersion().equals(version))
                .findFirst()
                .orElseThrow(() -> new ScrapeScriptNotFound("Scrape script not found: " + scriptName));
    }

    public ScrapeScript create(String scriptName) {
        return create(scriptName, DEFAULT_SCRIPT_VERSION);
    }

}