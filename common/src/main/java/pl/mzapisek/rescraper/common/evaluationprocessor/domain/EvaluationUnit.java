package pl.mzapisek.rescraper.common.evaluationprocessor.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@Getter
@ToString
@Builder(toBuilder = true)
@Document
@AllArgsConstructor
@NoArgsConstructor
public class EvaluationUnit {

    // not null
    @Id
    private String id;
    // not null
    private String jobId;
    // not null
    private String element;
    // not null
    private EvaluationUnitStatus status;
    // not null
    @DateTimeFormat(iso = DATE_TIME)
    private LocalDateTime createdAt;
    @DateTimeFormat(iso = DATE_TIME)
    private LocalDateTime finishedAt;
    // not null
    private String scriptName;
    // not null
    private Integer scriptVersion;
//    private Map<String, String> params;
//    private boolean testRun;
    // not null
    private List<Integer> assignedPartitions;
    private String errorMessage;

}
