package pl.mzapisek.rescraper.common.shared.domain;

import lombok.Builder;
import lombok.Data;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;

// todo make immutable
@Data
@Builder
public class JobScrapingStatus {

    private Job job;
    private Long evaluationUnitsCount;
    private Long finishedEvaluationUnitsCount;
    private Long scrapeUnitsCount;
    private Long finishedScrapeUnitsCount;

}
