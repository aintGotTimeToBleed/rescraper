package pl.mzapisek.rescraper.common.shared.domain;

public enum ScrapeUnitStatus {
    READY,
    PROCESSING, // todo is anything use this
    SUCCESS,
    FAILED,
    CANCELLED
}
