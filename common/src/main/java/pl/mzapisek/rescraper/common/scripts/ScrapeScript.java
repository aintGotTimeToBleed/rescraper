package pl.mzapisek.rescraper.common.scripts;

import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;

import java.util.List;
import java.util.Map;

public interface ScrapeScript {

    String getScriptName();

    Integer getVersion();

    String getHeader();

    String getRow(String json);

    List<EvaluationUnit> process(Job job);

    List<ScrapeUnit> process(EvaluationUnit evaluationUnit);

    List<ScrapeResult> process(ScrapeUnit scrapeUnit);

    Map<String, String> getDefaultParams();

    List<String> getDefaultUrlParams();

    default Integer getMaxParallelism() {
        return 4;
    }

}
