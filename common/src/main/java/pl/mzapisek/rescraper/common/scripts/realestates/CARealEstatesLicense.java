package pl.mzapisek.rescraper.common.scripts.realestates;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
class CARealEstatesLicense {

    private String name;
    private String licenseNumber;
    private String licenseType;
    private String licenseStatus;
    private String expiration;
    private String brokerIssue;
    private String salesPersonIssue;
    private String corporationPersonIssue;
    private String formerName;
    private String address;
    private String office;
    private String dba;
    private String branches;

}
