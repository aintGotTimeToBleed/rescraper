package pl.mzapisek.rescraper.common.api;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnitStatus;
import pl.mzapisek.rescraper.common.evaluationprocessor.repository.EvaluationUnitMongoRepository;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.jobprocessor.domain.JobStatus;
import pl.mzapisek.rescraper.common.jobprocessor.repository.JobMongoRepository;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.List;

@Log4j2
@Service
@RequiredArgsConstructor
public class CompleteProcessService {

    private final JobMongoRepository jobMongoRepository;
    private final EvaluationUnitMongoRepository evaluationUnitMongoRepository;
    private final ScrapeUnitMongoRepository scrapeUnitMongoRepository;

    // todo is it working?
    public Mono<Boolean> completeProcess(String evaluationId, String jobId) {
        return isAnyScrapeUnitNotCompleted(evaluationId)
                .filter(isAnyScrapeUnitNotCompleted -> isAnyScrapeUnitNotCompleted.equals(false))
                .flatMap(allScrapeUnitsAreCompleted -> completeEvaluationUnit(evaluationId))
                .flatMap(this::isAnyEvaluationUnitNotCompleted)
                .filter(isAnyEvaluationUnitNotCompleted -> isAnyEvaluationUnitNotCompleted.equals(false))
                .flatMap(allEvaluationUnitsAreCompleted -> completeJob(jobId))
                .map(job -> JobStatus.SUCCESS.equals(job.getStatus()))
                .switchIfEmpty(Mono.just(false));
    }

    private Mono<Boolean> isAnyScrapeUnitNotCompleted(String evaluationId) {
        return scrapeUnitMongoRepository.existsByEvaluationIdAndScrapeUnitStatusIn(
                evaluationId,
                List.of(ScrapeUnitStatus.READY, ScrapeUnitStatus.PROCESSING, ScrapeUnitStatus.FAILED));
    }

    private Mono<EvaluationUnit> completeEvaluationUnit(String evaluationId) {
        return evaluationUnitMongoRepository
                .findById(evaluationId)
                .flatMap(evaluationUnit ->
                        evaluationUnitMongoRepository.save(evaluationUnit.toBuilder()
                                .status(EvaluationUnitStatus.SUCCESS)
                                .finishedAt(LocalDateTime.now())
                                .build()));
    }

    private Mono<Boolean> isAnyEvaluationUnitNotCompleted(EvaluationUnit evaluationUnit) {
        return evaluationUnitMongoRepository.existsByJobIdAndStatusIn(
                evaluationUnit.getJobId(),
                List.of(EvaluationUnitStatus.READY, EvaluationUnitStatus.PROCESSING, EvaluationUnitStatus.FAILED));
    }

    private Mono<Job> completeJob(String jobId) {
        return jobMongoRepository.findById(jobId)
                .flatMap(job ->
                        jobMongoRepository.save(job.toBuilder()
                                .status(JobStatus.SUCCESS)
                                .finishedAt(LocalDateTime.now())
                                .build()));
    }

}
