package pl.mzapisek.rescraper.common.shared.mappers;

import com.google.protobuf.Timestamp;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.jobprocessor.domain.JobStatus;
import pl.mzapisek.rescraper.common.protobuf.JobProtos;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JobMapper {

	public static JobProtos.Job mapJobToProto(Job job) {
		var builder = JobProtos.Job.newBuilder()
				.setId(job.getId())
				.setScriptName(job.getScriptName())
				.setScriptVersion(job.getScriptVersion())
				.setJobStatus(JobProtos.Job.JobStatus.valueOf(job.getStatus().toString()))
				.setCreatedAt(Timestamp.newBuilder()
						.setSeconds(job.getCreatedAt().toEpochSecond(OffsetDateTime.now().getOffset()))
						.setNanos(job.getCreatedAt().getNano())
						.build())
				.addAllAssignedPartitions(job.getAssignedPartitions());

		if (job.getParams() != null) builder.putAllParams(job.getParams());
		if (job.getUrlParams() != null) builder.addAllUrlParams(job.getUrlParams());
		if (job.getErrorMessage() != null) builder.setErrorMessage(job.getErrorMessage());
		if (job.getFinishedAt() != null) {
			builder.setFinishedAt(Timestamp.newBuilder()
					.setSeconds(job.getFinishedAt().toEpochSecond(OffsetDateTime.now().getOffset()))
					.setNanos(job.getFinishedAt().getNano())
					.build());
		}

		return builder.build();
	}

	@SuppressWarnings("Duplicates")
	public static Job mapProtoToJob(JobProtos.Job jobProto) {
		var builder = Job.builder()
				.id(jobProto.getId())
				.status(JobStatus.valueOf(jobProto.getJobStatus().name()))
				.scriptName(jobProto.getScriptName())
				.scriptVersion(jobProto.getScriptVersion())
				.createdAt(LocalDateTime.ofEpochSecond(jobProto.getCreatedAt().getSeconds(),
						jobProto.getCreatedAt().getNanos(), OffsetDateTime.now().getOffset()))
				.assignedPartitions(jobProto.getAssignedPartitionsList())
				.params(jobProto.getParamsMap())
				.urlParams(jobProto.getUrlParamsList())
				.rerun(jobProto.getRerun());

		if (jobProto.hasFinishedAt()) {
			builder.finishedAt(LocalDateTime.ofEpochSecond(jobProto.getFinishedAt().getSeconds(),
					jobProto.getFinishedAt().getNanos(), OffsetDateTime.now().getOffset()));
		}
		if (!jobProto.getErrorMessage().equals(""))
			builder.errorMessage(jobProto.getErrorMessage());

		return builder.build();
	}

}