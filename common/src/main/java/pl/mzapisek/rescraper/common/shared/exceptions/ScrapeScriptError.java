package pl.mzapisek.rescraper.common.shared.exceptions;

public class ScrapeScriptError extends RuntimeException {

    public ScrapeScriptError(String message) {
        super(message);
    }

    public ScrapeScriptError(Throwable cause) {
        super(cause);
    }

    public ScrapeScriptError(String message, Throwable cause) {
        super(message, cause);
    }

}
