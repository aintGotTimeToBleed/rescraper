package pl.mzapisek.rescraper.common.jobprocessor.domain;

public enum JobStatus {
    READY,
    PROCESSING,
    SUCCESS,
    CANCELLED,
    FAILED
}
