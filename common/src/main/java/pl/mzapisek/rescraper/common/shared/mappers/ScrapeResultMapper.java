package pl.mzapisek.rescraper.common.shared.mappers;

import com.google.protobuf.Timestamp;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.mzapisek.rescraper.common.protobuf.ScrapeResultProtos;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ScrapeResultMapper {

    public static ScrapeResultProtos.ScrapeResult mapScrapeResultToProto(ScrapeResult scrapeResult) {
        return ScrapeResultProtos.ScrapeResult.newBuilder()
                .setId(scrapeResult.getId())
                .setJobId(scrapeResult.getJobId())
                .setEvaluationId(scrapeResult.getEvaluationId())
                .setScrapeUnitId(scrapeResult.getScrapeUnitId())
                .setUrlParam(scrapeResult.getUrlParam())
                .setJsonValue(scrapeResult.getJsonValue())
                .setCreatedAt(Timestamp.newBuilder()
                        .setSeconds(scrapeResult.getCreatedAt().toEpochSecond(OffsetDateTime.now().getOffset()))
                        .setNanos(scrapeResult.getCreatedAt().getNano())
                        .build())
                .build();
    }

    public static ScrapeResult mapProtoToScrapeResult(ScrapeResultProtos.ScrapeResult scrapeResultProto) {
        return ScrapeResult.builder()
                .id(scrapeResultProto.getId())
                .jobId(scrapeResultProto.getJobId())
                .evaluationId(scrapeResultProto.getEvaluationId())
                .scrapeUnitId(scrapeResultProto.getScrapeUnitId())
                .urlParam(scrapeResultProto.getUrlParam())
                .jsonValue(scrapeResultProto.getJsonValue())
                .createdAt(LocalDateTime.ofEpochSecond(scrapeResultProto.getCreatedAt().getSeconds(),
                        scrapeResultProto.getCreatedAt().getNanos(), OffsetDateTime.now().getOffset()))
                .build();
    }

}