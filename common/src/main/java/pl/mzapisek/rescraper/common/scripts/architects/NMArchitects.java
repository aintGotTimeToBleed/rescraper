package pl.mzapisek.rescraper.common.scripts.architects;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.scripts.ScrapeScriptService;
import pl.mzapisek.rescraper.common.shared.services.MetricsService;
import pl.mzapisek.rescraper.common.shared.services.ScriptUtils;
import pl.mzapisek.rescraper.common.shared.services.WebDriverService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Log4j2
@Service
@RequiredArgsConstructor
public class NMArchitects implements ScrapeScript {

    private static final String REQUEST_URL = "https://nmbea.force.com/nmbea/s/roster";
    private static final Integer SCROLL_LINE_HEIGHT = 34;

    private final WebDriverService webDriverService;
    private final NMArchitectsService nmArchitectsService;
    private final NMArchitectsScrapeService nmArchitectsScrapeService;
    private final ObjectMapper objectMapper;
    private final MetricsService metricsService;

    @Override
    public String getScriptName() {
        return this.getClass().getSimpleName();
    }

    @Override
    public Integer getVersion() {
        return 1;
    }

    @Override
    public String getHeader() {
        return ScrapeScriptService.headerGenerator(List.of(), "");
    }

    @Override
    public String getRow(String json) {
        return null;
    }

    @Override
    public List<EvaluationUnit> process(Job job) {
        return Collections.singletonList(EvaluationUnit.builder().build());
    }

    @Override
    public List<ScrapeUnit> process(EvaluationUnit evaluationUnit) {
        RemoteWebDriver webDriver = null;

        try {
            webDriver = webDriverService.create();
            webDriver.navigate().to(REQUEST_URL);

            Proxy proxy = (Proxy) webDriver.getCapabilities().getCapability("proxy");
            metricsService.proxyCounterSuccess(proxy.getHttpProxy());

            ScriptUtils.sleep(5);
        } catch (Exception e) {
            // evaluationUnit.setStatus(EvaluationUnitStatus.FAILED.toString());
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            if (webDriver != null) {
                webDriver.quit();
            }
        }

        return nmArchitectsService.createScrapeUnits(getStates(webDriver));
    }

    @Override
    public List<ScrapeResult> process(ScrapeUnit scrapeUnit) {
        RemoteWebDriver webDriver = null;
        List<ScrapeResult> scrapeResults = new ArrayList<>();
        try {
            webDriver = webDriverService.create();
            webDriver.navigate().to(REQUEST_URL);

            Proxy proxy = (Proxy) webDriver.getCapabilities().getCapability("proxy");
            metricsService.proxyCounterSuccess(proxy.getHttpProxy());

            ScriptUtils.sleep(5);

            for (String state : scrapeUnit.getUrlParams()) {
                scrapeResults.addAll(scrapeEach(state, scrapeUnit, webDriver));
            }
        } catch (Exception e) {
//            scrapeUnit.setScrapeStatus(ScrapeUnitStatus.FAILED);
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            if (webDriver != null) {
                webDriver.quit();
            }
        }

        return scrapeResults;
    }

    private List<ScrapeResult> getLicenses(String state, ScrapeUnit scrapeUnit, RemoteWebDriver webDriver)
            throws JsonProcessingException {
        Document document = Jsoup.parse(webDriver.getPageSource());

        if (document.text().contains("No Architects found")) {
            return List.of();
        }

        List<ScrapeResult> scrapeResults = new ArrayList<>();
        Elements elements = document.select("div.card");
        for (Element el : elements) {
            final NMArchitectsLicense license = nmArchitectsScrapeService.getLicense(el);
            final String licenseJson = objectMapper.writeValueAsString(license);
            final ScrapeResult scrapeResult = ScrapeResult.builder()
                    .urlParam(state)
                    .jsonValue(licenseJson)
                    .build();

            scrapeResults.add(scrapeResult);

            log.info(licenseJson);
        }

        return scrapeResults;
    }

    private List<String> getStates(RemoteWebDriver webDriver) {
        clickRefineSearch(webDriver);
        clickStateInput(webDriver);

        return scrapeStates(webDriver);
    }

    private List<String> scrapeStates(RemoteWebDriver webDriver) {
        List<String> states = new ArrayList<>();
        Document document = Jsoup.parse(webDriver.getPageSource());
        Elements elements = document.select("span.slds-truncate");
        for (Element el : elements) {
            if (!el.text().equalsIgnoreCase("All")) {
                states.add(el.text());
            }
        }

        return states;
    }

    private void clickStateInput(RemoteWebDriver webDriver) {
        String stateXPath = "//input[@name='state']";
        final WebElement element = webDriver.findElement(By.xpath(stateXPath));
        element.click();
    }

    private void selectState(String state, Integer number, RemoteWebDriver webDriver) {
        final WebElement element = webDriver.findElement(By.xpath("//input[@name='state']"));
        element.click();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement scroll = webDriver.findElement(By.xpath(
                "//div[@class='slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left slds-dropdown_length-with-icon-10']"));
        webDriver.executeScript("arguments[0].scrollTop=" + number * SCROLL_LINE_HEIGHT + ";", scroll);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        final WebElement element1 = webDriver.findElement(By.xpath("//span[@title='" + state + "']"));
        element1.click();
    }

    private void clickRefineSearch(RemoteWebDriver webDriver) {
        List<WebElement> next = webDriver.findElements(By.xpath("//button"));
        for (WebElement we : next) {
            final String text = we.getText();
            if (text.contains("Refine your Search")) {
                if (webDriver.findElements(By.xpath("//input[@name='company']")).isEmpty()) {
                    we.click();
                    return;
                }
            }
        }
    }

    private void clickSearch(RemoteWebDriver webDriver) {
        List<WebElement> next = webDriver.findElements(By.xpath("//button"));
        for (WebElement we : next) {
            final String text = we.getText();
            if (text.contains("Search")) {
                we.click();
                return;
            }
        }
    }

    private boolean hasNextPage(RemoteWebDriver webDriver) {
        List<WebElement> next = webDriver.findElements(By.xpath("//button"));
        for (WebElement we : next) {
            final String text = we.getText();
            if (text.contains("next")) {
                if (we.isEnabled()) {
                    return true;
                }
            }
        }

        return false;
    }

    private void clickNextPage(RemoteWebDriver webDriver) {
        List<WebElement> next = webDriver.findElements(By.xpath("//button"));
        for (WebElement we : next) {
            final String text = we.getText();
            if (text.contains("next")) {
                if (we.isEnabled()) {
                    log.info("next page");
                    we.click();
                    return;
                }
            }
        }
    }


    private List<ScrapeResult> scrapeEach(String state, ScrapeUnit scrapeUnit, RemoteWebDriver webDriver)
            throws JsonProcessingException {
        final Integer stateNumber = Integer.valueOf(scrapeUnit.getParams().get("stateNumber"));

        log.info("STATE: " + state);
        log.info("STATE_NUMBER: " + stateNumber);

        clickRefineSearch(webDriver);
        ScriptUtils.sleep(2);
        selectState(state, stateNumber, webDriver);
        ScriptUtils.sleep(3);
        clickSearch(webDriver);

        ScriptUtils.sleep(5);

        List<ScrapeResult> scrapeResults = getLicenses(state, scrapeUnit, webDriver);
        while (hasNextPage(webDriver)) {
            clickNextPage(webDriver);
            ScriptUtils.sleep(3);
            scrapeResults.addAll(getLicenses(state, scrapeUnit, webDriver));
        }

        return scrapeResults;
    }

    @Override
    public Map<String, String> getDefaultParams() {
        return null;
    }

    @Override
    public List<String> getDefaultUrlParams() {
        return null;
    }

}
