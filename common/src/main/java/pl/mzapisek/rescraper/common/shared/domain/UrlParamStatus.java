package pl.mzapisek.rescraper.common.shared.domain;

public enum UrlParamStatus {
    READY,
    PROCESSING,
    PROCESSED,
    FAILED
}
