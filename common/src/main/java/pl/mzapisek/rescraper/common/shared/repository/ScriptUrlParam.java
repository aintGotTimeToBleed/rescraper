package pl.mzapisek.rescraper.common.shared.repository;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

// todo make it immutable
@Data
@Document
public class ScriptUrlParam {

    @Id
    private String id;
    private String scriptName;
    private Integer scriptVersion;
    private List<String> params;

}
