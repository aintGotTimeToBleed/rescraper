package pl.mzapisek.rescraper.common.shared;

public interface Processor<T, R> {

    R process(T msg);

//    R filter(T msg);

//    R complete(T msg);

}
