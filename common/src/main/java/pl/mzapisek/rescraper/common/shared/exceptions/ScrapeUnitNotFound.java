package pl.mzapisek.rescraper.common.shared.exceptions;

public class ScrapeUnitNotFound extends RuntimeException {

    private static final String ERROR_MSG = "Scrape unit not found: %s";

    public ScrapeUnitNotFound() {
    }

    public ScrapeUnitNotFound(String scrapeUnitId) {
        super(String.format(ERROR_MSG, scrapeUnitId));
    }

}
