package pl.mzapisek.rescraper.common.jobprocessor.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;

@Repository
public interface JobMongoRepository extends ReactiveMongoRepository<Job, String> {
}
