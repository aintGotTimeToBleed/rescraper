package pl.mzapisek.rescraper.common.shared.exceptions;

public class JobCancelled extends RuntimeException {

    private static final String ERROR_MSG = "Job has been cancelled: %s";
    private static final String ERROR_MSG_SCRAPE_UNIT = "Job has been cancelled: %s, evaluation unit: %s, scrape unit: %s";

    public JobCancelled(String jobId) {
        super(String.format(ERROR_MSG, jobId));
    }

    public JobCancelled(String jobId, String evaluationUnitId, String scrapeUnitId) {
        super(String.format(ERROR_MSG_SCRAPE_UNIT, jobId, evaluationUnitId, scrapeUnitId));
    }

}
