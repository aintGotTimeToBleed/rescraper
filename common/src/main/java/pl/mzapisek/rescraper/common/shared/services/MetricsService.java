package pl.mzapisek.rescraper.common.shared.services;

import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;

@RequiredArgsConstructor
@Service
@Log4j2
public class MetricsService {
// rename service

    private static final String SCRIPT_TAG = "script";
    private static final String STATUS_TAG = "status";

    private final MeterRegistry registry;

    public void evaluationUnitTimer(EvaluationUnit evaluationUnit) {
        try {
            registry.counter("rescraper.process.evaluation-unit",
                    SCRIPT_TAG, evaluationUnit.getScriptName(),
                    STATUS_TAG, evaluationUnit.getStatus().toString()).increment();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    public void scrapeUnitTimer(ScrapeUnit scrapeUnit) {
        registry.counter("rescraper.process.scrape-unit",
                SCRIPT_TAG, scrapeUnit.getScriptName(),
                STATUS_TAG, scrapeUnit.getScrapeUnitStatus().toString()).increment();
    }

    public void jobTimer(Job job) {
        registry.counter("rescraper.process.job",
                SCRIPT_TAG, job.getScriptName(),
                STATUS_TAG, job.getStatus().toString()).increment();
    }

    public void proxyCounterSuccess(String proxy) {
        try {
            registry.counter("rescraper.proxy-success-counter",
                    "proxyUrl", proxy).increment();
        } catch (Exception e) {
            log.error("Proxy counter: " + e.getMessage());
            registry.counter("rescraper.proxy-fail-counter").increment();
        }
    }

    public void scrapeResultCounter() {
        registry.counter("rescraper.scrape-result-counter").increment();
    }
}
