package pl.mzapisek.rescraper.common.shared.mappers;

import com.google.protobuf.Timestamp;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.mzapisek.rescraper.common.protobuf.ScrapeUnitProtos;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ScrapeUnitMapper {

    @SuppressWarnings("Duplicates")
    public static ScrapeUnitProtos.ScrapeUnit mapScrapeUnitToProto(ScrapeUnit scrapeUnit) {
        var builder = ScrapeUnitProtos.ScrapeUnit.newBuilder()
                .setId(scrapeUnit.getId())
                .setEvaluationId(scrapeUnit.getEvaluationId())
                .setJobId(scrapeUnit.getJobId())
                .setScriptName(scrapeUnit.getScriptName())
                .setScriptVersion(scrapeUnit.getScriptVersion())
                .addAllUrlParams(scrapeUnit.getUrlParams())
                .setCreatedAt(Timestamp.newBuilder()
                        .setSeconds(scrapeUnit.getCreatedAt().toEpochSecond(OffsetDateTime.now().getOffset()))
                        .setNanos(scrapeUnit.getCreatedAt().getNano())
                        .build())
                .setScrapeUnitStatus(ScrapeUnitProtos.ScrapeUnit.ScrapeUnitStatus.valueOf(scrapeUnit.getScrapeUnitStatus().toString()));

        if (scrapeUnit.getParams() != null)
            builder.putAllParams(scrapeUnit.getParams());
        if (scrapeUnit.getFinishedAt() != null) {
            builder.setFinishedAt(Timestamp.newBuilder()
                    .setSeconds(scrapeUnit.getFinishedAt().toEpochSecond(OffsetDateTime.now().getOffset()))
                    .setNanos(scrapeUnit.getFinishedAt().getNano())
                    .build());
        }
        if (scrapeUnit.getErrorMessage() != null)
            builder.setErrorMessage(scrapeUnit.getErrorMessage());

        return builder.build();
    }

    @SuppressWarnings("Duplicates")
    public static ScrapeUnit mapProtoToScrapeUnit(ScrapeUnitProtos.ScrapeUnit scrapeUnitProto) {
        var builder = ScrapeUnit.builder()
                .id(scrapeUnitProto.getId())
                .jobId(scrapeUnitProto.getJobId())
                .evaluationId(scrapeUnitProto.getEvaluationId())
                .scriptName(scrapeUnitProto.getScriptName())
                .scriptVersion(scrapeUnitProto.getScriptVersion())
                .urlParams(scrapeUnitProto.getUrlParamsList())
                .createdAt(LocalDateTime.ofEpochSecond(scrapeUnitProto.getCreatedAt().getSeconds(),
                        scrapeUnitProto.getCreatedAt().getNanos(), OffsetDateTime.now().getOffset()))
                .scrapeUnitStatus(ScrapeUnitStatus.valueOf(scrapeUnitProto.getScrapeUnitStatus().name()));

        if (scrapeUnitProto.hasFinishedAt()) {
            builder.finishedAt(LocalDateTime.ofEpochSecond(scrapeUnitProto.getFinishedAt().getSeconds(),
                    scrapeUnitProto.getFinishedAt().getNanos(), OffsetDateTime.now().getOffset()));
        }
        if (!scrapeUnitProto.getErrorMessage().equals(""))
            builder.errorMessage(scrapeUnitProto.getErrorMessage());

        return builder.build();
    }

}