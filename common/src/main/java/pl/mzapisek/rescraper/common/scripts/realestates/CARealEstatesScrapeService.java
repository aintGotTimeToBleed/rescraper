package pl.mzapisek.rescraper.common.scripts.realestates;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
public class CARealEstatesScrapeService {

    CARealEstatesLicense getLicense(Document document) {
        return CARealEstatesLicense.builder()
                .name(getElement(document, "tr:has(strong):contains(Name:) > td:nth-child(2)"))
                .licenseNumber(getElement(document, "tr:has(strong):contains(License ID:) > td:nth-child(2)"))
                .licenseType(getElement(document, "tr:has(strong):contains(License Type:) > td:nth-child(2)"))
                .licenseStatus(getElement(document, "tr:has(strong):contains(License Status:) > td:nth-child(2)"))
                .expiration(getElement(document, "tr:has(strong):contains(Expiration Date:) > td:nth-child(2)"))
                .brokerIssue(getElement(document, "tr:has(strong):contains(Broker License Issued:) > td:nth-child(2)"))
                .salesPersonIssue(getElement(document, "tr:has(strong):contains(Salesperson License Issued:) > td:nth-child(2)"))
                .corporationPersonIssue(getElement(document, "tr:has(strong):contains(Corporation License Issued:) > td:nth-child(2)"))
                .formerName(getElement(document, "tr:has(strong):contains(Former Name(s):) > td:nth-child(2)"))
                .address(getElement(document, "tr:has(strong):contains(Mailing Address:) > td:nth-child(2)"))
                .office(getElement(document, "tr:has(strong):contains(Main Office:) > td:nth-child(2)"))
                .dba(getElement(document, "tr:has(strong):contains(DBA) > td:nth-child(2)"))
                .branches(getElement(document, "tr:has(strong):contains(Branches:) > td:nth-child(2)"))
                .build();
    }

    private String getElement(Document document, String css) {
        Elements select = document.select(css);
        if (select.size() > 0) {
            return cleaner(select.first().text());
        }

        return "";
    }

    private String cleaner(String text) {
        if (text == null) {
            return "";
        }

        return text.trim()
                .replaceAll("&nbsp;", "")
                .replaceAll("<br>", " ")
                .replaceAll("(\r\n|\n)", "");
    }

    List<String> getDetailParams(Document document) {
        HashSet<String> linksIds = new HashSet<>();
        Elements aElements = document.select("a");
        for (Element element : aElements) {
            String href = element.attr("href");
            if (href != null && href.contains("pplinfo.asp?License_id=")) {
                String[] split = href.split("=");
                linksIds.add(split[1]);
            }
        }

        return new ArrayList<>(linksIds);
    }

}
