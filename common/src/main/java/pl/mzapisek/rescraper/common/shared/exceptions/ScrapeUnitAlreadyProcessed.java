package pl.mzapisek.rescraper.common.shared.exceptions;

public class ScrapeUnitAlreadyProcessed extends RuntimeException {

    private static final String ERROR_MSG = "Scrape unit has been processed already: %s";

    public ScrapeUnitAlreadyProcessed() {
    }

    public ScrapeUnitAlreadyProcessed(String scrapeUnitId) {
        super(String.format(ERROR_MSG, scrapeUnitId));
    }

}
