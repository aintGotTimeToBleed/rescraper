package pl.mzapisek.rescraper.common.scripts;

import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class UrlDefaultParamsGenerator {

    private UrlDefaultParamsGenerator() {
    }

    public static List<String> singleLetter() {
        return IntStream.rangeClosed('a', 'z')
                .mapToObj(elem -> (char) elem)
                .map(Object::toString)
                .collect(toList());
    }

    public static List<String> doubleLetter() {
        return singleLetter()
                .stream()
                .map(firstLetter -> IntStream.rangeClosed('a', 'z')
                        .mapToObj(elem -> (char) elem)
                        .map(secondLetter -> firstLetter + secondLetter.toString())
                        .collect(toList()))
                .flatMap(Collection::stream)
                .collect(toList());
    }

    public static List<String> tripleLetter() {
        return doubleLetter()
                .stream()
                .map(firstLetter -> IntStream.rangeClosed('a', 'z')
                        .mapToObj(elem -> (char) elem)
                        .map(secondLetter -> firstLetter + secondLetter.toString())
                        .collect(toList()))
                .flatMap(Collection::stream)
                .collect(toList());
    }

}
