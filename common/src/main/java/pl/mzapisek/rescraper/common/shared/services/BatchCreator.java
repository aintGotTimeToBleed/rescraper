package pl.mzapisek.rescraper.common.shared.services;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class BatchCreator<T> {

    public List<List<T>> createBatch(List<T> messages, Integer numberOfPartitions) {
        AtomicInteger atomicInteger = new AtomicInteger();

        final double floor = Math.ceil((double) messages.size() / numberOfPartitions);

        return new ArrayList<>(messages.stream()
                .collect(Collectors.groupingBy(it -> atomicInteger.getAndIncrement() / (int) floor))
                .values());
    }

}
