package pl.mzapisek.rescraper.common.scrapeprocessor.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Document
@Builder(toBuilder = true)
@ToString
@EqualsAndHashCode
public class ScrapeResult {

    @Id
    private String id;
    private String jobId;
    private String evaluationId;
    private String scrapeUnitId;
    private String urlParam;
    private String jsonValue;
    @DateTimeFormat(iso = DATE_TIME)
    private LocalDateTime createdAt;

}

