package pl.mzapisek.rescraper.common.shared.services;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.shared.services.RedisService;

import java.net.MalformedURLException;
import java.net.URL;

@Service
public class WebDriverService {

    private final String seleniumHubHost;
    private final RedisService redisService;

    public WebDriverService(@Value("${selenium.hub.host}") String seleniumHubHost, RedisService redisService) {
        this.seleniumHubHost = seleniumHubHost;
        this.redisService = redisService;
    }

    public RemoteWebDriver create() throws MalformedURLException {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setPageLoadStrategy(PageLoadStrategy.NONE);

        // todo fix proxy rotator
        // ability to disable and enable proxy rotation
        // something was wrong with proxy and CARealestates, too many redirects, maybe that proxies are gone
        // Proxy proxy = new Proxy().setProxyType(Proxy.ProxyType.MANUAL).setHttpProxy(redisService.getRandomProxyUrl());
        // chromeOptions.setProxy(proxy);

        MutableCapabilities mutableCapabilities = new MutableCapabilities(chromeOptions);

        return new RemoteWebDriver(new URL(seleniumHubHost), mutableCapabilities);
    }

}
