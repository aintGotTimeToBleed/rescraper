package pl.mzapisek.rescraper.common.scrapeprocessor.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@Builder(toBuilder = true)
@Document
@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class ScrapeUnit {

    @Id
    private String id;
    private String jobId;
    private String evaluationId;
    private String scriptName;
    private Integer scriptVersion;
    private List<String> urlParams;
    private Map<String, String> params;
    @DateTimeFormat(iso = DATE_TIME)
    private LocalDateTime createdAt;
    // may be null
    @DateTimeFormat(iso = DATE_TIME)
    private LocalDateTime finishedAt;
    private ScrapeUnitStatus scrapeUnitStatus;
    // may be null
    private String errorMessage;

}
