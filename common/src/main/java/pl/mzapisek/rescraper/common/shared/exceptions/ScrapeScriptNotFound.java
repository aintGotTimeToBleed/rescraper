package pl.mzapisek.rescraper.common.shared.exceptions;

public class ScrapeScriptNotFound extends RuntimeException {

    public ScrapeScriptNotFound(String message) {
        super(message);
    }

}