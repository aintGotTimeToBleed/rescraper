package pl.mzapisek.rescraper.common.scripts.attorneys;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

@Service
public class FLAttorneysScrapeService {

    public FLAttorneysLicense getLicence(Document doc) {
        FLAttorneysLicense license = new FLAttorneysLicense();

        Element mProfile = doc.getElementById("mProfile");

        Elements full = mProfile.getElementsByClass("full");
        license.setName(getFirstElementValue(full));

        Elements rows = mProfile.getElementsByClass("row");
        for (Element el : rows) {
            Elements title = el.select("div:nth-child(1) > label");

            if (title.size() == 0) {
                continue;
            }

            String titleText = title.first().text();

            if (titleText.contains("Bar Number")) {
                license.setBarNumber(getFirstElementValue(el.select("div:nth-child(2) > p")));
            }
            if (titleText.contains("Mail Address")) {
                Elements mailPs = el.select("div:nth-child(2) > p");
                for (Element p : mailPs) {
                    if (p.text().contains("Office:")) {
                        license.setOfficePhone(getFirstElementValue(p.select("a")));
                    } else if (p.text().contains("Cell:")) {
                        license.setCellPhone(getFirstElementValue(p.select("a")));
                    } else if (p.text().contains("Fax:")) {
                        license.setFax(p.text().replace("Fax:", "").trim());
                    } else {
                        license.setMailAddress(p.text().replace("<br>", "").trim());
                    }
                }
            }
            if (titleText.contains("Email")) {
                license.setEmail(getFirstElementValue(el.select("div:nth-child(2) > p > a")));
            }
            if (titleText.contains("Personal Bar URL")) {
                license.setBarURL(getFirstElementValue(el.select("div:nth-child(2) > p")));
            }
            if (titleText.contains("County")) {
                license.setCounty(getFirstElementValue(el.select("div:nth-child(2) > p")));
            }
            if (titleText.contains("Circuit")) {
                license.setCircuit(getFirstElementValue(el.select("div:nth-child(2) > p")));
            }
            if (titleText.contains("Admitted")) {
                license.setAdmitted(getFirstElementValue(el.select("div:nth-child(2) > p")));
            }
            if (titleText.contains("Law School")) {
                license.setLawSchool(getFirstElementValue(el.select("div:nth-child(2) > p")));
            }
        }

        return license;
    }

    private String getFirstElementValue(Elements elements) {
        if (elements.size() > 0) {
            return elements.first().text().trim();
        }
        return null;
    }

}
