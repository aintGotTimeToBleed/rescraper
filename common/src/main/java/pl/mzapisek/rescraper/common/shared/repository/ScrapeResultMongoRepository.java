package pl.mzapisek.rescraper.common.shared.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ScrapeResultMongoRepository extends ReactiveMongoRepository<ScrapeResult, String> {

    Mono<Boolean> existsByJsonValueAndJobId(String jsonValue, String jobId);

    Flux<ScrapeResult> findByJobId(String jobId);

}