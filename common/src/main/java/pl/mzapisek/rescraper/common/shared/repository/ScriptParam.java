package pl.mzapisek.rescraper.common.shared.repository;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

// todo make it immutable
// remove?
@Builder
@Data
@Document
public class ScriptParam {

    @Id
    private String id;
    private String scriptName;
    private Integer scriptVersion;
    private String param;
    private String value;

}
