package pl.mzapisek.rescraper.common.shared.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@ConfigurationProperties(prefix = "kafka.consumer")
public class KafkaConsumerProperties {

    @NotBlank
    private String consumerGroup;
    @NotBlank
    private String autoOffsetReset;
    private String maxPollRecords;
    private String maxPollIntervalMs;
    private String autoCommitInterval;

}
