package pl.mzapisek.rescraper.common.scrapeprocessor.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Repository
public interface ScrapeUnitMongoRepository extends ReactiveMongoRepository<ScrapeUnit, String> {

    Flux<ScrapeUnit> findByScrapeUnitStatusAndJobId(ScrapeUnitStatus status, String jobId);

    Mono<Long> countByJobId(String jobId);

    Mono<Long> countByJobIdAndScrapeUnitStatus(String jobId, ScrapeUnitStatus scrapeStatus);

    Mono<Boolean> existsByEvaluationIdAndScrapeUnitStatusIn(String evaluationId, List<ScrapeUnitStatus> scrapeUnitStatuses);

    // find by id and status

}
