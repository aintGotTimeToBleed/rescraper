package pl.mzapisek.rescraper.common.scripts.attorneys;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnitStatus;
import pl.mzapisek.rescraper.common.evaluationprocessor.repository.EvaluationUnitMongoRepository;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

// todo remove?
@Service
@RequiredArgsConstructor
public class FLAttorneysService {

//    private final EvaluationUnitMongoRepository evaluationUnitMongoRepository;
//    private final ScrapeUnitMongoRepository scrapeUnitMongoRepository;

//    Flux<EvaluationUnit> saveToDb(Set<Integer> licenceNumbers, String jobId) {
//        return evaluationUnitMongoRepository.saveAll(mapLicenseNumbersToEvaluationUnit(licenceNumbers, jobId));
//    }

    private List<EvaluationUnit> mapLicenseNumbersToEvaluationUnit(Set<Integer> licenceNumbers, String jobId) {
        return licenceNumbers.stream()
                .map(licenceNumber -> create(licenceNumber, jobId))
                .collect(Collectors.toList());
    }

    private EvaluationUnit create(Integer licenseNumber, String jobId) {
        return EvaluationUnit.builder()
                .jobId(jobId)
                .element(licenseNumber.toString())
                .status(EvaluationUnitStatus.READY)
                .build();
    }

}
