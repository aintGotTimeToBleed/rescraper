package pl.mzapisek.rescraper.common.evaluationprocessor.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnitStatus;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Repository
public interface EvaluationUnitMongoRepository extends ReactiveMongoRepository<EvaluationUnit, String> {

    Flux<EvaluationUnit> findByStatusAndJobId(EvaluationUnitStatus status, String jobId);

    Flux<EvaluationUnit> findByJobId(String jobId);

    Mono<Long> countByJobId(String jobId);

    Mono<Long> countByJobIdAndStatus(String jobId, EvaluationUnitStatus status);

    Mono<Boolean> existsByJobIdAndStatusIn(String evaluationId, List<EvaluationUnitStatus> statuses);

}
