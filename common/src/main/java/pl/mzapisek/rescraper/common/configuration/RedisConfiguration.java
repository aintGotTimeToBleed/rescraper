package pl.mzapisek.rescraper.common.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.Jedis;

@Configuration
public class RedisConfiguration {

    @Value("${redis.host}")
    private String redisUrl;
    @Value("${redis.port}")
    private int redisPort;
//    @Value("${redis.timeout.ms}")
    private int timeoutMs = 200;

    @Bean
    public Jedis jedis() {
        return new Jedis(redisUrl, redisPort, timeoutMs);
    }

}
