package pl.mzapisek.rescraper.common.shared.mappers;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.mzapisek.rescraper.common.api.Task;
import pl.mzapisek.rescraper.common.protobuf.TaskProtos;

import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TaskMapper {

    public static Task mapTaskProtoToTask(TaskProtos.Task task) {
        return Task.builder()
                .id(task.getId())
                .taskName(task.getTaskName())
                .cronExpression(task.getCronExpression())
                .scriptName(task.getScriptName())
                .scriptVersion(task.getScriptVersion())
                .description(task.getDescription())
                .build();
    }

}
