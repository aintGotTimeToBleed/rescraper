package pl.mzapisek.rescraper.common.shared.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@ConfigurationProperties(prefix = "kafka.producer")
public class KafkaProducerProperties {

    @NotBlank
    private String acks;
    @NotBlank
    private String batchSize;

}
