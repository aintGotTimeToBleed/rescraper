package pl.mzapisek.rescraper.common.scripts.realestates;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.scripts.ScrapeScriptService;
import pl.mzapisek.rescraper.common.scripts.UrlDefaultParamsGenerator;
import pl.mzapisek.rescraper.common.shared.exceptions.ScrapeScriptError;
import pl.mzapisek.rescraper.common.shared.services.WebDriverService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Log4j2
@RequiredArgsConstructor
@Service
public class CARealEstates implements ScrapeScript {

    private static final Integer VERSION = 1;
    private static final String SCRIPT_NAME = "CARealEstates";
    private static final String REQUEST_URL = "http://www2.dre.ca.gov/PublicASP/pplinfo.asp";
    private static final String DETAIL_PREFIX = "https://www2.dre.ca.gov/PublicASP/pplinfo.asp?License_id=";
    private static final String COLUMN_SEPARATOR = "|";
    private static final String LINE_SEPARATOR = "\r\n";
    private static final List<String> COLUMNS = List.of(
            "name", "licenseNumber", "licenseType", "licenseStatus", "expiration", "brokerIssue", "salesPersonIssue",
            "corporationPersonIssue", "formerName", "address", "office", "dba", "branches");

    private static final Integer SLEEP_MS = 2500;

    private final WebDriverService webDriverService;
    private final CARealEstatesScrapeService caRealestatesScrapeService;
    private final ObjectMapper objectMapper;

    @Override
    public String getScriptName() {
        return SCRIPT_NAME;
    }

    @Override
    public Integer getVersion() {
        return VERSION;
    }

    @Override
    public String getHeader() {
        return ScrapeScriptService.headerGenerator(COLUMNS, COLUMN_SEPARATOR).concat(LINE_SEPARATOR);
    }

    @Override
    public String getRow(String json) {
        try {
            var license = objectMapper.readValue(json, CARealEstatesLicense.class);
            return new StringBuilder()
                    .append(license.getName()).append(COLUMN_SEPARATOR)
                    .append(license.getLicenseNumber()).append(COLUMN_SEPARATOR)
                    .append(license.getLicenseType()).append(COLUMN_SEPARATOR)
                    .append(license.getLicenseStatus()).append(COLUMN_SEPARATOR)
                    .append(license.getExpiration()).append(COLUMN_SEPARATOR)
                    .append(license.getBrokerIssue()).append(COLUMN_SEPARATOR)
                    .append(license.getSalesPersonIssue()).append(COLUMN_SEPARATOR)
                    .append(license.getCorporationPersonIssue()).append(COLUMN_SEPARATOR)
                    .append(license.getFormerName()).append(COLUMN_SEPARATOR)
                    .append(license.getAddress()).append(COLUMN_SEPARATOR)
                    .append(license.getOffice()).append(COLUMN_SEPARATOR)
                    .append(license.getDba()).append(COLUMN_SEPARATOR)
                    .append(license.getBranches()).append(COLUMN_SEPARATOR)
                    .append(LINE_SEPARATOR).toString();
        } catch (JsonProcessingException e) {
            throw new RuntimeException("error reading line for json: " + json);
        }
    }

    @Override
    public Integer getMaxParallelism() {
        return 2;
    }

    @Override
    public List<EvaluationUnit> process(Job job) {
        return ScrapeScriptService.createEvaluationUnits(job);
    }

    @Override
    public List<ScrapeUnit> process(EvaluationUnit evaluationUnit) {
        RemoteWebDriver webDriver = null;
        List<ScrapeUnit> scrapeUnits = new ArrayList<>();
        try {
            webDriver = webDriverService.create();
            webDriver.navigate().to(REQUEST_URL);

            Thread.sleep(10_000);

            WebElement licenseeName = webDriver.findElement(By.name("LICENSEE_NAME"));
            licenseeName.sendKeys(evaluationUnit.getElement());

            WebElement submit = webDriver.findElement(By.xpath("//input[@type='submit']"));
            submit.click();

            // todo https://stackoverflow.com/questions/5868439/wait-for-page-load-in-selenium
            Thread.sleep(35000);

            // if no records found return empty list
            List<WebElement> nonsortabletable = webDriver.findElements(By.id("nonsortabletable"));
            if (nonsortabletable.size() == 0) {
                if (webDriver.getPageSource().contains("No matching public record was found")) {
                    log.warn("No matching public record was found");
                    return Collections.emptyList();
                }
            }

            Document document = Jsoup.parse(webDriver.getPageSource());

            if (document == null || document.text().equals("")) {
                throw new ScrapeScriptError("empty page was displayed");
            }

            List<String> params = caRealestatesScrapeService.getDetailParams(document);
            scrapeUnits.addAll(ScrapeScriptService.createScrapeUnits(params, evaluationUnit));

            List<WebElement> elements = webDriver.findElements(By.xpath("//a[@href='pplinfo.asp?NAV=2&LicenseeName=RA']"));
            if (elements.size() > 0) {
                elements.get(0).click();

                document = Jsoup.parse(webDriver.getPageSource());
                List<String> detailParams = caRealestatesScrapeService.getDetailParams(document);
                scrapeUnits.addAll(ScrapeScriptService.createScrapeUnits(detailParams, evaluationUnit));
            }

        } catch (Exception e) {
            throw new ScrapeScriptError(evaluationUnit.getId() + ": " + e.getMessage(), e);
        } finally {
            if (webDriver != null) {
                webDriver.quit();
            }
        }

        return scrapeUnits;
    }

    // todo processes looks similar in every script, refactor
    @Override
    public List<ScrapeResult> process(ScrapeUnit scrapeUnit) {
//        try {
//            Thread.sleep(60_000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        ScrapeResult scrapeResult;
        RemoteWebDriver webDriver = null;
        try {
            String detailUrlParam = scrapeUnit.getUrlParams().get(0);
            String concat = DETAIL_PREFIX.concat(detailUrlParam);

            Document doc;

            webDriver = webDriverService.create();
            webDriver.get(concat);

            // tu trzeba zmienic zeby sprawdzal zawartosc z timeoutem
            Thread.sleep(SLEEP_MS);

            String pageSource = webDriver.getPageSource();
            doc = Jsoup.parse(pageSource);

            if (doc == null || doc.text().equals("")) {
                throw new ScrapeScriptError("empty page was displayed");
            }

            CARealEstatesLicense license = caRealestatesScrapeService.getLicense(doc);
            scrapeResult = ScrapeScriptService.createScrapeResult(scrapeUnit, objectMapper.writeValueAsString(license), detailUrlParam);

        } catch (Exception e) {
            throw new ScrapeScriptError(e);
        } finally {
            if (webDriver != null) {
                webDriver.quit();
            }
        }

        return List.of(scrapeResult);
    }

    @Override
    public Map<String, String> getDefaultParams() {
        return Collections.emptyMap();
    }

    @Override
    public List<String> getDefaultUrlParams() {
        return UrlDefaultParamsGenerator.tripleLetter();
    }

}
