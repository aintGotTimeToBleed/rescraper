package pl.mzapisek.rescraper.common.scripts.architects;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class NMArchitectsLicense {

    private String licenseNo;
    private String name;
    private String company;
    private String city;
    private String zip;
    private String status;
    private String expirationDate;

}
