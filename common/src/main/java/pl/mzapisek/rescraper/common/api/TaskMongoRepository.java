package pl.mzapisek.rescraper.common.api;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface TaskMongoRepository extends ReactiveMongoRepository<Task, String> {

    Mono<Boolean> existsByScriptName(String scriptName);

    @Tailable
    Flux<Task> findWithTailableCursorBy();

}
