package pl.mzapisek.rescraper.common.shared.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotBlank;

// todo constructor binding?
@Getter
@Setter
@ConfigurationProperties(prefix = "kafka.base") // todo change to common
public class KafkaBaseProperties {

    @NotBlank
    private String bootstrapServers;
    @NotBlank
    private String clientId;
    private String jobTopic;
    private String evaluationUnitTopic;
    private String scrapeUnitTopic;
    // not blank?
    private String eventBusTopic;

}
