package pl.mzapisek.rescraper.common.scripts.architects;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import reactor.core.Disposable;

@Service
@RequiredArgsConstructor
@Log4j2
public class NMArchitectsService {

//    private final ScrapeResultMongoRepository scrapeResultMongoRepository;
//
//    // ?? a po co to było?
//    public Disposable save(ScrapeResult scrapeResult) {
//        return scrapeResultMongoRepository
//                .existsByJsonValueAndJobId(scrapeResult.getJsonValue(), scrapeResult.getJobId())
//                .subscribe(exists -> {
//                    if (!exists) {
//                        scrapeResultMongoRepository.save(scrapeResult)
//                                .subscribe(scrapeResult1 -> log.info("SAVED: " + scrapeResult1.toString()));
//                    }
//                });
//    }

    public List<ScrapeUnit> createScrapeUnits(List<String> states) {
        final AtomicInteger stateCounter = new AtomicInteger();
        return states
                .stream()
                .peek(s -> stateCounter.incrementAndGet())
                .map(state -> ScrapeUnit.builder()
                        .urlParams(List.of(state))
                        .params(Map.of("stateNumber", String.valueOf(stateCounter.get())))
                        .build())
                .collect(Collectors.toList());
    }

}
