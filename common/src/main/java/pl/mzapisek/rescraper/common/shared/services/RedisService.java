package pl.mzapisek.rescraper.common.shared.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.util.concurrent.ThreadLocalRandom;

@RequiredArgsConstructor
@Service
public class RedisService {

    private static final int TTL_IN_SECONDS = 60 * 60 * 72;

    private final Jedis jedis;

    public String getRandomProxyUrl() {
        return jedis.lindex("proxies", ThreadLocalRandom.current().nextInt(10));
    }

    public String cancelJob(String jobId) {
        return jedis.setex("cancelled:job:" + jobId, TTL_IN_SECONDS, "true");
    }

    public boolean isJobCancelled(String jobId) {
        return jedis.exists("cancelled:job:" + jobId);
    }

    public String scrapeUnitCompleted(String scrapeUnitId) {
        return jedis.setex("completed:scrapeunit:" + scrapeUnitId, TTL_IN_SECONDS, "true");
    }

    public boolean isScrapeUnitCompleted(String scrapeUnitId) {
        return jedis.exists("completed:scrapeunit:" + scrapeUnitId);
    }

}
