package pl.mzapisek.rescraper.common.evaluationprocessor.domain;

public enum EvaluationUnitStatus {
    READY,
    PROCESSING,
    SUCCESS,
    SUCCESS_WITHOUT_DATA,
    FAILED,
    CANCELLED,
    FINISHED_WITH_ERRORS
}
