package pl.mzapisek.rescraper.common.scripts.attorneys;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public final class FLAttorneysLicense {

    private String name;
    private String barNumber;
    private String mailAddress;
    private String officePhone;
    private String cellPhone;
    private String fax;
    private String email;
    private String barURL;
    private String county;
    private String circuit;
    private String admitted;
    private String lawSchool;

}
