package pl.mzapisek.rescraper.common.shared.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

// todo make immutable
@Data
@AllArgsConstructor
@Document
@Builder
public class ScrapeUnitUrlParam {

    @Id
    private String id;
    private String jobId;
    private String param;
    private UrlParamStatus status;

}
