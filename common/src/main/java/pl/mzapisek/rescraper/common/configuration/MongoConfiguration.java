package pl.mzapisek.rescraper.common.configuration;

import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@RequiredArgsConstructor
@EnableReactiveMongoRepositories(basePackages = "pl.mzapisek.rescraper")
public class MongoConfiguration extends AbstractReactiveMongoConfiguration {

    @Value("${mongo.host}")
    private String mongoHost;
    @Value("${mongo.port}")
    private int mongoPort;
    @Value("${mongo.database-name}")
    private String mongoDbName;

    @Bean
    public MongoClient reactiveMongoClient() {
        return MongoClients.create(String.format(
                "mongodb://%s:%s/%s",
                mongoHost,
                mongoPort,
                mongoDbName));
    }

    @Override
    protected String getDatabaseName() {
        return mongoDbName;
    }

}
