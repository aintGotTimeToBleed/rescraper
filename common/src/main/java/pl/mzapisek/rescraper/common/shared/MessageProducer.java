package pl.mzapisek.rescraper.common.shared;

import java.util.List;

public interface MessageProducer<T,S,U> {

    S send(T message);

    U send(List<T> messages);

    U send(List<T> messages, List<Integer> partitionNumbers);

}
