package pl.mzapisek.rescraper.common.jobprocessor.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@Builder(toBuilder = true)
@Document
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class Job {

    @Id
    private String id;
    private Map<String, String> params;
    private List<String> urlParams;
    private JobStatus status;
    private String scriptName;
    private Integer scriptVersion;
    @DateTimeFormat(iso = DATE_TIME)
    private LocalDateTime createdAt;
    @DateTimeFormat(iso = DATE_TIME)
    private LocalDateTime finishedAt;
    private boolean rerun;
    private String errorMessage;
    private List<Integer> assignedPartitions;

}

