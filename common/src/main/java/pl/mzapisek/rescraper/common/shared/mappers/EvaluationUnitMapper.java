package pl.mzapisek.rescraper.common.shared.mappers;

import com.google.protobuf.Timestamp;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnitStatus;
import pl.mzapisek.rescraper.common.protobuf.EvaluationUnitProtos;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EvaluationUnitMapper {

    public static EvaluationUnitProtos.EvaluationUnit mapEvaluationUnitToProto(EvaluationUnit evaluationUnit) {
        var builder = EvaluationUnitProtos.EvaluationUnit.newBuilder()
                .setId(evaluationUnit.getId())
                .setJobId(evaluationUnit.getJobId())
                .setElement(evaluationUnit.getElement())
                .setEvaluationUnitStatus(EvaluationUnitProtos.EvaluationUnit.EvaluationUnitStatus.valueOf(evaluationUnit.getStatus().toString()))
                .setCreatedAt(Timestamp.newBuilder()
                        .setSeconds(evaluationUnit.getCreatedAt().toEpochSecond(OffsetDateTime.now().getOffset()))
                        .setNanos(evaluationUnit.getCreatedAt().getNano())
                        .build())
                .setScriptName(evaluationUnit.getScriptName())
                .setScriptVersion(evaluationUnit.getScriptVersion())
                .addAllAssignedPartitions(evaluationUnit.getAssignedPartitions());

        if (evaluationUnit.getFinishedAt() != null) {
            builder.setFinishedAt(Timestamp.newBuilder()
                    .setSeconds(evaluationUnit.getFinishedAt().toEpochSecond(OffsetDateTime.now().getOffset()))
                    .setNanos(evaluationUnit.getFinishedAt().getNano())
                    .build());
        }
        if (evaluationUnit.getErrorMessage() != null) builder.setErrorMessage(evaluationUnit.getErrorMessage());

        return builder.build();
    }

    @SuppressWarnings("Duplicates")
    public static EvaluationUnit mapProtoToEvaluationUnit(EvaluationUnitProtos.EvaluationUnit evaluationUnitProto) {
        var builder = EvaluationUnit.builder()
                .id(evaluationUnitProto.getId())
                .jobId(evaluationUnitProto.getJobId())
                .element(evaluationUnitProto.getElement())
                .status(EvaluationUnitStatus.valueOf(evaluationUnitProto.getEvaluationUnitStatus().name()))
                .createdAt(LocalDateTime.ofEpochSecond(evaluationUnitProto.getCreatedAt().getSeconds(),
                        evaluationUnitProto.getCreatedAt().getNanos(), OffsetDateTime.now().getOffset()))
                .scriptName(evaluationUnitProto.getScriptName())
                .scriptVersion(evaluationUnitProto.getScriptVersion())
                .assignedPartitions(evaluationUnitProto.getAssignedPartitionsList());

        if (evaluationUnitProto.hasFinishedAt()) {
            builder.finishedAt(LocalDateTime.ofEpochSecond(evaluationUnitProto.getFinishedAt().getSeconds(),
                    evaluationUnitProto.getFinishedAt().getNanos(), OffsetDateTime.now().getOffset()));
        }
        if (!evaluationUnitProto.getErrorMessage().equals(""))
            builder.errorMessage(evaluationUnitProto.getErrorMessage());

        return builder.build();
    }

}
