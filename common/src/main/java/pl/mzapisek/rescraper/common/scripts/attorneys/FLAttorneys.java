package pl.mzapisek.rescraper.common.scripts.attorneys;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.scripts.ScrapeScriptService;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitUrlParam;
import pl.mzapisek.rescraper.common.shared.domain.UrlParamStatus;
import pl.mzapisek.rescraper.common.shared.repository.ScrapeUnitUrlParamMongoRepository;
import pl.mzapisek.rescraper.common.shared.services.WebDriverService;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Log4j2
@Service
@RequiredArgsConstructor
public class FLAttorneys implements ScrapeScript {

    private static final Integer VERSION = 1;
    private static final String SCRIPT_NAME = "FLAttorneys";
    private static final Integer PAGE_SIZE = 50;
    private static final String REQUEST_URL =
            "https://www.floridabar.org/directories/find-mbr/?lName=%s&pageSize=" + PAGE_SIZE;
    private static final String DETAIL_REQUEST_URL = "https://www.floridabar.org/directories/find-mbr/profile/?num=%s";

    private final FLAttorneysService flAttorneysService;
    private final FLAttorneysScrapeService flAttorneysScrapeService;
    private final WebDriverService webDriverService;
//    private final ScrapeUnitUrlParamMongoRepository scrapeUnitUrlParamMongoRepository;
    private Set<Integer> licenceNumbers = new HashSet<>();

    @Override
    public String getScriptName() {
        return SCRIPT_NAME;
    }

    @Override
    public Integer getVersion() {
        return VERSION;
    }

    @Override
    public String getHeader() {
        return ScrapeScriptService.headerGenerator(List.of(), "");
    }

    @Override
    public String getRow(String json) {
        return null;
    }

    @Override
    public List<EvaluationUnit> process(Job job) {
        List<ScrapeUnit> scrapeUnits = new ArrayList<>();
        Set<String> params = new HashSet<>();

        RemoteWebDriver webDriver = null;

        //        try {
        //            webDriver = webDriverService.create();
        //
        //            webDriver.navigate().to(String.format(REQUEST_URL, evaluationUnit.getElement() + "%"));
        //            Document doc = Jsoup.parse(webDriver.getPageSource());
        //
        //            // duplicated
        //            Elements links = getLinks(doc);
        //            for (Element el : links) {
        //                getLicenseNumber(el.attr("href")).ifPresent(licenseNo -> params.add(licenseNo.toString()));
        //            }
        //
        //            while (hasNextPage(doc)) {
        //                WebElement element = webDriver.findElement(By.className("fa-chevron-circle-right"));
        //                element.click();
        //                doc = Jsoup.parse(webDriver.getPageSource());
        //                // duplicated
        //                final Elements links1 = getLinks(doc);
        //                for (Element el : links1) {
        //                    getLicenseNumber(el.attr("href")).ifPresent(licenseNo -> params.add(licenseNo.toString()));
        //                }
        //            }
        //
        //            // batches
        //            List<List<String>> urlParamsBatches = JobProcessorService.createBatches(5, params);
        //            scrapeUnits.addAll(urlParamsBatches.stream()
        //                    .map(urlParamsBatch -> createScrapeUnit(urlParamsBatch, evaluationUnit))
        //                    .collect(Collectors.toList()));
        //
        //        } catch (Exception e) {
        //            log.error(e.getMessage());
        //            evaluationUnit.setFinishedAt(LocalDateTime.now());
        //            evaluationUnit.setStatus(EvaluationUnitStatus.FAIL.toString());
        //        }
        //
        //        // todo refactor?
        //        if (webDriver != null) {
        //            webDriver.quit();
        //        }
        //
        //        evaluationUnit.setScrapeUnits(scrapeUnits);
        //        evaluationUnit.setStatus(EvaluationUnitStatus.SUCCESS.toString());
        //        evaluationUnit.setFinishedAt(LocalDateTime.now());

        return null;
    }

    @Override
    public List<ScrapeUnit> process(EvaluationUnit evaluationUnit) {
        return null;
    }

    private ScrapeUnit createScrapeUnit(List<String> urlParams, EvaluationUnit evaluationUnit) {
        return ScrapeUnit.builder()
                .urlParams(urlParams)
                .build();
    }

    // process batch

    @Override
    public List<ScrapeResult> process(ScrapeUnit scrapeUnit) {
        // todo validate necessary fields

        RemoteWebDriver webDriver = null;

        try {
            webDriver = webDriverService.create();

            webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            webDriver.navigate().to(String.format(REQUEST_URL, "x%"));

            String parentHandler = webDriver.getWindowHandle();
            String detailHandler;

            for (String licenseNumber : scrapeUnit.getUrlParams()) {
                // todo refactor block()
                Optional<ScrapeUnitUrlParam> scrapeUnitUrlParam =
//                        Optional.ofNullable(scrapeUnitUrlParamMongoRepository
//                                .findByParamAndJobId(licenseNumber, scrapeUnit.getJobId())
//                                .block());
                        Optional.empty();

                String detailUrl = String.format(DETAIL_REQUEST_URL, licenseNumber);

                log.info(detailUrl);

                // we need to open detail information page in a new "tab",
                // otherwise content wont load (cookies? or there is some other verification)
                webDriver.executeScript("window.open('" + detailUrl + "', '_blank')");

                Set<String> windowHandles = webDriver.getWindowHandles();
                detailHandler = getDetailedWindowHandler(windowHandles, parentHandler);
                WebDriver window = webDriver.switchTo().window(detailHandler);

                String detailPageSource = window.getPageSource();
                org.jsoup.nodes.Document detailDoc = Jsoup.parse(detailPageSource);

                try {
                    FLAttorneysLicense licence = flAttorneysScrapeService.getLicence(detailDoc);
                    log.info(licence);
                    // todo update mongo
                    scrapeUnitUrlParam.ifPresent(unit -> {
                        unit.setStatus(UrlParamStatus.PROCESSED);
                        // scrapeUnitUrlParamMongoRepository.save(unit).subscribe();
                    });
                    // save result to mongo
                } catch (Exception e) {
                    log.error(e.getMessage());
                    scrapeUnitUrlParam.ifPresent(unit -> {
                        unit.setStatus(UrlParamStatus.FAILED);
                        //scrapeUnitUrlParamMongoRepository.save(unit).subscribe();
                    });
                } finally {
                    // remove detail window and switch back to main window
                    window.close();
                    windowHandles.remove(detailHandler);
                    webDriver.switchTo().window(parentHandler);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            //            scrapeUnit.getUrlParams().stream()
            //                    .map(s -> scrapeUnitUrlParamMongoRepository.)
        } finally {
            if (webDriver != null) {
                webDriver.quit();
            }
        }

        return List.of();

        // todo improve
        // get statuses of each process
        //        return ScrapeUnitResult.builder()
        //                .status(ScrapeUnitStatus.SUCCESS)
        //                // .createdAt(scrapeUnit.getCreatedAt())
        //                // .finishedAt(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC))
        //                .build();
    }

    @Override
    public Map<String, String> getDefaultParams() {
        return Collections.emptyMap();
    }

    @Override
    public List<String> getDefaultUrlParams() {
        return List.of("aa", "aa");
        //        return scriptParamMongoRepository
        //                .findByScriptNameAndScriptVersion(this.getScriptName(), this.getVersion())
        //                .map(ScriptUrlParam::getParam)
        //                .collectList()
        //                .block();
    }

    private Elements getLinks(Document doc) {
        return doc.select(".profile-name > a");
    }

    private void addLicenseNumbers(Elements hrefLinks) {
        // createScrapeUnit ScrapeUnit
        for (Element el : hrefLinks) {
            getLicenseNumber(el.attr("href")).ifPresent(licenseNo -> licenceNumbers.add(licenseNo));
        }
    }

    private Optional<Integer> getLicenseNumber(String link) {
        String[] split = link.split("/?num=");
        return split.length == 2 ? Optional.of(Integer.valueOf(split[1])) : Optional.empty();
    }

    private boolean hasNextPage(Document doc) {
        Elements searchControls = doc.getElementsByClass("search-results-controls");
        Element pagination = searchControls.first();
        Elements circleRight = pagination.getElementsByClass("fa-chevron-circle-right");
        Element circleR = circleRight.first();
        Element parent = circleR.parent();
        String tagName = parent.tagName();

        return tagName.equals("a");
    }

    private String getDetailedWindowHandler(Set<String> windowHandles, String parentHandler) {
        for (String window : windowHandles) {
            if (!window.equals(parentHandler)) {
                return window;
            }
        }
        return null;
    }

}
