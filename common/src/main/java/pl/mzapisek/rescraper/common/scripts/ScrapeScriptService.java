package pl.mzapisek.rescraper.common.scripts;

import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnitStatus;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.time.LocalDateTime.now;

public class ScrapeScriptService {

    private ScrapeScriptService() {
    }

    public static List<EvaluationUnit> createEvaluationUnits(Job job) {
        return job.getUrlParams()
                .stream()
                .map(urlParam -> EvaluationUnit.builder()
                        .id(UUID.randomUUID().toString())
                        .jobId(job.getId())
                        .status(EvaluationUnitStatus.READY)
                        .createdAt(LocalDateTime.now())
                        .scriptName(job.getScriptName())
                        .scriptVersion(job.getScriptVersion())
                        .element(urlParam)
                        .assignedPartitions(job.getAssignedPartitions())
                        .build())
                .collect(Collectors.toList());
    }

    public static ScrapeResult createScrapeResult(ScrapeUnit scrapeUnit, String jsonValue, String urlParam) {
        return ScrapeResult.builder()
                .id(UUID.randomUUID().toString())
                .jobId(scrapeUnit.getJobId())
                .evaluationId(scrapeUnit.getEvaluationId())
                .scrapeUnitId(scrapeUnit.getId())
                .createdAt(LocalDateTime.now())
                .urlParam(urlParam)
                .jsonValue(jsonValue)
                .build();
    }

    public static List<ScrapeUnit> createScrapeUnits(List<String> params, EvaluationUnit evaluationUnit) {
        return params.stream()
                .map(param -> ScrapeUnit.builder()
                        .id(UUID.randomUUID().toString())
                        .jobId(evaluationUnit.getJobId())
                        .evaluationId(evaluationUnit.getId())
                        .scriptName(evaluationUnit.getScriptName())
                        .scriptVersion(evaluationUnit.getScriptVersion())
                        .createdAt(now())
                        .scrapeUnitStatus(ScrapeUnitStatus.READY)
                        .urlParams(List.of(param))
                        .build())
                .collect(Collectors.toList());
    }

    public static String headerGenerator(List<String> columns, String separator) {
        return String.join(separator, columns);
    }

}
