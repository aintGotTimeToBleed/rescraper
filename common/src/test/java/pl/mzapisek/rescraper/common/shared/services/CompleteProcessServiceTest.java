package pl.mzapisek.rescraper.common.shared.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.mzapisek.rescraper.common.api.CompleteProcessService;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.repository.EvaluationUnitMongoRepository;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.jobprocessor.domain.JobStatus;
import pl.mzapisek.rescraper.common.jobprocessor.repository.JobMongoRepository;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CompleteProcessServiceTest {

    private static final String EVALUATION_ID = "evaluationUnitId";
    private static final String JOB_ID = "jobId";

    @Mock
    private JobMongoRepository jobMongoRepository;
    @Mock
    private EvaluationUnitMongoRepository evaluationUnitMongoRepository;
    @Mock
    private ScrapeUnitMongoRepository scrapeUnitMongoRepository;
    @InjectMocks
    private CompleteProcessService completeProcessService;

    @Test
    void shouldCompleteProcess_whenThereIsNoScrapeUnitsAndEvaluationUnitsInProgress() {
        var evaluationUnit = EvaluationUnit.builder().id(EVALUATION_ID).jobId(JOB_ID).build();
        var job = Job.builder().id(JOB_ID).build();

        when(scrapeUnitMongoRepository.existsByEvaluationIdAndScrapeUnitStatusIn(any(), any()))
                .thenReturn(Mono.just(false));
        when(evaluationUnitMongoRepository.findById(EVALUATION_ID))
                .thenReturn(Mono.just(evaluationUnit));
        when(evaluationUnitMongoRepository.save(any()))
                .thenReturn(Mono.just(evaluationUnit));
        when(evaluationUnitMongoRepository.existsByJobIdAndStatusIn(any(), any()))
                .thenReturn(Mono.just(false));
        when(jobMongoRepository.findById(JOB_ID))
                .thenReturn(Mono.just(job));
        when(jobMongoRepository.save(any()))
                .thenReturn(Mono.just(job.toBuilder().status(JobStatus.SUCCESS).build()));

        StepVerifier.create(completeProcessService.completeProcess(EVALUATION_ID, JOB_ID))
                .expectSubscription()
                .expectNext(true)
                .verifyComplete();
    }

    @Test
    void shouldNotCompleteProcess_whenThereAreScrapeUnitsInProgress() {
        var evaluationUnitId = "evaluationUnitId";
        var jobId = "jobId";

        when(scrapeUnitMongoRepository.existsByEvaluationIdAndScrapeUnitStatusIn(any(), any()))
                .thenReturn(Mono.just(true));

        StepVerifier.create(completeProcessService.completeProcess(evaluationUnitId, jobId))
                .expectSubscription()
                .expectNext(false)
                .verifyComplete();
    }

    @Test
    void shouldNotCompleteProcess_whenThereAreEvaluationUnitsInProgress() {
        var evaluationUnitId = "evaluationUnitId";
        var jobId = "jobId";
        var evaluationUnit = EvaluationUnit.builder().id(EVALUATION_ID).jobId(JOB_ID).build();

        when(scrapeUnitMongoRepository.existsByEvaluationIdAndScrapeUnitStatusIn(any(), any()))
                .thenReturn(Mono.just(false));
        when(evaluationUnitMongoRepository.findById(EVALUATION_ID))
                .thenReturn(Mono.just(evaluationUnit));
        when(evaluationUnitMongoRepository.save(any()))
                .thenReturn(Mono.just(evaluationUnit));
        when(evaluationUnitMongoRepository.existsByJobIdAndStatusIn(any(), any()))
                .thenReturn(Mono.just(true));

        StepVerifier.create(completeProcessService.completeProcess(evaluationUnitId, jobId))
                .expectSubscription()
                .expectNext(false)
                .verifyComplete();
    }

}