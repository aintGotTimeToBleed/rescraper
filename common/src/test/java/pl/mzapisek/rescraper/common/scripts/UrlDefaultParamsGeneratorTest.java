package pl.mzapisek.rescraper.common.scripts;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class UrlDefaultParamsGeneratorTest {

    @Test
    void singleLetter_shouldGenerateWholeEnglishAlphabet() {
        assertThat(UrlDefaultParamsGenerator.singleLetter()).hasSize(26);
    }

    @Test
    void singleLetter_shouldGenerateValidLetters() {
        List<String> letters = UrlDefaultParamsGenerator.singleLetter();
        assertThat(letters.get(0)).isEqualTo("a");
        assertThat(letters.get(25)).isEqualTo("z");
    }

    @Test
    void doubleLetter_shouldGenerateDoubledEnglishAlphabet() {
        assertThat(UrlDefaultParamsGenerator.doubleLetter()).hasSize(676);
    }

    @Test
    void doubleLetter_shouldGenerateValidLetters() {
        List<String> letters = UrlDefaultParamsGenerator.doubleLetter();
        assertThat(letters.get(0)).isEqualTo("aa");
        assertThat(letters.get(675)).isEqualTo("zz");
    }

    @Test
    void tripleLetter_shouldGenerateDoubledEnglishAlphabet() {
        assertThat(UrlDefaultParamsGenerator.tripleLetter()).hasSize(17576);
    }

    @Test
    void tripleLetter_shouldGenerateValidLetters() {
        List<String> letters = UrlDefaultParamsGenerator.tripleLetter();
        assertThat(letters.get(0)).isEqualTo("aaa");
        assertThat(letters.get(17575)).isEqualTo("zzz");
    }

}