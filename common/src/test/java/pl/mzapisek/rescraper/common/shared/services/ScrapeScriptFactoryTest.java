package pl.mzapisek.rescraper.common.shared.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.shared.exceptions.ScrapeScriptNotFound;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ScrapeScriptFactoryTest {

    @Mock
    ScrapeScript scrapeScript;

    @Test
    void shouldThrowScrapeScriptNotFound_whenNoScriptWasFound() {
        var scrapeScriptName = "reactor";
        var scrapeScriptVersion = 1;

        when(scrapeScript.getScriptName()).thenReturn(scrapeScriptName);
        when(scrapeScript.getVersion()).thenReturn(scrapeScriptVersion);

        ScrapeScriptFactory scrapeScriptFactory = new ScrapeScriptFactory(List.of(scrapeScript));
        assertThatThrownBy(() -> scrapeScriptFactory.create(scrapeScriptName, 77))
                .isInstanceOf(ScrapeScriptNotFound.class)
                .hasMessageContaining("Scrape script not found: " + scrapeScriptName);
    }

    @Test
    void shouldCreateValidScrapeScript() {
        var scrapeScriptName = "reactor";
        var scrapeScriptVersion = 1;

        when(scrapeScript.getScriptName()).thenReturn(scrapeScriptName);
        when(scrapeScript.getVersion()).thenReturn(scrapeScriptVersion);

        ScrapeScriptFactory scrapeScriptFactory = new ScrapeScriptFactory(List.of(scrapeScript));
        assertThatThrownBy(() -> scrapeScriptFactory.create(scrapeScriptName, 77))
                .isInstanceOf(ScrapeScriptNotFound.class)
                .hasMessageContaining("Scrape script not found: " + scrapeScriptName);
    }

}