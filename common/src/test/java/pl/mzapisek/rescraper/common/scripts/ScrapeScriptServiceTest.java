package pl.mzapisek.rescraper.common.scripts;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class ScrapeScriptServiceTest {

    @ParameterizedTest
    @MethodSource("input")
    void shouldGenerateValidHeader(List<String> columns, String separator, String resultHeader) {
        assertThat(ScrapeScriptService.headerGenerator(columns, separator)).isEqualTo(resultHeader);
    }

    private static Stream<Arguments> input() {
        return Stream.of(
                Arguments.of(List.of("one", "two"), "|", "one|two"),
                Arguments.of(List.of("one"), ";", "one"));
    }

}