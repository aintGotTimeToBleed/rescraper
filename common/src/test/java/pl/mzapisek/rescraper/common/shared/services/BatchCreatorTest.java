package pl.mzapisek.rescraper.common.shared.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class BatchCreatorTest {

    private BatchCreator<ScrapeUnit> scrapeUnitBatchCreator;

    @BeforeEach
    void init() {
        scrapeUnitBatchCreator = new BatchCreator<>();
    }

    @Test
    void shouldCreateThreeSameSizeBatches_whenSizeCanBeEquallyDivided() {
        List<List<ScrapeUnit>> batches = scrapeUnitBatchCreator.createBatch(createScrapeUnits(), 3);

        assertThat(batches.size()).isEqualTo(3);
        assertThat(batches.get(0).size()).isEqualTo(3);
        assertThat(batches.get(1).size()).isEqualTo(3);
        assertThat(batches.get(2).size()).isEqualTo(3);
    }

    @Test
    void shouldCreateTwoBatches_restOfDivisionShouldGoToFirstBatch() {
        List<List<ScrapeUnit>> batches = scrapeUnitBatchCreator.createBatch(createScrapeUnits(), 2);

        assertThat(batches.size()).isEqualTo(2);
        assertThat(batches.get(0).size()).isEqualTo(5);
        assertThat(batches.get(1).size()).isEqualTo(4);
    }

    @Test
    void shouldCreateOneBatch() {
        List<List<ScrapeUnit>> batches = scrapeUnitBatchCreator.createBatch(createScrapeUnits(), 1);

        assertThat(batches.size()).isEqualTo(1);
        assertThat(batches.get(0).size()).isEqualTo(9);
    }

    private List<ScrapeUnit> createScrapeUnits() {
        return List.of(
                ScrapeUnit.builder().id("one").build(),
                ScrapeUnit.builder().id("two").build(),
                ScrapeUnit.builder().id("three").build(),
                ScrapeUnit.builder().id("four").build(),
                ScrapeUnit.builder().id("five").build(),
                ScrapeUnit.builder().id("six").build(),
                ScrapeUnit.builder().id("seven").build(),
                ScrapeUnit.builder().id("eight").build(),
                ScrapeUnit.builder().id("nine").build());
    }

}