package pl.mzapisek.rescraper.scrapeprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.mzapisek.rescraper.reactor_kafka.scrapeprocessor.EnableReactorKafkaScrapeProcessor;
import reactor.blockhound.BlockHound;

@SpringBootApplication
//@EnableSpringKafkaScrapeProcessor // for spring kafka
@EnableReactorKafkaScrapeProcessor // for reactor kafka
public class ScrapeProcessorApplication {

    public static void main(String[] args) {
        BlockHound.install();
        SpringApplication.run(ScrapeProcessorApplication.class, args);
    }

}
