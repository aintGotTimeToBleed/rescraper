package pl.mzapisek.rescraper.reactiveapi.task;

import lombok.RequiredArgsConstructor;
import net.javacrumbs.shedlock.core.LockConfiguration;
import net.javacrumbs.shedlock.core.LockingTaskExecutor;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.api.Task;
import pl.mzapisek.rescraper.common.api.TaskMongoRepository;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.reactiveapi.job.JobDto;
import pl.mzapisek.rescraper.reactiveapi.job.JobService;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

@Service
@RequiredArgsConstructor
public class TaskSchedulerService {

	private final TaskScheduler taskScheduler;
	private final JobService jobService;
	private final TaskMongoRepository taskMongoRepository;
	private final LockingTaskExecutor lockingTaskExecutor;

	private final Map<String, ScheduledFuture<?>> taskIdToScheduledFuture = new HashMap<>();

	Mono<Task> scheduleTask(Task task) {
		// todo check whether task is in db
		return Mono.fromSupplier(() -> taskScheduler.schedule(
						() -> createJob(task.getScriptName(), task.getScriptVersion()),
						new CronTrigger(task.getCronExpression())))
				.doOnNext(scheduledFuture -> taskIdToScheduledFuture.put(task.getId(), scheduledFuture))
				.map(scheduledFuture -> task);
	}

	Mono<String> removeScheduledTask(String taskId) {
		return Mono.fromSupplier(() -> taskIdToScheduledFuture.remove(taskId))
				.map(scheduledFuture -> taskId);
	}

	@EventListener({ContextRefreshedEvent.class})
	public void refreshScheduledTasks() {
		taskMongoRepository.findAll()
				.map(task -> taskScheduler.schedule(
						() -> createJob(task.getScriptName(), task.getScriptVersion()),
						new CronTrigger(task.getCronExpression())))
				.subscribe();
	}

	// todo refactor
	private LockingTaskExecutor.TaskResult<Job> createJob(String scriptName, int scriptVersion) {
		LockConfiguration lockConfiguration =
				new LockConfiguration(Instant.now(), scriptName + ":v" + scriptVersion, Duration.ofMinutes(15), Duration.ofMinutes(5));

		try {
			return this.lockingTaskExecutor.executeWithLock(
					() -> jobService.create(JobDto.builder()
									.scriptName(scriptName)
									.scriptVersion(scriptVersion)
									.build())
							.block(),
					lockConfiguration
			);
		} catch (Throwable e) {
			// todo create custom
			throw new RuntimeException(e);
		}
	}

}
