package pl.mzapisek.rescraper.reactiveapi.evaluationunit;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnitStatus;
import pl.mzapisek.rescraper.common.evaluationprocessor.repository.EvaluationUnitMongoRepository;
import reactor.core.publisher.Flux;

@Log4j2
@Service
@RequiredArgsConstructor
public class EvaluationUnitService {

    private final EvaluationUnitMongoRepository evaluationUnitMongoRepository;

    Flux<EvaluationUnit> findAll() {
        return evaluationUnitMongoRepository.findAll();
    }

    Flux<EvaluationUnit> findEvaluationUnitsByStatusAndJobId(EvaluationUnitStatus evaluationUnitStatus, String jobId) {
        return evaluationUnitMongoRepository.findByStatusAndJobId(evaluationUnitStatus, jobId);
    }

}
