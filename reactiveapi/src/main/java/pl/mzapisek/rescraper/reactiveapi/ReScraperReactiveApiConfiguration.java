package pl.mzapisek.rescraper.reactiveapi;

import net.javacrumbs.shedlock.core.DefaultLockingTaskExecutor;
import net.javacrumbs.shedlock.core.LockingTaskExecutor;
import net.javacrumbs.shedlock.provider.redis.jedis.JedisLockProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import pl.mzapisek.rescraper.common.api.CompleteProcessService;
import pl.mzapisek.rescraper.common.api.TaskMongoRepository;
import pl.mzapisek.rescraper.common.configuration.MongoConfiguration;
import pl.mzapisek.rescraper.common.configuration.RescraperConfiguration;
import pl.mzapisek.rescraper.common.evaluationprocessor.repository.EvaluationUnitMongoRepository;
import pl.mzapisek.rescraper.common.jobprocessor.repository.JobMongoRepository;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.services.JsonUtils;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.reactor_kafka.KafkaUtils;
import pl.mzapisek.rescraper.reactor_kafka.configs.ReactorKafkaProducerBaseConfiguration;
import pl.mzapisek.rescraper.reactor_kafka.jobprocessor.JobMessageProducer;
import reactor.kafka.sender.KafkaSender;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
@ComponentScan(
  basePackages = {"pl.mzapisek.rescraper.reactiveapi"},
  basePackageClasses = {
    JobMongoRepository.class,
    EvaluationUnitMongoRepository.class,
    ScrapeUnitMongoRepository.class,
    TaskMongoRepository.class,
    KafkaUtils.class,
    ScrapeScriptFactory.class,
    CompleteProcessService.class
    //,
    //EventBusProducer.class // todo is that necessary?, its a @Service, be sure no other projects need this
    // bug currently it looks like producer is opened twice
    // javax.management.InstanceAlreadyExistsException: kafka.producer:type=app-info,id=rescraper-job-processor
  })
@Import({RescraperConfiguration.class, ReactorKafkaProducerBaseConfiguration.class, MongoConfiguration.class})
public class ReScraperReactiveApiConfiguration {

  @Value("${redis.host}")
  private String redisHost;
  @Value("${redis.port}")
  private int redisPort;

  @Bean
  public JobMessageProducer jobMessageProducer(KafkaSender<String, String> kafkaSender,
                                               KafkaBaseProperties kafkaBaseProperties, JsonUtils jsonUtils) {
    return new JobMessageProducer(kafkaSender, kafkaBaseProperties, jsonUtils);
  }

  @Bean
  public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
    ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
    threadPoolTaskScheduler.setPoolSize(5);
    threadPoolTaskScheduler.setThreadNamePrefix("ThreadPoolTaskScheduler");
    return threadPoolTaskScheduler;
  }

  // todo, pass host and port
  @Bean
  public JedisLockProvider jedisLockProvider() {
    return new JedisLockProvider(new JedisPool(new JedisPoolConfig(), "localhost"));
  }

  @Bean
  public LockingTaskExecutor defaultLockingTaskExecutor(JedisLockProvider jedisLockProvider) {
    return new DefaultLockingTaskExecutor(jedisLockProvider);
  }

}
