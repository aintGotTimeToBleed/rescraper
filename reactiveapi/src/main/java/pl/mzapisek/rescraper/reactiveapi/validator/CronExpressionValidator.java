package pl.mzapisek.rescraper.reactiveapi.validator;

import org.springframework.scheduling.support.CronExpression;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CronExpressionValidator implements ConstraintValidator<ValidCronExpression, String> {

    @Override
    public boolean isValid(String cronExpression, ConstraintValidatorContext context) {
        return ConstraintValidatorHelper.validateField(() -> CronExpression.parse(cronExpression), "cronExpression", context);
    }

}
