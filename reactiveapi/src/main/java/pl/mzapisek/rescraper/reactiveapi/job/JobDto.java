package pl.mzapisek.rescraper.reactiveapi.job;

import lombok.*;
import pl.mzapisek.rescraper.reactiveapi.validator.ValidScriptName;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@ToString
public class JobDto {

	// @ValidUuid(message = "invalid id, must be a valid uuid")
	private String id;

	private Map<String, String> params;

	private List<String> urlParams;

	@NotNull(message = "invalid scriptName: must not be null")
	@ValidScriptName
	private String scriptName;

	@Min(value = 1, message = "invalid scriptVersion: must be greater than 0")
	private Integer scriptVersion;

	private List<Integer> assignedPartitions;

}
