package pl.mzapisek.rescraper.reactiveapi.task;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.api.Task;
import pl.mzapisek.rescraper.common.api.TaskMongoRepository;
import pl.mzapisek.rescraper.common.protobuf.TaskProtos;
import pl.mzapisek.rescraper.common.protobuf.TaskRemovedProtos;
import pl.mzapisek.rescraper.common.shared.mappers.TaskMapper;
import pl.mzapisek.rescraper.reactiveapi.task.exception.TaskAlreadyExists;
import pl.mzapisek.rescraper.reactor_kafka.eventbus.EventBusProducer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Log4j2
@Service
@RequiredArgsConstructor
public class TaskService {

	private final EventBusProducer eventBusProducer;
	private final TaskMongoRepository taskMongoRepository;

	Flux<Task> findAllTasks() {
		return taskMongoRepository.findAll();
	}

	Mono<Task> findById(String taskId) {
		return taskMongoRepository.findById(taskId);
	}

	Mono<Void> deleteById(String taskId) {
		var taskRemovedProto = TaskRemovedProtos.TaskRemoved.newBuilder().setId(taskId).build();
		return taskMongoRepository.findById(taskId)
				.switchIfEmpty(Mono.error(new RuntimeException("task of id not found")))
				.flatMap(task -> eventBusProducer.sendTaskRemovedEvent(taskRemovedProto, task.getTaskName()).next())
				.then();
	}

	Mono<Task> createTask(TaskDto taskDto) {
		var taskProto = mapTaskDtoToTaskProto(taskDto);
		return taskMongoRepository
				.existsByScriptName(taskDto.getScriptName())
				.flatMap(taskExists -> {
					if (taskExists) {
						return Mono.error(new TaskAlreadyExists("Task for given script name already scheduled"));
					} else {
						// task created event
						return eventBusProducer.sendTask(taskProto)
								.collectList()
								.map(senderResults -> TaskMapper.mapTaskProtoToTask(taskProto));
					}
				});
	}

	private TaskProtos.Task mapTaskDtoToTaskProto(TaskDto taskDto) {
		var builder = TaskProtos.Task.newBuilder()
				.setId(UUID.randomUUID().toString())
				.setTaskName(taskDto.getTaskName())
				.setCronExpression(taskDto.getCronExpression())
				.setScriptName(taskDto.getScriptName())
				.setScriptVersion(taskDto.getScriptVersion());

		if (taskDto.getDescription() != null)
			builder.setDescription(taskDto.getDescription());

		return builder.build();
	}

}
