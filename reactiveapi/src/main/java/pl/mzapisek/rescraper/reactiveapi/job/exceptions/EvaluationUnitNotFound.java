package pl.mzapisek.rescraper.reactiveapi.job.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

// todo
// @ResponseStatus
public class EvaluationUnitNotFound extends RuntimeException {

    public EvaluationUnitNotFound(String message) {
        super(message);
    }

}
