package pl.mzapisek.rescraper.reactiveapi.task;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.mzapisek.rescraper.common.api.Task;
import pl.mzapisek.rescraper.reactiveapi.task.exception.TaskNotFound;
import pl.mzapisek.rescraper.reactiveapi.validator.ValidUuid;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Validated
@RequestMapping("/tasks")
@RestController
@RequiredArgsConstructor
public class TaskController {

	private static final String INVALID_TASK_ID_MSG = "invalid taskId, only UUID format accepted";

	private final TaskService taskService;
	private final TaskSchedulerService taskSchedulerService;

	@GetMapping
	public Flux<Task> findTasks() {
		return taskService.findAllTasks();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Mono<Task> create(@Valid @RequestBody TaskDto taskDto) {
		return taskService.createTask(taskDto);
	}

	@GetMapping("{taskId}")
	public Mono<Task> getById(@ValidUuid(message = INVALID_TASK_ID_MSG) @PathVariable String taskId) {
		return taskService.findById(taskId)
				.switchIfEmpty(Mono.error(new TaskNotFound(String.format("Task of id: %s not found", taskId))));
	}

	@PutMapping("{taskId}")
	public Mono<Task> update(@ValidUuid(message = INVALID_TASK_ID_MSG) @PathVariable String taskId) {
		return Mono.error(new RuntimeException("not yet implemented"));
	}

	@DeleteMapping("{taskId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public Mono<Void> deleteById(@ValidUuid(message = INVALID_TASK_ID_MSG) @PathVariable String taskId) {
		return taskService.deleteById(taskId);
	}

	// todo CORS localhost only, only event bus consumer should be able to call this endpoint
	@PostMapping("/schedule")
	public Mono<Task> schedule(@RequestBody Task task) {
		return taskSchedulerService.scheduleTask(task);
	}

	// todo CORS localhost only, only event bus consumer should be able to call this endpoint
	// @CrossOrigin
	@DeleteMapping("/schedule/{taskId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public Mono<Void> removeSchedule(@ValidUuid(message = INVALID_TASK_ID_MSG) @PathVariable String taskId) {
		return taskSchedulerService.removeScheduledTask(taskId).then();
	}

}
