package pl.mzapisek.rescraper.reactiveapi.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import javax.validation.ConstraintValidatorContext;
import java.util.function.Supplier;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class ConstraintValidatorHelper {

    static boolean validateField(Supplier<?> supplier, String fieldName, ConstraintValidatorContext context) {
        try {
            supplier.get();
            return true;
        } catch (Exception ex) {
            if ("".equals(context.getDefaultConstraintMessageTemplate())) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(String.format("invalid %s: %s", fieldName, ex.getMessage()))
                        .addConstraintViolation();
            }
            return false;
        }
    }

}
