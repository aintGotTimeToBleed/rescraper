package pl.mzapisek.rescraper.reactiveapi.job;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.shared.domain.JobScrapingStatus;
import pl.mzapisek.rescraper.reactiveapi.validator.ValidUuid;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/jobs")
public class JobController {

	private static final String BASE_JOB_ENTITY_LINK = "/jobs/";
	private static final String INVALID_JOB_ID_MSG = "invalid jobId, only UUID format accepted";

	private final JobService jobService;

	@GetMapping
	public Flux<EntityModel<Job>> findJobs() {
		return jobService.findAll().map(this::createJobEntityModel);
	}

	@PostMapping
	public Mono<EntityModel<Job>> createJob(@Valid @RequestBody JobDto jobDto) {
		return jobService.create(jobDto).map(this::createJobEntityModel);
	}

	@GetMapping("{jobId}")
	public Mono<EntityModel<Job>> getJob(@ValidUuid(message = INVALID_JOB_ID_MSG) @PathVariable String jobId) {
		return jobService.find(jobId).map(this::createJobEntityModel);
	}

	@GetMapping("{jobId}/status")
	public Mono<EntityModel<JobScrapingStatus>> checkStatus(@ValidUuid(message = INVALID_JOB_ID_MSG) @PathVariable String jobId) {
		return jobService.checkStatus(jobId).map(this::createJobScrapingStatusEntityModel);
	}

	@Operation(summary = "cancel job")
	@PutMapping("{jobId}/cancel")
	public Mono<EntityModel<Job>> cancelJob(@ValidUuid(message = INVALID_JOB_ID_MSG) @PathVariable String jobId) {
		return jobService.cancel(jobId).map(this::createJobEntityModel);
	}

	@Operation(summary = "rerun failed units")
	@PutMapping("{jobId}/rerun")
	public Mono<EntityModel<Job>> rerunJob(@ValidUuid(message = INVALID_JOB_ID_MSG) @PathVariable String jobId) {
		return jobService.rerun(jobId).map(this::createJobEntityModel);
	}

	@Operation(summary = "try to complete job")
	@PutMapping("{jobId}/complete")
	public Mono<EntityModel<Job>> completeJob(@ValidUuid(message = INVALID_JOB_ID_MSG) @PathVariable String jobId) {
		return jobService.complete(jobId).map(this::createJobEntityModel);
	}

	private EntityModel<Job> createJobEntityModel(Job job) {
		return EntityModel.of(job,
				Link.of(BASE_JOB_ENTITY_LINK + job.getId()).withSelfRel(),
				Link.of(BASE_JOB_ENTITY_LINK + job.getId() + "/status").withTitle("check job status"),
				Link.of(BASE_JOB_ENTITY_LINK + job.getId() + "/cancel").withTitle("cancel job"),
				Link.of(BASE_JOB_ENTITY_LINK + job.getId() + "/rerun").withTitle("rerun failed units"),
				Link.of(BASE_JOB_ENTITY_LINK + job.getId() + "/complete").withTitle("verify job completeness"),
				Link.of("/results/csv/" + job.getId()).withRel("results").withTitle("generate results as a csv file"));
	}

	private EntityModel<JobScrapingStatus> createJobScrapingStatusEntityModel(JobScrapingStatus jobScrapingStatus) {
		return EntityModel.of(jobScrapingStatus,
				Link.of(BASE_JOB_ENTITY_LINK + jobScrapingStatus.getJob().getId() + "/status").withSelfRel(),
				Link.of(BASE_JOB_ENTITY_LINK + jobScrapingStatus.getJob().getId()).withTitle("job entity"));
	}

}
