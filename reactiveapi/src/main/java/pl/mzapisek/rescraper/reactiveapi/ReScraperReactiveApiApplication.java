package pl.mzapisek.rescraper.reactiveapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.blockhound.BlockHound;

@SpringBootApplication
public class ReScraperReactiveApiApplication {

    public static void main(String[] args) {
        // verify how its look like in prometheus/grafana
        // Schedulers.enableMetrics();
        // todo disable later
        BlockHound.install();
        SpringApplication.run(ReScraperReactiveApiApplication.class, args);
    }

}
