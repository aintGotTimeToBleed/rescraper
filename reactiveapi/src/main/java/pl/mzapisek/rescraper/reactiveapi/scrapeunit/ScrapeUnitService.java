package pl.mzapisek.rescraper.reactiveapi.scrapeunit;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;
import reactor.core.publisher.Flux;

@Service
@RequiredArgsConstructor
public class ScrapeUnitService {

    private final ScrapeUnitMongoRepository scrapeUnitMongoRepository;

    Flux<ScrapeUnit> findAll() {
        return scrapeUnitMongoRepository.findAll();
    }

    Flux<ScrapeUnit> findByStatusAndJobId(ScrapeUnitStatus scrapeUnitStatus, String jobId) {
        return scrapeUnitMongoRepository.findByScrapeUnitStatusAndJobId(scrapeUnitStatus, jobId);
    }

}
