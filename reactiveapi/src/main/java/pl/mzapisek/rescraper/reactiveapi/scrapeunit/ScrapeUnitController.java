package pl.mzapisek.rescraper.reactiveapi.scrapeunit;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;
import reactor.core.publisher.Flux;

import java.util.Map;

@RestController
@RequestMapping("/scrapeunits")
@RequiredArgsConstructor
public class ScrapeUnitController {

    private static final String SCRAPE_STATUS = "scrape_status";
    private static final String JOB_ID = "job_id";

    private final ScrapeUnitService scrapeUnitService;

    @GetMapping
    public Flux<ScrapeUnit> getScrapeUnits(@RequestParam Map<String, String> params) {
        if (params.containsKey(SCRAPE_STATUS) && params.containsKey(JOB_ID)) {
            return scrapeUnitService
                    .findByStatusAndJobId(ScrapeUnitStatus.valueOf(params.get(SCRAPE_STATUS)), params.get(JOB_ID));
        }
        return scrapeUnitService.findAll();
    }

}
