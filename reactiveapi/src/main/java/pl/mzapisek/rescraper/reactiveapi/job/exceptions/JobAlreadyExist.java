package pl.mzapisek.rescraper.reactiveapi.job.exceptions;

public class JobAlreadyExist extends RuntimeException {

    private static final String MESSAGE = "job of given id: %s already exist";

    public JobAlreadyExist(String jobId) {
        super(String.format(MESSAGE, jobId));
    }

}
