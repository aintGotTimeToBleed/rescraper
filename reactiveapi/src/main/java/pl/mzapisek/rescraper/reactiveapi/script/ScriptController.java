package pl.mzapisek.rescraper.reactiveapi.script;

import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.reactiveapi.script.testrun.TestRunResult;
import pl.mzapisek.rescraper.reactiveapi.script.testrun.TestRunService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/scripts")
public class ScriptController {

    private static final String BASE_SCRIPT_ENTITY_LINK = "/scripts/";
    private static final String BASE_TEST_RUN_ENTITY_LINK = "/testrun";

    private final List<ScrapeScript> scrapeScripts;
    private final ScrapeScriptFactory scrapeScriptFactory;
    private final TestRunService testRunService;

    @GetMapping
    public Flux<EntityModel<ScrapeScript>> findAll() {
        return Flux
                .fromIterable(scrapeScripts)
                .map(this::createScriptEntityModel);
    }

    @GetMapping("{scriptName}")
    public Mono<EntityModel<ScrapeScript>> findByName(@PathVariable String scriptName) {
        return Mono
                .just(scrapeScriptFactory.create(scriptName, 1))
                .map(this::createScriptEntityModel);
    }

    // todo publishOn?
    @GetMapping("{scriptName}/testrun")
    public Mono<TestRunResult> testSingleScript(@PathVariable String scriptName) {
        return Mono.fromCallable(() -> testRunService.testSingleScript(testRunService.createTestJob(scriptName)));
    }

    // todo publishOn?
    @GetMapping("{scriptName}/testrun/job")
    public Mono<TestRunResult> testJobForSingleScript(@PathVariable String scriptName) {
        return Mono.fromCallable(() -> testRunService.testJobForSingleScript(testRunService.createTestJob(scriptName)));
    }

    // todo publishOn?
    @GetMapping("{scriptName}/testrun/evaluation")
    public Mono<TestRunResult> testEvaluationForSingleScript(@PathVariable String scriptName) {
        return Mono.fromCallable(() -> testRunService.testEvaluationUnitForSingleScript(testRunService.createEvaluationUnit(scriptName)));
    }

    // todo publishOn?
    @GetMapping("{scriptName}/testrun/scrape")
    public Mono<TestRunResult> testScrapeForSingleScript(@PathVariable String scriptName) {
        return Mono.fromCallable(() -> testRunService.testScrapeUnitForSingleScript(testRunService.createScrapeUnit(scriptName)));
    }

    private EntityModel<ScrapeScript> createScriptEntityModel(ScrapeScript scrapeScript) {
        return EntityModel.of(scrapeScript,
                Link.of(BASE_SCRIPT_ENTITY_LINK + scrapeScript.getScriptName()).withSelfRel(),
                Link.of(BASE_SCRIPT_ENTITY_LINK + scrapeScript.getScriptName() + BASE_TEST_RUN_ENTITY_LINK).withTitle("full test run"),
                Link.of(BASE_SCRIPT_ENTITY_LINK + scrapeScript.getScriptName() + BASE_TEST_RUN_ENTITY_LINK + "/job").withTitle("job test run only"),
                Link.of(BASE_SCRIPT_ENTITY_LINK + scrapeScript.getScriptName() + BASE_TEST_RUN_ENTITY_LINK + "/evaluation").withTitle("evaluation unit test run only"),
                Link.of(BASE_SCRIPT_ENTITY_LINK + scrapeScript.getScriptName() + BASE_TEST_RUN_ENTITY_LINK + "/scrape").withTitle("scrape unit test run only"));
    }

}
