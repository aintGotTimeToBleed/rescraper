package pl.mzapisek.rescraper.reactiveapi.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

public class UuidValidator implements ConstraintValidator<ValidUuid, String> {

    @Override
    public boolean isValid(String uuid, ConstraintValidatorContext context) {
        return ConstraintValidatorHelper.validateField(() -> UUID.fromString(uuid), "UUID", context);
    }

}
