package pl.mzapisek.rescraper.reactiveapi.task;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.mzapisek.rescraper.reactiveapi.validator.ValidCronExpression;
import pl.mzapisek.rescraper.reactiveapi.validator.ValidScriptName;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
public class TaskDto {

	@NotNull(message = "invalid taskName: must not be null")
	@Pattern(regexp = "^[a-zA-Z0-9-._]{3,36}", message = "invalid taskName: must be [a-zA-Z0-9-._]{3,36}")
	private String taskName;

	@NotNull(message = "invalid cronExpression: must not be null")
	@ValidCronExpression
	private String cronExpression;

	@NotNull(message = "invalid scriptName: must not be null")
	@ValidScriptName
	private String scriptName;

	@Min(value = 1, message = "invalid scriptVersion: must be greater than 0")
	private int scriptVersion;

	@Pattern(regexp = "^[a-zA-Z0-9-._ ]{0,128}", message = "invalid description: must be [a-zA-Z0-9-._]{0,128}")
	private String description;

}
