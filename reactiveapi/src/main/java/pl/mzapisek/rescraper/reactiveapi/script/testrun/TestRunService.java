package pl.mzapisek.rescraper.reactiveapi.script.testrun;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;

import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
@RequiredArgsConstructor
public class TestRunService {

    private static final int DEFAULT_SCRIPT_VERSION = 1;

    private final ScrapeScriptFactory scrapeScriptFactory;

    public TestRunResult testSingleScript(Job job) {
        ScrapeScript scrapeScript = scrapeScriptFactory.create(job.getScriptName(), job.getScriptVersion());
        List<EvaluationUnit> evaluationUnits = scrapeScript.process(job);
        EvaluationUnit evaluationUnit = null;
        List<ScrapeUnit> scrapeUnits = new ArrayList<>();
        ScrapeUnit scrapeUnit = null;
        List<ScrapeResult> scrapeResults = new ArrayList<>();

        if (isNotEmpty(evaluationUnits)) {
            evaluationUnit = evaluationUnits.get(0);
            scrapeUnits.addAll(scrapeScript.process(evaluationUnit));

            if (isNotEmpty(scrapeUnits)) {
                scrapeUnit = scrapeUnits.get(0);
                scrapeResults.addAll(scrapeScript.process(scrapeUnit));
            }
        }

        return TestRunResult.builder()
                .scriptName(job.getScriptName())
                .scriptVersion(job.getScriptVersion())
                .evaluationUnits(evaluationUnits)
                .evaluationUnit(evaluationUnit)
                .scrapeUnits(scrapeUnits)
                .scrapeUnit(scrapeUnit)
                .scrapeResults(scrapeResults)
                .build();
    }

    public TestRunResult testJobForSingleScript(Job job) {
        return TestRunResult.builder()
                .scriptName(job.getScriptName())
                .scriptVersion(job.getScriptVersion())
                .evaluationUnits(scrapeScriptFactory
                        .create(job.getScriptName(), job.getScriptVersion())
                        .process(job))
                .build();
    }

    public TestRunResult testEvaluationUnitForSingleScript(EvaluationUnit evaluationUnit) {
        return TestRunResult.builder()
                .scriptName(evaluationUnit.getScriptName())
                .scriptVersion(evaluationUnit.getScriptVersion())
                .scrapeUnits(scrapeScriptFactory
                        .create(evaluationUnit.getScriptName(), evaluationUnit.getScriptVersion())
                        .process(evaluationUnit))
                .build();
    }

    public TestRunResult testScrapeUnitForSingleScript(ScrapeUnit scrapeUnit) {
        return TestRunResult.builder()
                .scriptName(scrapeUnit.getScriptName())
                .scriptVersion(scrapeUnit.getScriptVersion())
                .scrapeResults(scrapeScriptFactory
                        .create(scrapeUnit.getScriptName(), scrapeUnit.getScriptVersion())
                        .process(scrapeUnit))
                .build();
    }

    public Job createTestJob(String scriptName) {
        // if i am sticking to version then newest one should be chosen
        // if not there version should be removed completelly
        var scrapeScript = scrapeScriptFactory.create(scriptName, DEFAULT_SCRIPT_VERSION);
        return Job.builder()
                .id("test_run")
                .urlParams(scrapeScript.getDefaultUrlParams())
                .params(scrapeScript.getDefaultParams())
                .scriptName(scriptName)
                .scriptVersion(DEFAULT_SCRIPT_VERSION)
                .build();
    }

    public EvaluationUnit createEvaluationUnit(String scriptName) {
        var scrapeScript = scrapeScriptFactory.create(scriptName, DEFAULT_SCRIPT_VERSION);
        return EvaluationUnit.builder()
                .id("test_evaluation")
                .element(scrapeScript.getDefaultUrlParams()
                        .stream()
                        .findFirst()
                        .orElse("ab"))
                .scriptName(scrapeScript.getScriptName())
                .scriptVersion(scrapeScript.getVersion())
                .build();
    }

    public ScrapeUnit createScrapeUnit(String scriptName) {
        return testEvaluationUnitForSingleScript(createEvaluationUnit(scriptName))
                .getScrapeUnits()
                .stream()
                .findFirst()
                .orElseThrow(() -> new RuntimeException("cannot get scrape unit from default"))
                .toBuilder()
                .id("test_run")
                .build();
    }

}
