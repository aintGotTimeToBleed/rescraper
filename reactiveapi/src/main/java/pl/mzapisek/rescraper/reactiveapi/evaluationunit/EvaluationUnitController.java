package pl.mzapisek.rescraper.reactiveapi.evaluationunit;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnitStatus;
import reactor.core.publisher.Flux;

import java.util.Map;

@RestController
@RequestMapping("/evaluationunits")
@RequiredArgsConstructor
public class EvaluationUnitController {

	private static final String EVALUATION_STATUS = "evaluation_status";
	private static final String JOB_ID = "job_id";

	private final EvaluationUnitService evaluationUnitService;

	@GetMapping
	public Flux<EvaluationUnit> getEvaluationUnits(@RequestParam Map<String, String> params) {
		if (params.containsKey(EVALUATION_STATUS) && params.containsKey(JOB_ID)) {
			return evaluationUnitService
					.findEvaluationUnitsByStatusAndJobId(
							EvaluationUnitStatus.valueOf(params.get(EVALUATION_STATUS)),
							params.get(JOB_ID));
		}
		return evaluationUnitService.findAll();
	}

}
