package pl.mzapisek.rescraper.reactiveapi.job;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.api.CompleteProcessService;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnitStatus;
import pl.mzapisek.rescraper.common.evaluationprocessor.repository.EvaluationUnitMongoRepository;
import pl.mzapisek.rescraper.common.jobprocessor.domain.Job;
import pl.mzapisek.rescraper.common.jobprocessor.domain.JobStatus;
import pl.mzapisek.rescraper.common.jobprocessor.repository.JobMongoRepository;
import pl.mzapisek.rescraper.common.scrapeprocessor.repository.ScrapeUnitMongoRepository;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.shared.domain.JobScrapingStatus;
import pl.mzapisek.rescraper.common.shared.domain.ScrapeUnitStatus;
import pl.mzapisek.rescraper.common.shared.mappers.JobMapper;
import pl.mzapisek.rescraper.common.shared.properties.KafkaBaseProperties;
import pl.mzapisek.rescraper.common.shared.services.RedisService;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.reactiveapi.job.exceptions.EvaluationUnitNotFound;
import pl.mzapisek.rescraper.reactiveapi.job.exceptions.JobAlreadyExist;
import pl.mzapisek.rescraper.reactiveapi.job.exceptions.JobAlreadyFinishedWithSuccess;
import pl.mzapisek.rescraper.reactiveapi.job.exceptions.JobNotFound;
import pl.mzapisek.rescraper.reactor_kafka.KafkaUtils;
import pl.mzapisek.rescraper.reactor_kafka.eventbus.EventBusProducer;
import pl.mzapisek.rescraper.reactor_kafka.jobprocessor.JobMessageProducer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

import static java.time.LocalDateTime.now;

@Service
@RequiredArgsConstructor
@Log4j2
public class JobService {

    private final JobMongoRepository jobMongoRepository;
    private final EvaluationUnitMongoRepository evaluationUnitMongoRepository;
    private final ScrapeUnitMongoRepository scrapeUnitMongoRepository;
    private final RedisService redisService;
    private final JobMessageProducer jobMessageProducer;
    private final KafkaUtils kafkaUtils;
    private final ScrapeScriptFactory scrapeScriptFactory;
    private final KafkaBaseProperties kafkaBaseProperties;
    private final CompleteProcessService completeProcessService;
    private final EventBusProducer eventBusProducer;

    Flux<Job> findAll() {
        return jobMongoRepository.findAll();
    }

    Mono<Job> find(String jobId) {
        return jobMongoRepository.findById(jobId)
                .switchIfEmpty(Mono.error(new JobNotFound(jobId)));
    }

    public Mono<Job> create(JobDto jobDto) {
        return createJobEntity(jobDto)
                .flatMap(job ->
                        jobMongoRepository
                                .existsById(job.getId())
                                .flatMap(jobExists -> {
                                    if (jobExists) {
                                        return Mono.error(new JobAlreadyExist(job.getId()));
                                    } else {
                                        return eventBusProducer
                                                .sendJob(JobMapper.mapJobToProto(job))
                                                .collectList()
                                                .flatMap(results -> jobMessageProducer.send(job)
                                                        .collectList()
                                                        .map(senderResults -> job));
                                    }
                                }));
    }

    Mono<Job> cancel(String jobId) {
        return jobMongoRepository
                .findById(jobId)
                .switchIfEmpty(Mono.error(new JobNotFound(jobId)))
                .flatMap(job -> {
                    redisService.cancelJob(jobId);
                    var cancelledJob = job.toBuilder().status(JobStatus.CANCELLED).build();
                    return eventBusProducer
                            .sendJob(JobMapper.mapJobToProto(cancelledJob))
                            .collectList()
                            .map(senderResults -> cancelledJob);
                });
    }

    Mono<JobScrapingStatus> checkStatus(String jobId) {
        // todo add metric, number of messages processed
        // todo add counter, how many messages left in topic?
        return jobMongoRepository
                .findById(jobId)
                .switchIfEmpty(Mono.error(new JobNotFound(jobId)))
                .flatMap(job -> Mono.just(JobScrapingStatus.builder().job(job).build()))
                .flatMap(scriptStatus -> addEvaluationCountToScrapeScriptStatusMono(jobId, scriptStatus))
                .flatMap(scriptStatus -> addFinishedEvaluationCountToScrapeScriptStatusMono(jobId, scriptStatus))
                .flatMap(scriptStatus -> addScrapeCountToScrapeScriptStatusMono(jobId, scriptStatus))
                .flatMap(scriptStatus -> addFinishedScrapeCountToScrapeScriptStatusMono(jobId, scriptStatus));
    }

    private Mono<Job> createJobEntity(JobDto jobDto) {
        ScrapeScript scrapeScript = scrapeScriptFactory.create(jobDto.getScriptName(), jobDto.getScriptVersion());
        var jobBuilder = Job.builder()
                .id(jobDto.getId() != null ? jobDto.getId() : UUID.randomUUID().toString())
                .createdAt(now())
                .scriptName(jobDto.getScriptName())
                .scriptVersion(jobDto.getScriptVersion())
                .status(JobStatus.READY)
                .assignedPartitions(jobDto.getAssignedPartitions())
                .urlParams(jobDto.getUrlParams() != null ? jobDto.getUrlParams() : scrapeScript.getDefaultUrlParams())
                .params(jobDto.getParams() != null ? jobDto.getParams() : scrapeScript.getDefaultParams());

        return Mono.just(jobDto)
                .filter(job -> job.getAssignedPartitions() != null)
                .map(jobWithAlreadyAssignedPartitions -> jobBuilder.build())
                .switchIfEmpty(kafkaUtils.getRandomPartitions(kafkaBaseProperties.getJobTopic(), scrapeScript.getMaxParallelism())
                        .map(partitions -> jobBuilder.assignedPartitions(partitions).build()));
    }

    Mono<Job> rerun(String jobId) {
        return jobMongoRepository
                .findById(jobId)
                .switchIfEmpty(Mono.error(new JobNotFound(jobId)))
                .flatMap(job -> {
                    if (JobStatus.SUCCESS.equals(job.getStatus())) {
                        return Mono.error(new JobAlreadyFinishedWithSuccess(job.getId()));
                    } else {
                        // todo refactor
                        return Mono
                                .just(createRerunJob(job))
                                .flatMap(rerunJob ->
                                        jobMessageProducer.send(rerunJob)
                                                .map(stringSenderResult -> rerunJob)
                                                .next());
                    }
                });
    }

    Mono<Job> complete(String jobId) {
        return evaluationUnitMongoRepository.findByJobId(jobId)
                .switchIfEmpty(Mono.error(new EvaluationUnitNotFound(String.format("Evaluation unit with jobId %s, not found", jobId))))
                .flatMap(evaluationUnit -> completeProcessService.completeProcess(evaluationUnit.getId(), jobId))
                .then(jobMongoRepository.findById(jobId));
    }

    private Job createRerunJob(Job job) {
        return job.toBuilder()
                .rerun(true)
                .status(JobStatus.PROCESSING)
                .build();
    }

    private Mono<JobScrapingStatus> addFinishedScrapeCountToScrapeScriptStatusMono(String jobId,
                                                                                   JobScrapingStatus jobScrapingStatus) {
        return scrapeUnitMongoRepository
                .countByJobIdAndScrapeUnitStatus(jobId, ScrapeUnitStatus.SUCCESS)
                .flatMap(finishedScrapeUnitCount -> {
                    jobScrapingStatus.setFinishedScrapeUnitsCount(finishedScrapeUnitCount);
                    return Mono.just(jobScrapingStatus);
                });
    }

    private Mono<JobScrapingStatus> addScrapeCountToScrapeScriptStatusMono(String jobId,
                                                                           JobScrapingStatus jobScrapingStatus) {
        return scrapeUnitMongoRepository
                .countByJobId(jobId)
                .flatMap(scrapeUnitCount -> {
                    jobScrapingStatus.setScrapeUnitsCount(scrapeUnitCount);
                    return Mono.just(jobScrapingStatus);
                });
    }

    private Mono<JobScrapingStatus> addFinishedEvaluationCountToScrapeScriptStatusMono(String jobId, JobScrapingStatus jobScrapingStatus) {
        return evaluationUnitMongoRepository
                .countByJobIdAndStatus(jobId, EvaluationUnitStatus.SUCCESS)
                .flatMap(finishedEvaluationUnitCount -> {
                    jobScrapingStatus.setFinishedEvaluationUnitsCount(finishedEvaluationUnitCount);
                    return Mono.just(jobScrapingStatus);
                });
    }

    private Mono<JobScrapingStatus> addEvaluationCountToScrapeScriptStatusMono(String jobId,
                                                                               JobScrapingStatus jobScrapingStatus) {
        return evaluationUnitMongoRepository
                .countByJobId(jobId)
                .flatMap(evaluationUnitCount -> {
                    jobScrapingStatus.setEvaluationUnitsCount(evaluationUnitCount);
                    return Mono.just(jobScrapingStatus);
                });
    }

}
