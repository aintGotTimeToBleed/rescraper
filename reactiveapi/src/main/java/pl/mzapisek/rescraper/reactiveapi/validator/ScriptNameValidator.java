package pl.mzapisek.rescraper.reactiveapi.validator;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Service
@RequiredArgsConstructor
public class ScriptNameValidator implements ConstraintValidator<ValidScriptName, String> {

    private final ScrapeScriptFactory scrapeScriptFactory;

    @Override
    public boolean isValid(String scriptName, ConstraintValidatorContext context) {
        return ConstraintValidatorHelper.validateField(() -> scrapeScriptFactory.create(scriptName), "scriptName", context);
    }

}
