package pl.mzapisek.rescraper.reactiveapi.result;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mzapisek.rescraper.common.jobprocessor.repository.JobMongoRepository;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import pl.mzapisek.rescraper.common.scripts.ScrapeScript;
import pl.mzapisek.rescraper.common.shared.repository.ScrapeResultMongoRepository;
import pl.mzapisek.rescraper.common.shared.services.ScrapeScriptFactory;
import pl.mzapisek.rescraper.reactiveapi.job.exceptions.JobNotFound;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@RequiredArgsConstructor
@RestController
@RequestMapping("results")
public class ResultController {

    private final JobMongoRepository jobMongoRepository;
    private final ScrapeResultMongoRepository scrapeResultMongoRepository;
    private final ScrapeScriptFactory scrapeScriptFactory;

    @Operation(summary = "generate result as a csv file")
    @GetMapping("/csv/{jobId}")
    public Mono<ResponseEntity<Resource>> generateResult(@PathVariable String jobId) {
        // todo refactor, extract
        return jobMongoRepository.findById(jobId)
                .switchIfEmpty(Mono.error(new JobNotFound(jobId)))
                .map(job -> scrapeScriptFactory.create(job.getScriptName(), job.getScriptVersion()))
                .publishOn(Schedulers.boundedElastic())
                .flatMap(scrapeScript -> create(jobId, scrapeScript)
                        .map(resource -> ResponseEntity.ok()
                                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + scrapeScript.getScriptName() + "_" + jobId + ".csv")
                                .body(resource)));
    }

    // todo refactor, extract
    private Mono<Resource> create(String jobId, ScrapeScript scrapeScript) {
        return scrapeResultMongoRepository.findByJobId(jobId)
                .switchIfEmpty(Mono.error(new RuntimeException("no results found")))
                .map(ScrapeResult::getJsonValue)
                .map(scrapeScript::getRow)
                .collectList()
                .map(strings -> {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(scrapeScript.getHeader());
                    strings.forEach(stringBuilder::append);
                    return stringBuilder.toString();
                })
                .map(s -> new ByteArrayResource(s.getBytes()));
    }

}
