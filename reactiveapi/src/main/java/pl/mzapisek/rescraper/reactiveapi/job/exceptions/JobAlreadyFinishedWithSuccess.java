package pl.mzapisek.rescraper.reactiveapi.job.exceptions;

public class JobAlreadyFinishedWithSuccess extends RuntimeException {

    private static final String MESSAGE = "job of given id: %s already finished with success";

    public JobAlreadyFinishedWithSuccess(String jobId) {
        super(String.format(MESSAGE, jobId));
    }

}
