package pl.mzapisek.rescraper.reactiveapi.script.testrun;

import lombok.Builder;
import lombok.Getter;
import pl.mzapisek.rescraper.common.evaluationprocessor.domain.EvaluationUnit;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeResult;
import pl.mzapisek.rescraper.common.scrapeprocessor.domain.ScrapeUnit;

import java.util.List;

@Getter
@Builder
public class TestRunResult {

    private String scriptName;
    private Integer scriptVersion;
    private List<EvaluationUnit> evaluationUnits;
    private EvaluationUnit evaluationUnit;
    private List<ScrapeUnit> scrapeUnits;
    private ScrapeUnit scrapeUnit;
    private List<ScrapeResult> scrapeResults;

}
