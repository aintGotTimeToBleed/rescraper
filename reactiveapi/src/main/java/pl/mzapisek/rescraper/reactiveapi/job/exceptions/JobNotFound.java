package pl.mzapisek.rescraper.reactiveapi.job.exceptions;

public class JobNotFound extends RuntimeException {

    private static final String MESSAGE = "job of id: %s not found";

    public JobNotFound(String jobId) {
        super(String.format(MESSAGE, jobId));
    }

}
