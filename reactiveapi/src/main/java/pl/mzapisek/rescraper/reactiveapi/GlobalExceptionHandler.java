package pl.mzapisek.rescraper.reactiveapi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ServerWebExchange;
import pl.mzapisek.rescraper.common.shared.exceptions.ScrapeScriptNotFound;
import pl.mzapisek.rescraper.reactiveapi.job.exceptions.JobAlreadyExist;
import pl.mzapisek.rescraper.reactiveapi.job.exceptions.JobAlreadyFinishedWithSuccess;
import pl.mzapisek.rescraper.reactiveapi.job.exceptions.JobNotFound;
import pl.mzapisek.rescraper.reactiveapi.task.exception.TaskAlreadyExists;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler({
			JobAlreadyExist.class, JobAlreadyFinishedWithSuccess.class, ConstraintViolationException.class,
			TaskAlreadyExists.class
	})
	public final ResponseEntity<String> handleApiExceptions(Exception ex, ServerWebExchange serverWebExchange) {
		log.error(ex.getMessage() + "; on path: " + serverWebExchange.getRequest().getPath());
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({ScrapeScriptNotFound.class, JobNotFound.class})
	public final ResponseEntity<String> handleNotFoundExceptions(Exception ex, ServerWebExchange serverWebExchange) {
		log.error(ex.getMessage() + "; on path: " + serverWebExchange.getRequest().getPath());
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler({WebExchangeBindException.class})
	public ResponseEntity<List<String>> handleException(WebExchangeBindException ex) {
		return ResponseEntity.badRequest()
				.body(ex.getBindingResult()
						.getAllErrors()
						.stream()
						.map(DefaultMessageSourceResolvable::getDefaultMessage)
						.collect(Collectors.toList()));
	}

}
