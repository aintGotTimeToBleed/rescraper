package pl.mzapisek.rescraper.reactiveapi.task.exception;

public class TaskAlreadyExists extends RuntimeException {

    public TaskAlreadyExists() {
        super();
    }

    public TaskAlreadyExists(String message) {
        super(message);
    }

}
