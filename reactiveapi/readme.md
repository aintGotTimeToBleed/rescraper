## ReScraper Reactive API

#### Create job example
POST localhost:9999/jobs
```
{
  "status":"READY",
  "scriptName":"NMArchitects",
  "scriptVersion": 1
}
```
```
{
  "status":"READY",
  "scriptName":"CARealEstates",
  "scriptVersion": 1
}
```
```
{
  "status":"READY",
  "scriptName":"CARealEstates",
  "scriptVersion": 1,
  "urlParams": [
         "aaa"
  ]
}
```
#### Test runs examples
##### Complete test run
POST localhost:9999/testruns
```
{
    "id": "complete_run_test_job",
    "scriptName":"CARealEstates",
    "scriptVersion": 1,
    "urlParams": [
        "aaa"
    ]
}
```
##### Job 
POST localhost:9999/testruns/job
```
{
    "id": "test_job",
    "scriptName":"CARealEstates",
    "scriptVersion": 1,
    "urlParams": [
        "aaa"
    ]
}
```
##### Evaluation
POST localhost:9999/testruns/evaluation
```
{
    "id": "test_evaluation",
    "scriptName":"CARealEstates",
    "scriptVersion": 1,
    "element": "aaa"
}
```
##### Scrape
POST localhost:9999/testruns/scrape
```
{
    "id": "test_scrape",
    "scriptName":"CARealEstates",
    "scriptVersion": 1,
    "urlParams": [
        "01038853"
    ]
}
```
#### Schedule task
```
{
    "id": "73fb2648-7fb7-4d4e-bc4a-153772ab8bdc",
    "taskName": "test",
    "cronExpression": "0 0/1 * 1/1 * ?",
    "scriptName": "CARealEstates",
    "scriptVersion": 1,
    "description": "example"
}
```

#### Swagger endpoint
- localhost:9999/swagger-ui.html

