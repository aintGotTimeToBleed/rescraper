package pl.mzapisek.rescraper.evaluationprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.mzapisek.rescraper.reactor_kafka.evaluationprocessor.EnableReactorKafkaEvaluationProcessor;
import reactor.blockhound.BlockHound;

@SpringBootApplication
//@EnableSpringKafkaEvaluationProcessor // for spring kafka
@EnableReactorKafkaEvaluationProcessor // for reactor kafka
public class EvaluationProcessorApplication {

    public static void main(String[] args) {
        BlockHound.install();
        SpringApplication.run(EvaluationProcessorApplication.class, args);
    }

}
